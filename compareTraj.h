// MODULES
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <math.h>
#include <mpi.h>

/* MPI function to share and compare trajectories accross all processors */
int compareEndoTraj(struct endoTrajCompIn compIn)
{
    int i, j, k, njobs, idx, remIdx, jobType, jobIdx;
    //int recTrajJobs[jobParams.rootNumTrajJobs + 1];
    int target, field, compPts, numApproaches, totApproaches, approachFlag, failFlag;
    double delX, delY, delZ, rSep, mjd, flagDist;
    int jobPtr;
    int targetMapId, fieldMapId;
    int approachCount;
    int nomLength = compIn.jobParams.rootNumTrajJobs + 1;
    // int *compJobs;
    int compJobs[compIn.jobParams.rootNumTrajJobs + 1];
    int targId, fieldId, Pidx, cpuIdx, targLength, fieldLength, tag, tag1, tag2;
    int locNumJobs;
    int length;
    int combos = 0;

    // define MPI Status
    MPI_Status stat;

    // derived MPI data types
    MPI_Datatype trajJobs;
    MPI_Datatype targSend;
    MPI_Datatype fieldSend;
    MPI_Type_contiguous(compIn.jobParams.rootNumTrajJobs + 1, MPI_INT, &trajJobs);
    MPI_Type_contiguous(12*compIn.mxpts, MPI_DOUBLE, &targSend);
    MPI_Type_contiguous(12*compIn.mxpts, MPI_DOUBLE, &fieldSend);
    MPI_Type_commit(&trajJobs);
    MPI_Type_commit(&targSend);
    MPI_Type_commit(&fieldSend);

    // zero Pcount
    // memset(&Pcount,0,combs*sizeof(int));

    // assign jobs
    cpuIdx = 1;
    int localEntryIdx = 0;
    for(i = 0; i < compIn.combs; i++)
    {
        // assign
        if(cpuIdx == compIn.rank)
        {
            compJobs[localEntryIdx] = i;
            localEntryIdx++;
        }

        // update index
        cpuIdx++;

        // reset cpuIdx if exceeds num cpus
        if(cpuIdx == compIn.cpus)
            cpuIdx = 1;
        
    }

    // save local number of jobs - no jobs on root rank
    locNumJobs = localEntryIdx;

    // MASTER PROCESSOR
    if (compIn.rank == 0)
    {
        int Pcount[compIn.combs];
        int Pbuff[compIn.combs][2];

        // zero Pcount
        memset(&Pcount,0,compIn.combs*sizeof(int));

        // define array for collisions
        //int locApproaches[jobParams.rootNumTrajJobs + 1]
        

        // send trajectories
        cpuIdx = 1;
        for(i = 0; i < compIn.combs; i++)
        {
            // set Ids
            // printf("STARTING TRAJECTORY SENDING\n");
            Pidx = compIn.trajCompJobs[i];
            targetMapId = compIn.P[Pidx][0];
            fieldMapId = compIn.P[Pidx][1];
            targId = compIn.targetMap[targetMapId];
            fieldId = compIn.targetMap[fieldMapId];
            // printf(" Sending from ROOT --     index : %d of %d -- Pidx: %d -- targMapId: %d -- fieldMapId: %d -- targId: %d -- fieldId: %d \n",i,compIn.combs,Pidx,targetMapId,fieldMapId,targId,fieldId);

            // send trajectories
            targLength = compIn.targTrajLengths[targId];
            fieldLength = compIn.targTrajLengths[fieldId];
            tag1 = Pidx;
            tag2 = Pidx + 1000;
            MPI_Send(compIn.targetTrajectories[targId], 1, targSend, cpuIdx, tag1, MPI_COMM_WORLD);
            MPI_Send(compIn.targetTrajectories[fieldId], 1, fieldSend, cpuIdx, tag2, MPI_COMM_WORLD);

            // update cpuIdx
            cpuIdx++;

            // reset cpuIdx if equal to number of cpus
            if(cpuIdx == compIn.cpus)
                cpuIdx = 1;

        }
        // printf("DONE WITH SENDS\n");

        // calculate total number of approaches
        numApproaches = 0;
        MPI_Allreduce(&numApproaches, &totApproaches, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);

        // reallocate C array if necessary
        if(4*totApproaches > *(compIn.C->elements))
        {   
            printf("reallocating C\n");
            reallocContigDouble(compIn.C, 2*4*totApproaches);
            *(compIn.cpuMem) += *(compIn.C->mem);
            *(compIn.cpuMem) -= *(compIn.C->prevmem);
            printf("done reallocating C\n");
            
        }

        // gather from other cores
        idx = 0;
        for (i = 1; i < compIn.cpus; i++)
        {
            MPI_Recv(&numApproaches, 1, MPI_INT, i, 0, MPI_COMM_WORLD, &stat);

            // reallocate arrays if necessary
            if(numApproaches > *(compIn.locApproaches->elements))
            {
                reallocContigInt(compIn.locApproaches, 2*numApproaches);
                reallocContigDouble(compIn.mjdArr, 2*numApproaches);
                reallocContigDouble(compIn.sepArr, 2*numApproaches);
                *(compIn.cpuMem) += ( *(compIn.locApproaches->mem) + *(compIn.mjdArr->mem) + *(compIn.sepArr->mem) );
                *(compIn.cpuMem) -= ( *(compIn.locApproaches->prevmem) + *(compIn.mjdArr->prevmem) + *(compIn.sepArr->prevmem) ); 

            }

            // get arrays
            if (numApproaches > 0)
            {
                MPI_Recv(compIn.locApproaches->arr, numApproaches, MPI_INT, i, 1, MPI_COMM_WORLD, &stat);
                MPI_Recv(compIn.sepArr->arr, numApproaches, MPI_DOUBLE, i, 2, MPI_COMM_WORLD, &stat);
                MPI_Recv(compIn.mjdArr->arr, numApproaches, MPI_DOUBLE, i, 3, MPI_COMM_WORLD, &stat);
            }
            for (j = 0; j < numApproaches; j++)
            {   
                Pidx = compIn.locApproaches->arr[j];
                target = compIn.P[Pidx][0];
                field = compIn.P[Pidx][1];
                compIn.C->arr[idx * 4 + 0] = (double)target;
                compIn.C->arr[idx * 4 + 1] = (double)field;
                compIn.C->arr[idx * 4 + 2] = compIn.sepArr->arr[j];
                compIn.C->arr[idx * 4 + 3] = compIn.mjdArr->arr[j];
                idx++;

                // count combinations
                if(Pcount[Pidx] == 0)
                {
                    
                    Pbuff[combos][0] = target;
                    Pbuff[combos][1] = field;
                    Pcount[Pidx] = 1;
                    combos++;

                    // reallocate if necessary

                    
                }
            }
        }

        // reallocate next P if necessary
        printf("begining to reallocate P\n");
        reallocRowInt(compIn.nextRed->P, combos);
        printf("finished reallocating\n");
        *(compIn.cpuMem) += *(compIn.nextRed->P->mem);
        *(compIn.cpuMem) -= *(compIn.nextRed->P->prevmem);
        printf("finished counting mem\n");
        // save Pbuffer to next P array
        for(i = 0; i < combos; i++)
        {
            compIn.nextRed->P->arr[i][0] = Pbuff[i][0];
            compIn.nextRed->P->arr[i][1] = Pbuff[i][1];
        }

    }

    // SLAVE PROCESS
    else
    {

        // init number of approaches
        numApproaches = 0;

        for(i = 0; i < locNumJobs; i++)
        {
            jobIdx = compJobs[i];
            Pidx = compIn.trajCompJobs[jobIdx];
            targetMapId = compIn.P[Pidx][0];
            fieldMapId = compIn.P[Pidx][1];
            targId = compIn.targetMap[targetMapId];
            fieldId = compIn.targetMap[fieldMapId]; 
            // printf(" Receiving from RANK %d -- index : %d of %d -- Pidx: %d -- targMapId: %d -- fieldMapId: %d -- targId: %d -- fieldId: %d \n",compIn.rank,i,locNumJobs,Pidx,targetMapId,fieldMapId,targId,fieldId); 

            // get trajectories
            tag1 = Pidx;
            tag2 = Pidx + 1000;

            // printf("looking for job %d on cpu %d - iter: %d --- %d points\n",tag1,compIn.rank,i,compIn.mxpts);
            MPI_Recv(compIn.targetTrajectories[0], 12*compIn.mxpts, MPI_DOUBLE, 0, tag1, MPI_COMM_WORLD, &stat);
            MPI_Recv(compIn.fieldTrajectories[0], 12*compIn.mxpts, MPI_DOUBLE, 0, tag2, MPI_COMM_WORLD, &stat);
            // printf("received job %d on cpu %d\n",tag1,compIn.rank);

            // skip if propagation has failed
            if(compIn.targExitCodes[targId] == 1 || compIn.targExitCodes[fieldId] == 1 )
            {
                printf("propagation failure detected on comparison of target %d (%d) and field %d (%d)...skipping\n",targId,compIn.targExitCodes[targId],fieldId,compIn.targExitCodes[fieldId]);
                continue;
            }

            // get lengths
            targLength = compIn.targTrajLengths[targId];
            fieldLength = compIn.targTrajLengths[fieldId];
            

            // check which trajectory has a shorter propagation time
            if (compIn.targTrajLengths[targId] < compIn.targTrajLengths[fieldId])
                compPts = compIn.targTrajLengths[targId];
            else
                compPts = compIn.targTrajLengths[fieldId];
            // printf("rank_%d comparing target %d with field %d over %d points\n",compIn.rank,targId,fieldId,compPts);
            // step through trajectories

            for (j = 0; j < compPts; j++)
            {
                // calculate relative separations distance
                delX = compIn.targetTrajectories[0][j * 12 + 1] - compIn.fieldTrajectories[0][j * 12 + 1];
                delY = compIn.targetTrajectories[0][j * 12 + 2] - compIn.fieldTrajectories[0][j * 12 + 2];
                delZ = compIn.targetTrajectories[0][j * 12 + 3] - compIn.fieldTrajectories[0][j * 12 + 3];
                rSep =  sqrt(delX*delX + delY*delY + delZ*delZ);

                // check if a close approach has occurred
                if (rSep <= compIn.dApproach)
                {
                    // save approach conditions
                    mjd = compIn.targetTrajectories[0][j * 12 + 0]; 
                    flagDist = rSep;

                    // add to local approach array
                    compIn.locApproaches->arr[numApproaches] = Pidx;
                    compIn.mjdArr->arr[numApproaches] = mjd;
                    compIn.sepArr->arr[numApproaches] = flagDist;

                    // update number of approaches
                    numApproaches++;

                    // reallocate arrays if necessary
                    if(numApproaches == *(compIn.locApproaches->elements))
                    {    
                        reallocContigInt(compIn.locApproaches, 2*numApproaches);
                        reallocContigDouble(compIn.mjdArr, 2*numApproaches);
                        reallocContigDouble(compIn.sepArr, 2*numApproaches);
                        *(compIn.cpuMem) += ( *(compIn.locApproaches->mem) + *(compIn.mjdArr->mem) + *(compIn.sepArr->mem) );
                        *(compIn.cpuMem) -= ( *(compIn.locApproaches->prevmem) + *(compIn.mjdArr->prevmem) + *(compIn.sepArr->prevmem) );
                    }  
                    
                } 

            }

            

        }
        // printf("finished on compIn.rank %d\n",compIn.rank);

        // calculate total number of approaches
        MPI_Allreduce(&numApproaches, &totApproaches, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);

        // send local approach array to master processor
        // printf("sending %d from %d\n",numApproaches,compIn.rank);
        MPI_Send(&numApproaches, 1, MPI_INT, 0, 0, MPI_COMM_WORLD);
        if (numApproaches > 0)
        {
            MPI_Send(compIn.locApproaches->arr, numApproaches, MPI_INT, 0, 1, MPI_COMM_WORLD);
            MPI_Send(compIn.sepArr->arr, numApproaches, MPI_DOUBLE, 0, 2, MPI_COMM_WORLD);
            MPI_Send(compIn.mjdArr->arr, numApproaches, MPI_DOUBLE, 0, 3, MPI_COMM_WORLD);
        }

    }
    
    // broadcast number of combinations
    MPI_Bcast(&combos, 1, MPI_INT, 0, MPI_COMM_WORLD);

    // reallocate next P if necessary
    reallocRowInt(compIn.nextRed->P, combos);
    *(compIn.cpuMem) += *(compIn.nextRed->P->mem);
    *(compIn.cpuMem) -= *(compIn.nextRed->P->prevmem);

    // send all other processors P array
    for (i = 0; i < combos; i++)
        MPI_Bcast(compIn.nextRed->P->arr[i], 2, MPI_INT, 0, MPI_COMM_WORLD);

    // dellocate MPI derived types
    MPI_Type_free(&trajJobs);
    MPI_Type_free(&targSend);
    MPI_Type_free(&fieldSend);

    // save number of combos
    *(compIn.pairs) = combos;

    return totApproaches;

}

/* MPI function to share and compare exogenous trajectories accross all processors */
int compareExoTraj(struct exoTrajCompIn compIn)
{
    int i, j, k, njobs, idx, remIdx, jobType, jobIdx;
    //int recTrajJobs[jobParams.rootNumTrajJobs + 1];
    int target, field, compPts, numApproaches, totApproaches, approachFlag, failFlag;
    double delX, delY, delZ, rSep, mjd, flagDist;
    int jobPtr;
    int targetMapId, fieldMapId;
    int approachCount;
    int nomLength = compIn.jobParams.rootNumTrajJobs + 1;
    // int *compJobs;
    int compJobs[compIn.jobParams.rootNumTrajJobs + 1];
    int targId, fieldId, Pidx, cpuIdx, targLength, fieldLength, tag, tag1, tag2;
    int locNumJobs;
    int length;
    int combos = 0;

    // define MPI Status
    MPI_Status stat;

    // derived MPI data types
    MPI_Datatype trajJobs;
    MPI_Datatype targSend;
    MPI_Datatype fieldSend;
    MPI_Type_contiguous(compIn.jobParams.rootNumTrajJobs + 1, MPI_INT, &trajJobs);
    MPI_Type_contiguous(12*compIn.mxpts, MPI_DOUBLE, &targSend);
    MPI_Type_contiguous(12*compIn.mxpts, MPI_DOUBLE, &fieldSend);
    MPI_Type_commit(&trajJobs);
    MPI_Type_commit(&targSend);
    MPI_Type_commit(&fieldSend);

    // zero Pcount
    // memset(&Pcount,0,combs*sizeof(int));

    // assign jobs
    cpuIdx = 1;
    int localEntryIdx = 0;
    for(i = 0; i < compIn.combs; i++)
    {
        // assign
        if(cpuIdx == compIn.rank)
        {
            compJobs[localEntryIdx] = i;
            localEntryIdx++;
        }

        // update index
        cpuIdx++;

        // reset cpuIdx if exceeds num cpus
        if(cpuIdx == compIn.cpus)
            cpuIdx = 1;
        
    }

    // save local number of jobs - no jobs on root rank
    locNumJobs = localEntryIdx;

    // MASTER PROCESSOR
    if (compIn.rank == 0)
    {
        int Pcount[compIn.combs];
        int Pbuff[compIn.combs][2];

        // zero Pcount
        memset(&Pcount,0,compIn.combs*sizeof(int));

        // define array for collisions
        //int locApproaches[jobParams.rootNumTrajJobs + 1]
        

        // send trajectories
        cpuIdx = 1;
        for(i = 0; i < compIn.combs; i++)
        {
            // set Ids
            // printf("STARTING TRAJECTORY SENDING\n");
            Pidx = compIn.trajCompJobs[i];
            targetMapId = compIn.P[Pidx][0];
            fieldMapId = compIn.P[Pidx][1];
            targId = compIn.targetMap[targetMapId];
            fieldId = compIn.fieldMap[fieldMapId];
            // printf(" Sending from ROOT --     index : %d of %d -- Pidx: %d -- targMapId: %d -- fieldMapId: %d -- targId: %d -- fieldId: %d \n",i,compIn.combs,Pidx,targetMapId,fieldMapId,targId,fieldId);

            // send trajectories
            targLength = compIn.targTrajLengths[targId];
            fieldLength = compIn.fieldTrajLengths[fieldId];
            tag1 = Pidx;
            tag2 = Pidx + 1000;
            MPI_Send(compIn.targetTrajectories[targId], 1, targSend, cpuIdx, tag1, MPI_COMM_WORLD);
            MPI_Send(compIn.fieldTrajectories[fieldId], 1, fieldSend, cpuIdx, tag2, MPI_COMM_WORLD);

            // update cpuIdx
            cpuIdx++;

            // reset cpuIdx if equal to number of cpus
            if(cpuIdx == compIn.cpus)
                cpuIdx = 1;

        }
        // printf("DONE WITH SENDS\n");

        // calculate total number of approaches
        numApproaches = 0;
        MPI_Allreduce(&numApproaches, &totApproaches, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);

        // reallocate C array if necessary
        if(4*totApproaches > *(compIn.C->elements))
        {   
            printf("reallocating C\n");
            reallocContigDouble(compIn.C, 2*4*totApproaches);
            *(compIn.cpuMem) += *(compIn.C->mem);
            *(compIn.cpuMem) -= *(compIn.C->prevmem);
            printf("done reallocating C\n");
            
        }

        // gather from other cores
        idx = 0;
        for (i = 1; i < compIn.cpus; i++)
        {
            MPI_Recv(&numApproaches, 1, MPI_INT, i, 0, MPI_COMM_WORLD, &stat);

            // reallocate arrays if necessary
            if(numApproaches > *(compIn.locApproaches->elements))
            {
                reallocContigInt(compIn.locApproaches, 2*numApproaches);
                reallocContigDouble(compIn.mjdArr, 2*numApproaches);
                reallocContigDouble(compIn.sepArr, 2*numApproaches);
                *(compIn.cpuMem) += ( *(compIn.locApproaches->mem) + *(compIn.mjdArr->mem) + *(compIn.sepArr->mem) );
                *(compIn.cpuMem) -= ( *(compIn.locApproaches->prevmem) + *(compIn.mjdArr->prevmem) + *(compIn.sepArr->prevmem) ); 

            }

            // get arrays
            if (numApproaches > 0)
            {
                MPI_Recv(compIn.locApproaches->arr, numApproaches, MPI_INT, i, 1, MPI_COMM_WORLD, &stat);
                MPI_Recv(compIn.sepArr->arr, numApproaches, MPI_DOUBLE, i, 2, MPI_COMM_WORLD, &stat);
                MPI_Recv(compIn.mjdArr->arr, numApproaches, MPI_DOUBLE, i, 3, MPI_COMM_WORLD, &stat);
            }
            for (j = 0; j < numApproaches; j++)
            {   
                Pidx = compIn.locApproaches->arr[j];
                target = compIn.P[Pidx][0];
                field = compIn.P[Pidx][1];
                compIn.C->arr[idx * 4 + 0] = (double)target;
                compIn.C->arr[idx * 4 + 1] = (double)field;
                compIn.C->arr[idx * 4 + 2] = compIn.sepArr->arr[j];
                compIn.C->arr[idx * 4 + 3] = compIn.mjdArr->arr[j];
                idx++;

                // count combinations
                if(Pcount[Pidx] == 0)
                {
                    
                    Pbuff[combos][0] = target;
                    Pbuff[combos][1] = field;
                    Pcount[Pidx] = 1;
                    combos++;

                    // reallocate if necessary

                    
                }
            }
        }

        // reallocate next P if necessary
        printf("begining to reallocate P\n");
        reallocRowInt(compIn.nextRed->P, combos);
        printf("finished reallocating\n");
        *(compIn.cpuMem) += *(compIn.nextRed->P->mem);
        *(compIn.cpuMem) -= *(compIn.nextRed->P->prevmem);
        printf("finished counting mem\n");
        // save Pbuffer to next P array
        for(i = 0; i < combos; i++)
        {
            compIn.nextRed->P->arr[i][0] = Pbuff[i][0];
            compIn.nextRed->P->arr[i][1] = Pbuff[i][1];
        }

    }

    // SLAVE PROCESS
    else
    {

        // init number of approaches
        numApproaches = 0;

        for(i = 0; i < locNumJobs; i++)
        {
            jobIdx = compJobs[i];
            Pidx = compIn.trajCompJobs[jobIdx];
            targetMapId = compIn.P[Pidx][0];
            fieldMapId = compIn.P[Pidx][1];
            targId = compIn.targetMap[targetMapId];
            fieldId = compIn.fieldMap[fieldMapId]; 
            // printf(" Receiving from RANK %d -- index : %d of %d -- Pidx: %d -- targMapId: %d -- fieldMapId: %d -- targId: %d -- fieldId: %d \n",compIn.rank,i,locNumJobs,Pidx,targetMapId,fieldMapId,targId,fieldId); 

            // get trajectories
            tag1 = Pidx;
            tag2 = Pidx + 1000;

            // printf("looking for job %d on cpu %d - iter: %d --- %d points\n",tag1,compIn.rank,i,compIn.mxpts);
            MPI_Recv(compIn.targetTrajectories[0], 12*compIn.mxpts, MPI_DOUBLE, 0, tag1, MPI_COMM_WORLD, &stat);
            MPI_Recv(compIn.fieldTrajectories[0], 12*compIn.mxpts, MPI_DOUBLE, 0, tag2, MPI_COMM_WORLD, &stat);
            // printf("received job %d on cpu %d\n",tag1,compIn.rank);

            // skip if propagation has failed
            if(compIn.targExitCodes[targId] == 1 || compIn.fieldExitCodes[fieldId] == 1 )
            {
                printf("propagation failure detected on comparison of target %d (%d) and field %d (%d)...skipping\n",targId,compIn.targExitCodes[targId],fieldId,compIn.fieldExitCodes[fieldId]);
                continue;
            }

            // get lengths
            targLength = compIn.targTrajLengths[targId];
            fieldLength = compIn.fieldTrajLengths[fieldId];
            

            // check which trajectory has a shorter propagation time
            if (compIn.targTrajLengths[targId] < compIn.fieldTrajLengths[fieldId])
                compPts = compIn.targTrajLengths[targId];
            else
                compPts = compIn.fieldTrajLengths[fieldId];
            // printf("rank_%d comparing target %d with field %d over %d points\n",compIn.rank,targId,fieldId,compPts);
            // step through trajectories

            for (j = 0; j < compPts; j++)
            {
                // calculate relative separations distance
                delX = compIn.targetTrajectories[0][j * 12 + 1] - compIn.fieldTrajectories[0][j * 12 + 1];
                delY = compIn.targetTrajectories[0][j * 12 + 2] - compIn.fieldTrajectories[0][j * 12 + 2];
                delZ = compIn.targetTrajectories[0][j * 12 + 3] - compIn.fieldTrajectories[0][j * 12 + 3];
                rSep =  sqrt(delX*delX + delY*delY + delZ*delZ);

                // check if a close approach has occurred
                if (rSep <= compIn.dApproach)
                {
                    // save approach conditions
                    mjd = compIn.targetTrajectories[0][j * 12 + 0]; 
                    flagDist = rSep;

                    // add to local approach array
                    compIn.locApproaches->arr[numApproaches] = Pidx;
                    compIn.mjdArr->arr[numApproaches] = mjd;
                    compIn.sepArr->arr[numApproaches] = flagDist;

                    // update number of approaches
                    numApproaches++;

                    // reallocate arrays if necessary
                    if(numApproaches == *(compIn.locApproaches->elements))
                    {    
                        reallocContigInt(compIn.locApproaches, 2*numApproaches);
                        reallocContigDouble(compIn.mjdArr, 2*numApproaches);
                        reallocContigDouble(compIn.sepArr, 2*numApproaches);
                        *(compIn.cpuMem) += ( *(compIn.locApproaches->mem) + *(compIn.mjdArr->mem) + *(compIn.sepArr->mem) );
                        *(compIn.cpuMem) -= ( *(compIn.locApproaches->prevmem) + *(compIn.mjdArr->prevmem) + *(compIn.sepArr->prevmem) );
                    }  
                    
                } 

            }

            

        }
        // printf("finished on compIn.rank %d\n",compIn.rank);

        // calculate total number of approaches
        MPI_Allreduce(&numApproaches, &totApproaches, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);

        // send local approach array to master processor
        // printf("sending %d from %d\n",numApproaches,compIn.rank);
        MPI_Send(&numApproaches, 1, MPI_INT, 0, 0, MPI_COMM_WORLD);
        if (numApproaches > 0)
        {
            MPI_Send(compIn.locApproaches->arr, numApproaches, MPI_INT, 0, 1, MPI_COMM_WORLD);
            MPI_Send(compIn.sepArr->arr, numApproaches, MPI_DOUBLE, 0, 2, MPI_COMM_WORLD);
            MPI_Send(compIn.mjdArr->arr, numApproaches, MPI_DOUBLE, 0, 3, MPI_COMM_WORLD);
        }

    }
    
    // broadcast number of combinations
    MPI_Bcast(&combos, 1, MPI_INT, 0, MPI_COMM_WORLD);

    // reallocate next P if necessary
    reallocRowInt(compIn.nextRed->P, combos);
    *(compIn.cpuMem) += *(compIn.nextRed->P->mem);
    *(compIn.cpuMem) -= *(compIn.nextRed->P->prevmem);

    // send all other processors P array
    for (i = 0; i < combos; i++)
        MPI_Bcast(compIn.nextRed->P->arr[i], 2, MPI_INT, 0, MPI_COMM_WORLD);

    // dellocate MPI derived types
    MPI_Type_free(&trajJobs);
    MPI_Type_free(&targSend);
    MPI_Type_free(&fieldSend);

    // save number of combos
    *(compIn.pairs) = combos;

    return totApproaches;

}