#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <math.h>
#include "mpi.h"

int launchExoCase( struct branchInput exoIn )
{
    // DEFINE VARIABLES
    int deltaT;
    double vmax;
    double tstep1;
    struct exoJobParams exoJobs1, exoJobs2, exoJobs3, exoJobs4, exoJobs5;
    double partialTspan;
    int mxpts;
    int appr1, appr2, appr3, appr4, appr5;
    struct redStates *reds, *reds2, *reds3, *reds4, *reds5;
    int *targPropJobs, *fieldPropJobs, *trajCompJobs, *targExitCodes, *fieldExitCodes;
    int *targTrajLengths, *fieldTrajLengths;
    struct contigDouble *C;
    struct contigInt *locApproaches;
    struct contigDouble *sepArr;
    struct contigDouble *mjdArr;
    int numTargJobs, numFieldJobs, numCompJobs, *targetMapping, *fieldMapping;
    struct doubleSemi *targetTraj, *fieldTraj;
    int *globTargetMap, *globFieldMap;
    int targL, fieldL;
    int pairs1, pairs2, pairs3, pairs4, pairs5;
    int prevAppr1;

    // MEMORY VARIABLES
    double memEstimate = 0;
    double sumAllocatedMem = 0;
    double sumDeltaMem = 0;
    double allocatedMem = 0;
    double prevMem = 0;
    double deltaMem = 0;
    double buff = 0;
    double buff2 = 0;
    double buff3 = 0;
    double buff4 = 0;
    double buff5 = 0;
    double targPropJobsMem = 0;
    double fieldPropJobsMem = 0;
    double trajCompJobsMem = 0;

    // INPUT STRUCT VARS
    struct exoTargPropInput targPropIn;
    struct exoFieldPropInput fieldPropIn;
    struct exoTrajCompIn trajCompIn;
    struct exoUpdateIn updateIn;
    struct exoReduceIn reduceIn;
    struct printIn poutIn;

    for (deltaT = 0; deltaT < exoIn.inputs.numIntervals; deltaT++)
    {   
        // sum memory
        MPI_Allreduce(&exoIn.allocatedMem, &sumAllocatedMem, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
        MPI_Allreduce(&deltaMem, &sumDeltaMem, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);

        // print memory use summary
        if (exoIn.rank == 0) {
            printf("TIME CHUNK %d of %d\n",deltaT, exoIn.inputs.numIntervals);
            printf("\n\n=============================================================================\n");
            printf("MEMORY INFORMATION\n");
            printf("Allocated memory (gigabytes) = %f\n",(sumAllocatedMem/1e+3));
            printf("Change in allocated memory (gigabytes) = %f\n", (sumDeltaMem/1e+3));
            printf("=============================================================================\n\n\n");
        }

        // on first iteration
        if (deltaT == 0)
        {
            if (exoIn.rank == 0)
                printf("EXOGENOUS CASE\n");
            // allocate object mapping
            targetMapping = calloc(exoIn.inputs.numTargetObjs, sizeof(int));
            fieldMapping = calloc(exoIn.inputs.numFieldObjs, sizeof(int));
            initMapping(targetMapping, exoIn.inputs.numTargetObjs);
            initMapping(fieldMapping, exoIn.inputs.numFieldObjs);
            MPI_Barrier(MPI_COMM_WORLD);
            exoIn.allocatedMem += (exoIn.inputs.numTargetObjs + exoIn.inputs.numFieldObjs)*sizeof(int)/1e6;

            // allocate memory for max propagations jobs
            targPropJobs = calloc(exoIn.inputs.numTargetObjs, sizeof(int));
            fieldPropJobs = calloc(exoIn.inputs.numFieldObjs, sizeof(int));
            MPI_Barrier(MPI_COMM_WORLD);
            exoIn.allocatedMem += (exoIn.inputs.numTargetObjs + exoIn.inputs.numFieldObjs)*sizeof(int)/1e6;
            targPropJobsMem = exoIn.inputs.numTargetObjs*sizeof(int)/1e6;
            fieldPropJobsMem = exoIn.inputs.numFieldObjs*sizeof(int)/1e6;
    
            // allocate memory for max trajectory comparison jobs
            trajCompJobs = calloc(exoIn.combs, sizeof(int));
            MPI_Barrier(MPI_COMM_WORLD);
            exoIn.allocatedMem += exoIn.combs*sizeof(int)/1e6;
            trajCompJobsMem = exoIn.combs*sizeof(int)/1e6;

            // allocate memory for exit codes
            targExitCodes = calloc(exoIn.inputs.numTargetObjs, sizeof(int));
            fieldExitCodes = calloc(exoIn.inputs.numFieldObjs, sizeof(int));
            MPI_Barrier(MPI_COMM_WORLD);
            exoIn.allocatedMem += (exoIn.inputs.numTargetObjs + exoIn.inputs.numFieldObjs)*sizeof(int)/1e6;

            // populate propagation job arrays
            initPropArr(targPropJobs, exoIn.inputs.numTargetObjs);
            initPropArr(fieldPropJobs, exoIn.inputs.numFieldObjs);

            // populate comparison job arrays
            initTrajCompArr(trajCompJobs, exoIn.combs);

            // intialize output directory
            initDirOut(exoIn.inputs.outputDir, exoIn.inputs.numIntervals, exoIn.rank);

            // allocate memory for trajectory lengths
            targTrajLengths = calloc(exoIn.inputs.numTargetObjs, sizeof(int));
            fieldTrajLengths = calloc(exoIn.inputs.numFieldObjs, sizeof(int));
            MPI_Barrier(MPI_COMM_WORLD);
            exoIn.allocatedMem += (exoIn.inputs.numTargetObjs + exoIn.inputs.numFieldObjs)*sizeof(int)/1e6;
            
            // print memory allocation estimate 1
            if(deltaT == 0)
            {
                MPI_Allreduce(&exoIn.allocatedMem, &memEstimate, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
                if(exoIn.rank == 0)
                    printf("Minmum memory requirement(gigabytes) 1 = %f\n",(memEstimate/1e+3));
            }
            

            // collision array
            C = malloc(sizeof(struct contigDouble));
            locApproaches = malloc(sizeof(struct contigInt));
            sepArr = malloc(sizeof(struct contigDouble));
            mjdArr = malloc(sizeof(struct contigDouble));
            if(exoIn.rank == 0)
                allocContigDouble(C, 100*4);
            else
                allocContigDouble(C, 1);
            allocContigInt(locApproaches, 100);
            allocContigDouble(sepArr, 100);
            allocContigDouble(mjdArr, 100);
            //C = calloc(combs*4, sizeof(double));
            MPI_Barrier(MPI_COMM_WORLD);
            exoIn.allocatedMem += *(C->mem);
            exoIn.allocatedMem += *(locApproaches->mem);
            exoIn.allocatedMem += *(sepArr->mem);
            exoIn.allocatedMem += *(mjdArr->mem);

            // print memory allocation estimate 2
            if(deltaT == 0)
            {
                MPI_Allreduce(&exoIn.allocatedMem, &memEstimate, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
                if(exoIn.rank == 0)
                    printf("Minmum memory requirement(gigabytes) 2 = %f\n",(memEstimate/1e+3));
            }

            // number of jobs
            numTargJobs = exoIn.inputs.numTargetObjs;
            numFieldJobs = exoIn.inputs.numFieldObjs;
            numCompJobs = exoIn.combs;
            MPI_Barrier(MPI_COMM_WORLD);

            // global mapping
            globTargetMap = calloc(exoIn.inputs.numTargetObjs, sizeof(int));
            globFieldMap = calloc(exoIn.inputs.numFieldObjs, sizeof(int));
            exoIn.allocatedMem += (exoIn.inputs.numFieldObjs + exoIn.inputs.numTargetObjs)*sizeof(int)/1e6;

            // initialize 1 to 1 global mapping
            // printf("initialize global mapping\n");
            initMapping(globTargetMap, exoIn.inputs.numTargetObjs);
            initMapping(globFieldMap, exoIn.inputs.numFieldObjs);

            // alloc mem for trajectory structs
            targetTraj = malloc(sizeof(struct doubleSemi));
            fieldTraj = malloc(sizeof(struct doubleSemi));
            exoIn.allocatedMem += 2*sizeof(struct doubleSemi)/1e6;

            // allocate mem for reds
            reds = malloc(sizeof(struct redStates));
            reds2 = malloc(sizeof(struct redStates));
            reds3 = malloc(sizeof(struct redStates));
            reds4 = malloc(sizeof(struct redStates));
            reds5 = malloc(sizeof(struct redStates));

            // save previous number of combs
            prevAppr1 = exoIn.combs;
            
            // print memory allocation estimate 3
            if(deltaT == 0)
            {
                MPI_Allreduce(&exoIn.allocatedMem, &memEstimate, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
                if(exoIn.rank == 0)
                    printf("Minmum memory requirement(gigabytes) 3 = %f\n",(memEstimate/1e+3));
            }
        }


        // calculate maximum velocity
        // printf("calculate max velocity\n");
        vmax = calcVmax(exoIn.targetStates, exoIn.fieldStates, exoIn.inputs.numTargetObjs, exoIn.inputs.numFieldObjs, exoIn.inputs.endogenous);

        // calculate tstep for first round
        // printf("calculate tstep for first round\n");
        tstep1 = ((exoIn.d1/vmax)/2)/60/60/24;
        
        // calculate max points
        // printf("calculate max points\n");
        partialTspan = exoIn.inputs.tspan/exoIn.inputs.numIntervals;
        mxpts = calcMxpts(partialTspan, tstep1);
        if(exoIn.rank == 0)
            printf("vmax = %f ||| mxpts = %d\n",vmax,mxpts);

        // initialize reds 
        if (deltaT == 0)
        {
            initReds(reds, &buff, numTargJobs, numFieldJobs, mxpts*10, exoIn.rank);
            initReds(reds2, &buff2, numTargJobs, numFieldJobs, mxpts*10, exoIn.rank);
            initReds(reds3, &buff3, numTargJobs, numFieldJobs, mxpts*10, exoIn.rank);
            initReds(reds4, &buff4, numTargJobs, numFieldJobs, mxpts*10, exoIn.rank);
            initReds(reds5, &buff5, numTargJobs, numFieldJobs, mxpts*10, exoIn.rank);
            exoIn.allocatedMem += 5*sizeof(struct redStates);
            exoIn.allocatedMem += buff + buff2 + buff3;
            exoIn.allocatedMem += buff4 + buff5;
        }

        // print memory allocation estimate 4
        if(deltaT == 0)
        {
            MPI_Allreduce(&exoIn.allocatedMem, &memEstimate, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
            if(exoIn.rank == 0)
                printf("Minmum memory requirement(gigabytes) 4 = %f\n",(memEstimate/1e+3));
        }
        
        //DEBUG
        if (debug == 1)
        {
            printf("VMAX: %.15f", vmax);
            printf("TIMESTEP: %.15f",tstep1);
            printf("MAXPOINTS: %d\n",mxpts);
        }

        // ==========================================================
        // FILTER 1 =================================================
        // ==========================================================
        
        // calculate number of jobs per cpu for 1st round of filter
        // printf("calculate number of jobs per cpu for 1st round of filter\n");
        exoJobs1 = calcExoJobLengths(exoIn.inputs.numCores, numTargJobs, numFieldJobs, numCompJobs);

        // allocate trajectories
        if ( exoIn.rank == 0)
            printf("allocating trajectories\n");
        if (exoIn.rank == 0)
        {
            targL = exoIn.inputs.numTargetObjs;
            fieldL = exoIn.inputs.numFieldObjs;
        }
        else
        {
            targL = 1;
            fieldL = 1;

        }
        if (deltaT == 0)
        {
            // printf("ranke %d allocating on  %d - %d - %d\n",rank,mxpts,targL,fieldL);
            alloc3DDoubleSemi(targetTraj, targL, mxpts*2, 12);
            alloc3DDoubleSemi(fieldTraj, fieldL, mxpts*2, 12);
            exoIn.allocatedMem += ( *(targetTraj->mem) + *(fieldTraj->mem) );
        }
        else
        {
            // printf("ranke %d reallocating on  %d - %d - %d\n",rank,mxpts,targL,fieldL);
            reallocDouble3Dsemi(targetTraj,targL, mxpts);
            reallocDouble3Dsemi(fieldTraj,fieldL, mxpts);
            exoIn.allocatedMem += ( *(targetTraj->mem) + *(fieldTraj->mem) );
            exoIn.allocatedMem -= ( *(targetTraj->prevmem) + *(fieldTraj->prevmem) );
            // printf("done on ranke %d\n",rank);
        }

        // print memory allocation estimate main
        if(deltaT == 0)
        {
            MPI_Allreduce(&exoIn.allocatedMem, &memEstimate, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
            if(exoIn.rank == 0)
                printf("Minmum memory requirement(gigabytes) = %f\n",(memEstimate/1e+3));
        }
        //break;

        // populate targ prop input 1
        targPropIn.rank            = exoIn.rank;
        targPropIn.jobParams       = exoJobs1;
        targPropIn.input           = exoIn.inputs;
        targPropIn.targetTraj      = targetTraj->arr;
        targPropIn.targetStates    = exoIn.targetStates;
        targPropIn.mxpts           = mxpts; 
        targPropIn.partialTspan    = partialTspan; 
        targPropIn.tstep           = tstep1; 
        targPropIn.targTrajLengths = targTrajLengths; 
        targPropIn.numJobs         = numTargJobs; 
        targPropIn.targPropJobs    = targPropJobs;
        targPropIn.targExitCodes   = targExitCodes; 
        targPropIn.lenSet          = exoIn.inputs.numTargetObjs; 
        targPropIn.bugtag          = 100; 
        targPropIn.cpus            = exoIn.numtasks;

        printf("tstep1 = %.20f\n",tstep1);

        // populate field prop input 1
        fieldPropIn.rank             = exoIn.rank;
        fieldPropIn.jobParams        = exoJobs1;
        fieldPropIn.input            = exoIn.inputs;
        fieldPropIn.fieldTraj        = fieldTraj->arr;
        fieldPropIn.fieldStates      = exoIn.fieldStates;
        fieldPropIn.mxpts            = mxpts; 
        fieldPropIn.partialTspan     = partialTspan; 
        fieldPropIn.tstep            = tstep1; 
        fieldPropIn.fieldTrajLengths = fieldTrajLengths; 
        fieldPropIn.numJobs          = numFieldJobs; 
        fieldPropIn.fieldPropJobs    = fieldPropJobs;
        fieldPropIn.fieldExitCodes   = fieldExitCodes; 
        fieldPropIn.lenSet           = exoIn.inputs.numFieldObjs; 
        fieldPropIn.bugtag           = 100; 
        fieldPropIn.cpus             = exoIn.numtasks;

        // propagate 1st round of target objects
        if ( exoIn.rank == 0 )
            printf("propagate 1st round of target objects -> %d\n",mxpts);
        propTargObjs(targPropIn);
        // printf("done with target propagations on rank: %d\n",rank);

        // propagate 1st round of field objects
        if ( exoIn.rank == 0 )
            printf("propagate 1st round of field objects -> %d\n",mxpts);
        propFieldObjs(fieldPropIn);
        // printf("done with field prop on rank: %d\n",rank);

        // populate traj comp input 1
        trajCompIn.rank               = exoIn.rank; 
        trajCompIn.targTrajLengths    = targTrajLengths;
        trajCompIn.fieldTrajLengths   = fieldTrajLengths;
        trajCompIn.targetTrajectories = targetTraj->arr; 
        trajCompIn.fieldTrajectories  = fieldTraj->arr; 
        trajCompIn.input              = exoIn.inputs;
        trajCompIn.jobParams          = exoJobs1; 
        trajCompIn.mxpts              = mxpts; 
        trajCompIn.P                  = exoIn.P; 
        trajCompIn.C                  = C; 
        trajCompIn.trajCompJobs       = trajCompJobs; 
        trajCompIn.targetMap          = globTargetMap; 
        trajCompIn.fieldMap           = globFieldMap; 
        trajCompIn.dApproach          = exoIn.d1; 
        trajCompIn.combs              = numCompJobs; 
        trajCompIn.cpus               = exoIn.numtasks; 
        trajCompIn.pairs              = &pairs1; 
        trajCompIn.locApproaches      = locApproaches; 
        trajCompIn.sepArr             = sepArr;
        trajCompIn.mjdArr             = mjdArr; 
        trajCompIn.cpuMem             = &exoIn.allocatedMem; 
        trajCompIn.nextRed            = reds; 
        trajCompIn.targExitCodes      = targExitCodes; 
        trajCompIn.fieldExitCodes     = fieldExitCodes;

        // compare 1st round of trajectories
        if ( exoIn.rank == 0 )
            printf("compare trajectories\n");
        appr1 = compareExoTraj(trajCompIn);

        // populate update input structure
        updateIn.targetTrajectory  = targetTraj->arr; 
        updateIn.targetStates      = exoIn.targetStates; 
        updateIn.targetTrajLengths = targTrajLengths;
        updateIn.fieldTrajectory   = fieldTraj->arr; 
        updateIn.fieldStates       = exoIn.fieldStates;
        updateIn.fieldTrajLengths  = fieldTrajLengths;
        updateIn.targExitCodes     = targExitCodes; 
        updateIn.fieldExitCodes    = fieldExitCodes; 
        updateIn.C                 = C; 
        updateIn.P                 = exoIn.P; 
        updateIn.numTargetObjs     = exoIn.inputs.numTargetObjs; 
        updateIn.numFieldObjs      = exoIn.inputs.numFieldObjs; 
        updateIn.combs             = exoIn.combs; 
        updateIn.targPropJobs      = targPropJobs; 
        updateIn.fieldPropJobs     = fieldPropJobs; 
        updateIn.trajCompJobs      = trajCompJobs; 
        updateIn.rank              = exoIn.rank; 
        updateIn.numCompJobs       = &numCompJobs; 
        updateIn.numTargJobs       = &numTargJobs; 
        updateIn.numFieldJobs      = &numFieldJobs;
        updateIn.totalMem          = &exoIn.allocatedMem; 
        updateIn.trajComJobsMem    = &trajCompJobsMem; 
        updateIn.targPropJobsMem   = &targPropJobsMem; 
        updateIn.fieldPropJobsMem  = &fieldPropJobsMem; 
        
        // exit if no close approaches recorded
        // printf("exit if no close approaches recorded\n");

        if (appr1 == 0)
        {

            if ( exoIn.rank == 0 )
                printf("no close approaches\n");
            updateExoStates(updateIn);
            
            // update memory
            deltaMem = exoIn.allocatedMem - prevMem;
            prevMem = exoIn.allocatedMem;

            // BREAK IF NO MORE JOBS REQUIRED
            if (numCompJobs == 0 || numTargJobs == 0 || numFieldJobs == 0)
            {
                // printf("no more jobs required\n");
                break;
            }
            else {
                // printf("no stage 1 ca in time chunk %d\n",deltaT);
                continue;
            }
        }

        if (exoIn.rank == 0)
            printf("stage 1 collision detected\n");
        MPI_Barrier(MPI_COMM_WORLD);

        // populate reduction input 1
        reduceIn.origTargStates  = exoIn.targetStates;
        reduceIn.origFieldStates = exoIn.fieldStates;
        reduceIn.C               = C;
        reduceIn.nApproach       = appr1;
        reduceIn.numTargObjs     = exoIn.inputs.numTargetObjs;
        reduceIn.numFieldObjs    = exoIn.inputs.numFieldObjs;
        reduceIn.rank            = exoIn.rank;
        reduceIn.d_rec           = exoIn.d2;
        reduceIn.tspan           = partialTspan;
        reduceIn.totalMem        = &exoIn.allocatedMem;
        reduceIn.reds            = reds;
        reduceIn.cpus            = exoIn.numtasks;
        reduceIn.combos          = pairs1;

        // reduce sets 
        if ( exoIn.rank == 0 )
            printf("reducing sets\n");
        reduceExoStates(reduceIn);

        // populate print input 1
        poutIn.targTraj           = targetTraj->arr;
        poutIn.fieldTraj          = fieldTraj->arr;
        poutIn.targStates         = exoIn.targetStates;
        poutIn.fieldStates        = exoIn.fieldStates;
        poutIn.tag                = 1;
        poutIn.idx                = deltaT;
        poutIn.rank               = exoIn.rank;
        poutIn.endo               = exoIn.inputs.endogenous;
        poutIn.numApproaches      = appr1;
        poutIn.targTrajLengths    = targTrajLengths;
        poutIn.fieldTrajLengths   = fieldTrajLengths;
        poutIn.numTargetObjs      = exoIn.inputs.numTargetObjs;
        poutIn.numFieldObjs       = exoIn.inputs.numFieldObjs;
        poutIn.C                  = C->arr;
        poutIn.ogTargetTraj       = targetTraj->arr;
        poutIn.ogFieldTraj        = fieldTraj->arr;
        poutIn.ogTargTrajLengths  = targTrajLengths;
        poutIn.ogFieldTrajLengths = fieldTrajLengths;
        poutIn.ogNumTargetObjs    = exoIn.inputs.numTargetObjs;
        poutIn.ogNumFieldObjs     = exoIn.inputs.numFieldObjs;
        poutIn.targMapping        = globTargetMap;
        poutIn.fieldMapping       = globFieldMap;
        poutIn.fieldStatus        = reds->fieldStatus->arr;
        poutIn.targStatus         = reds->targStatus->arr; 
    
        // print output for filter 1
        if (exoIn.inputs.print1)
        {
            if (exoIn.rank == 0)
                printf("printing stage 1 output\n");
            printOutput(poutIn, exoIn.inputs.outputDir);
        }

        // synchronize
        MPI_Barrier(MPI_COMM_WORLD);

        // ==========================================================
        // FILTER 2 =================================================
        // ==========================================================

        // break condition
        if (exoIn.inputs.numFilters < 2)
        {
            updateExoStates(updateIn);
            continue;
        }


        //calculate number of jobs per cpu for 2nd round of filter
        if ( exoIn.rank == 0 )
            printf("calculate number of jobs per cpu for 2nd round of filter on core: %d\n",exoIn.rank);
        exoJobs2 = calcExoJobLengths(exoIn.inputs.numCores, *(reds->numTargetObjs), *(reds->numFieldObjs), pairs1);

        // populate targ prop input 2
        targPropIn.jobParams       = exoJobs2;
        targPropIn.targetTraj      = reds->targetTraj->arr;
        targPropIn.targetStates    = reds->targetStates->arr;
        targPropIn.mxpts           = *(reds->mxpts); 
        targPropIn.tstep           = *(reds->tstep); 
        targPropIn.targTrajLengths = reds->targetTrajLengths->arr; 
        targPropIn.numJobs         = *(reds->numTargetObjs); 
        targPropIn.targPropJobs    = reds->targetPropJobs->arr;
        targPropIn.targExitCodes   = reds->targExitCodes->arr; 
        targPropIn.lenSet          = *(reds->numTargetObjs); 

        printf("tstep2 = %.20f\n",*(reds->tstep));

        // populate field prop input 2
        fieldPropIn.jobParams        = exoJobs2;
        fieldPropIn.fieldTraj        = reds->fieldTraj->arr;
        fieldPropIn.fieldStates      = reds->fieldStates->arr;
        fieldPropIn.mxpts            = *(reds->mxpts); 
        fieldPropIn.tstep            = *(reds->tstep); 
        fieldPropIn.fieldTrajLengths = reds->fieldTrajLengths->arr; 
        fieldPropIn.numJobs          = *(reds->numFieldObjs); 
        fieldPropIn.fieldPropJobs    = reds->fieldPropJobs->arr;
        fieldPropIn.fieldExitCodes   = reds->fieldExitCodes->arr; 
        fieldPropIn.lenSet           = *(reds->numFieldObjs); 

        // propagate 2nd round of objects
        if ( exoIn.rank == 0 )
            printf("propagate 2nd round target of objects -> %d\n", *(reds->mxpts));
        propTargObjs(targPropIn);
        if ( exoIn.rank == 0 )
            printf("propagate 2nd round of field objects -> %d\n", *(reds->mxpts));
        propFieldObjs(fieldPropIn);

        // synchronize processors
        MPI_Barrier(MPI_COMM_WORLD);

        // populate traj comp input 2
        trajCompIn.targTrajLengths    = reds->targetTrajLengths->arr;
        trajCompIn.fieldTrajLengths   = reds->fieldTrajLengths->arr;
        trajCompIn.targetTrajectories = reds->targetTraj->arr; 
        trajCompIn.fieldTrajectories  = reds->fieldTraj->arr; 
        trajCompIn.jobParams          = exoJobs2; 
        trajCompIn.mxpts              = *(reds->mxpts); 
        trajCompIn.P                  = reds->P->arr; 
        trajCompIn.C                  = reds->C; 
        trajCompIn.trajCompJobs       = reds->trajCompJobs->arr; 
        trajCompIn.targetMap          = reds->targetMapping->arr; 
        trajCompIn.fieldMap           = reds->fieldMapping->arr; 
        trajCompIn.dApproach          = exoIn.d2; 
        trajCompIn.combs              = pairs1; 
        trajCompIn.pairs              = &pairs2; 
        trajCompIn.locApproaches      = reds->locApproaches; 
        trajCompIn.sepArr             = reds->sepArr;
        trajCompIn.mjdArr             = reds->mjdArr; 
        trajCompIn.nextRed            = reds2; 
        trajCompIn.targExitCodes      = reds->targExitCodes->arr; 
        trajCompIn.fieldExitCodes     = reds->fieldExitCodes->arr;

        // compare trajectories
        if ( exoIn.rank == 0 )
            printf("compare trajectories\n");
        appr2 = compareExoTraj(trajCompIn);
        
        // exit if no close approaches recorded
        if (appr2 == 0)
        {
            if ( exoIn.rank == 0 )
                printf("no close approaches\n");
            updateExoStates(updateIn);

            // update memory
            deltaMem = exoIn.allocatedMem - prevMem;
            prevMem = exoIn.allocatedMem;

            // BREAK IF NO MORE JOBS REQUIRED
            if (numCompJobs == 0 || numTargJobs == 0 || numFieldJobs == 0)
            {
                // printf("no more jobs required\n");
                break;
            }
            else {
                continue;
            }
        }

        if (exoIn.rank == 0)
            printf("stage 2 collision detected\n");
        
        // populate reduction input 2
        reduceIn.C               = reds->C;
        reduceIn.nApproach       = appr2;
        reduceIn.d_rec           = exoIn.d3;
        reduceIn.reds            = reds2;
        reduceIn.combos          = pairs2;
        
        // reduce round 2 sets
        if ( exoIn.rank == 0 )
            printf("reducing round 2 sets\n");
        reduceExoStates(reduceIn);

        // populate print input 2
        poutIn.targTraj           = reds->targetTraj->arr;
        poutIn.fieldTraj          = reds->fieldTraj->arr;
        poutIn.targStates         = reds->targetStates->arr;
        poutIn.fieldStates        = reds->fieldStates->arr;
        poutIn.tag                = 2;
        poutIn.numApproaches      = appr2;
        poutIn.targTrajLengths    = reds->targetTrajLengths->arr;
        poutIn.fieldTrajLengths   = reds->fieldTrajLengths->arr;
        poutIn.C                  = reds->C->arr;
        poutIn.targMapping        = reds->targetMapping->arr;
        poutIn.fieldMapping       = reds->fieldMapping->arr;
        poutIn.fieldStatus        = reds2->fieldStatus->arr;
        poutIn.targStatus         = reds2->targStatus->arr; 

        // print output for filter 2
        if (exoIn.inputs.print2)
        {   
            if ( exoIn.rank == 0 )
                printf("print round 2 filter\n"); 
            printOutput(poutIn, exoIn.inputs.outputDir);
        }

        // synchronize
        MPI_Barrier(MPI_COMM_WORLD);
    

        // ==========================================================
        // FILTER 3 =================================================
        // ==========================================================

        // break condition
        if (exoIn.inputs.numFilters < 3)
        {
            updateExoStates(updateIn);
            continue;
        }

        // calculate job parameters
        // printf("calculate round 3 parameters\n");
        exoJobs3 = calcExoJobLengths(exoIn.inputs.numCores, *(reds2->numTargetObjs), *(reds2->numFieldObjs), pairs2);

        // populate targ prop input 3
        targPropIn.jobParams       = exoJobs3;
        targPropIn.targetTraj      = reds2->targetTraj->arr;
        targPropIn.targetStates    = reds2->targetStates->arr;
        targPropIn.mxpts           = *(reds2->mxpts); 
        targPropIn.tstep           = *(reds2->tstep); 
        targPropIn.targTrajLengths = reds2->targetTrajLengths->arr; 
        targPropIn.numJobs         = *(reds2->numTargetObjs); 
        targPropIn.targPropJobs    = reds2->targetPropJobs->arr;
        targPropIn.targExitCodes   = reds2->targExitCodes->arr; 
        targPropIn.lenSet          = *(reds2->numTargetObjs);

        printf("tstep3 = %.20f\n",*(reds2->tstep));

        // populate field prop input 3
        fieldPropIn.jobParams        = exoJobs3;
        fieldPropIn.fieldTraj        = reds2->fieldTraj->arr;
        fieldPropIn.fieldStates      = reds2->fieldStates->arr;
        fieldPropIn.mxpts            = *(reds2->mxpts); 
        fieldPropIn.tstep            = *(reds2->tstep); 
        fieldPropIn.fieldTrajLengths = reds2->fieldTrajLengths->arr; 
        fieldPropIn.numJobs          = *(reds2->numFieldObjs); 
        fieldPropIn.fieldPropJobs    = reds2->fieldPropJobs->arr;
        fieldPropIn.fieldExitCodes   = reds2->fieldExitCodes->arr; 
        fieldPropIn.lenSet           = *(reds2->numFieldObjs);

        // propagate 3rd round of target objects
        if ( exoIn.rank == 0 )
        {
            printf("propagate 3rd round target of objects\n");
            //printf("reqd pts: %d - - - allocd pts: %d",*(reds2->mxpts), *(reds2->targetTraj->rows));
        }
        propTargObjs(targPropIn);

        // propagate 3rd round of field objects
        if ( exoIn.rank == 0 )
            printf("propagate 3rd round field objects -> %d\n", *(reds2->mxpts));
        propFieldObjs(fieldPropIn);            

        // synchronize
        MPI_Barrier(MPI_COMM_WORLD);

        // populate traj comp input 3
        trajCompIn.targTrajLengths    = reds2->targetTrajLengths->arr;
        trajCompIn.fieldTrajLengths   = reds2->fieldTrajLengths->arr;
        trajCompIn.targetTrajectories = reds2->targetTraj->arr; 
        trajCompIn.fieldTrajectories  = reds2->fieldTraj->arr; 
        trajCompIn.jobParams          = exoJobs3; 
        trajCompIn.mxpts              = *(reds2->mxpts); 
        trajCompIn.P                  = reds2->P->arr; 
        trajCompIn.C                  = reds2->C; 
        trajCompIn.trajCompJobs       = reds2->trajCompJobs->arr; 
        trajCompIn.targetMap          = reds2->targetMapping->arr; 
        trajCompIn.fieldMap           = reds2->fieldMapping->arr; 
        trajCompIn.dApproach          = exoIn.d3; 
        trajCompIn.combs              = pairs2; 
        trajCompIn.pairs              = &pairs3; 
        trajCompIn.locApproaches      = reds2->locApproaches; 
        trajCompIn.sepArr             = reds2->sepArr;
        trajCompIn.mjdArr             = reds2->mjdArr; 
        trajCompIn.nextRed            = reds3; 
        trajCompIn.targExitCodes      = reds2->targExitCodes->arr; 
        trajCompIn.fieldExitCodes     = reds2->fieldExitCodes->arr;

        // compare round 3 trajectories
        if ( exoIn.rank == 0 )
            printf("compare round 3 trajectories\n");
        appr3 = compareExoTraj(trajCompIn);
        
        // exit if no close approaches recorded
        if (appr3 == 0)
        {
            if ( exoIn.rank == 0 )
                printf("no close approaches recorded\n");
            updateExoStates(updateIn);

            // update memory
            deltaMem = exoIn.allocatedMem - prevMem;
            prevMem = exoIn.allocatedMem;

            // BREAK IF NO MORE JOBS REQUIRED
            if (numCompJobs == 0 || numTargJobs == 0 || numFieldJobs == 0)
            {
                // printf("no more jobs required\n");
                break;
            }
            else {
                continue;
            }
        }

        if (exoIn.rank == 0)
            printf("stage 3 collision detected\n");

        // populate reduction input 3
        reduceIn.C               = reds2->C;
        reduceIn.nApproach       = appr3;
        reduceIn.d_rec           = exoIn.d4;
        reduceIn.reds            = reds3;
        reduceIn.combos          = pairs3;

        // reduce round 3 sets
        if ( exoIn.rank == 0 )
            printf("reduce round 3 exo states\n");
        reduceExoStates(reduceIn);

        // populate print input 3
        poutIn.targTraj           = reds2->targetTraj->arr;
        poutIn.fieldTraj          = reds2->fieldTraj->arr;
        poutIn.targStates         = reds2->targetStates->arr;
        poutIn.fieldStates        = reds2->fieldStates->arr;
        poutIn.tag                = 3;
        poutIn.numApproaches      = appr3;
        poutIn.targTrajLengths    = reds2->targetTrajLengths->arr;
        poutIn.fieldTrajLengths   = reds2->fieldTrajLengths->arr;
        poutIn.C                  = reds2->C->arr;
        poutIn.targMapping        = reds2->targetMapping->arr;
        poutIn.fieldMapping       = reds2->fieldMapping->arr;
        poutIn.fieldStatus        = reds3->fieldStatus->arr;
        poutIn.targStatus         = reds3->targStatus->arr; 

        // print output for filter 3
        if (exoIn.inputs.print3)
        {
            if ( exoIn.rank == 0 )
                printf("printing round 3 output\n");
            printOutput(poutIn, exoIn.inputs.outputDir);
        }
        
        // synchronize
        MPI_Barrier(MPI_COMM_WORLD);

        // ==========================================================
        // FILTER 4 =================================================
        // ==========================================================

        // break condition
        if (exoIn.inputs.numFilters < 4)
        {
            updateExoStates(updateIn);
            continue;
        }

        // calculate job parameters
        // printf("calculate round 3 parameters\n");
        exoJobs4 = calcExoJobLengths(exoIn.inputs.numCores, *(reds3->numTargetObjs), *(reds3->numFieldObjs), pairs3);
        
        // populate targ prop input 4
        targPropIn.jobParams       = exoJobs4;
        targPropIn.targetTraj      = reds3->targetTraj->arr;
        targPropIn.targetStates    = reds3->targetStates->arr;
        targPropIn.mxpts           = *(reds3->mxpts); 
        targPropIn.tstep           = *(reds3->tstep); 
        targPropIn.targTrajLengths = reds3->targetTrajLengths->arr; 
        targPropIn.numJobs         = *(reds3->numTargetObjs); 
        targPropIn.targPropJobs    = reds3->targetPropJobs->arr;
        targPropIn.targExitCodes   = reds3->targExitCodes->arr; 
        targPropIn.lenSet          = *(reds3->numTargetObjs);

        printf("tstep4 = %.20f\n",*(reds3->tstep));

        // populate field prop input 4
        fieldPropIn.jobParams        = exoJobs4;
        fieldPropIn.fieldTraj        = reds3->fieldTraj->arr;
        fieldPropIn.fieldStates      = reds3->fieldStates->arr;
        fieldPropIn.mxpts            = *(reds3->mxpts); 
        fieldPropIn.tstep            = *(reds3->tstep); 
        fieldPropIn.fieldTrajLengths = reds3->fieldTrajLengths->arr; 
        fieldPropIn.numJobs          = *(reds3->numFieldObjs); 
        fieldPropIn.fieldPropJobs    = reds3->fieldPropJobs->arr;
        fieldPropIn.fieldExitCodes   = reds3->fieldExitCodes->arr; 
        fieldPropIn.lenSet           = *(reds3->numFieldObjs);

        // propagate 4th round of objects
        if ( exoIn.rank == 0 )
        {
            printf("propagate 4th round target of objects\n");
            //printf("reqd pts: %d - - - allocd pts: %d",*(reds2->mxpts), *(reds2->targetTraj->rows));
        }
        propTargObjs(targPropIn);
        if ( exoIn.rank == 0 )
            printf("propagate 4th round field objects -> %d\n", *(reds3->mxpts));
        propFieldObjs(fieldPropIn);            

        // populate traj comp input 4
        trajCompIn.targTrajLengths    = reds3->targetTrajLengths->arr;
        trajCompIn.fieldTrajLengths   = reds3->fieldTrajLengths->arr;
        trajCompIn.targetTrajectories = reds3->targetTraj->arr; 
        trajCompIn.fieldTrajectories  = reds3->fieldTraj->arr; 
        trajCompIn.jobParams          = exoJobs4; 
        trajCompIn.mxpts              = *(reds3->mxpts); 
        trajCompIn.P                  = reds3->P->arr; 
        trajCompIn.C                  = reds3->C; 
        trajCompIn.trajCompJobs       = reds3->trajCompJobs->arr; 
        trajCompIn.targetMap          = reds3->targetMapping->arr; 
        trajCompIn.fieldMap           = reds3->fieldMapping->arr; 
        trajCompIn.dApproach          = exoIn.d4; 
        trajCompIn.combs              = pairs3; 
        trajCompIn.pairs              = &pairs4; 
        trajCompIn.locApproaches      = reds3->locApproaches; 
        trajCompIn.sepArr             = reds3->sepArr;
        trajCompIn.mjdArr             = reds3->mjdArr; 
        trajCompIn.nextRed            = reds4; 
        trajCompIn.targExitCodes      = reds3->targExitCodes->arr; 
        trajCompIn.fieldExitCodes     = reds3->fieldExitCodes->arr;
        
        // compare trajectories
        if ( exoIn.rank == 0 )
            printf("compare round 4 trajectories\n");
        appr4 = compareExoTraj(trajCompIn);
        
        // exit if no close approaches recorded
        if (appr4 == 0)
        {
            if ( exoIn.rank == 0 )
                printf("no close approaches recorded\n");
            updateExoStates(updateIn);

            // update memory
            deltaMem = exoIn.allocatedMem - prevMem;
            prevMem = exoIn.allocatedMem;

            // BREAK IF NO MORE JOBS REQUIRED
            if (numCompJobs == 0 || numTargJobs == 0 || numFieldJobs == 0)
            {
                // printf("no more jobs required\n");
                break;
            }
            else {
                continue;
            }
        }

        if (exoIn.rank == 0)
            printf("stage 4 collision detected\n");

        // populate reduction input 4
        reduceIn.C               = reds3->C;
        reduceIn.nApproach       = appr4;
        reduceIn.d_rec           = exoIn.d5;
        reduceIn.reds            = reds4;
        reduceIn.combos          = pairs4;
        
        // reduce sets
        if ( exoIn.rank == 0 )
            printf("reduce round 4 exo states -- with %d pairs\n",pairs4);
        reduceExoStates(reduceIn);

        // populate print input 4
        poutIn.targTraj           = reds3->targetTraj->arr;
        poutIn.fieldTraj          = reds3->fieldTraj->arr;
        poutIn.targStates         = reds3->targetStates->arr;
        poutIn.fieldStates        = reds3->fieldStates->arr;
        poutIn.tag                = 4;
        poutIn.numApproaches      = appr4;
        poutIn.targTrajLengths    = reds3->targetTrajLengths->arr;
        poutIn.fieldTrajLengths   = reds3->fieldTrajLengths->arr;
        poutIn.C                  = reds3->C->arr;
        poutIn.targMapping        = reds3->targetMapping->arr;
        poutIn.fieldMapping       = reds3->fieldMapping->arr;
        poutIn.fieldStatus        = reds4->fieldStatus->arr;
        poutIn.targStatus         = reds4->targStatus->arr; 
        
        // print output for filter 4
        if (exoIn.inputs.print4)
        {
            if ( exoIn.rank == 0 )
                printf("printing round 4 output\n");
            printOutput(poutIn, exoIn.inputs.outputDir);
        }

        // synchronize
        MPI_Barrier(MPI_COMM_WORLD);

        // ==========================================================
        // FILTER 5 =================================================
        // ==========================================================

        // break condition
        if (exoIn.inputs.numFilters < 5)
        {
            updateExoStates(updateIn);
            continue;
        }

        // calculate job parameters
        // printf("calculate round 3 parameters\n");
        exoJobs5 = calcExoJobLengths(exoIn.inputs.numCores, *(reds4->numTargetObjs), *(reds4->numFieldObjs), pairs4);

        // populate targ prop input 5
        targPropIn.jobParams       = exoJobs5;
        targPropIn.targetTraj      = reds4->targetTraj->arr;
        targPropIn.targetStates    = reds4->targetStates->arr;
        targPropIn.mxpts           = *(reds4->mxpts); 
        targPropIn.tstep           = *(reds4->tstep); 
        targPropIn.targTrajLengths = reds4->targetTrajLengths->arr; 
        targPropIn.numJobs         = *(reds4->numTargetObjs); 
        targPropIn.targPropJobs    = reds4->targetPropJobs->arr;
        targPropIn.targExitCodes   = reds4->targExitCodes->arr; 
        targPropIn.lenSet          = *(reds4->numTargetObjs);

        printf("tstep5 = %.20f\n",*(reds4->tstep));

        // populate field prop input 5
        fieldPropIn.jobParams        = exoJobs5;
        fieldPropIn.fieldTraj        = reds4->fieldTraj->arr;
        fieldPropIn.fieldStates      = reds4->fieldStates->arr;
        fieldPropIn.mxpts            = *(reds4->mxpts); 
        fieldPropIn.tstep            = *(reds4->tstep); 
        fieldPropIn.fieldTrajLengths = reds4->fieldTrajLengths->arr; 
        fieldPropIn.numJobs          = *(reds4->numFieldObjs); 
        fieldPropIn.fieldPropJobs    = reds4->fieldPropJobs->arr;
        fieldPropIn.fieldExitCodes   = reds4->fieldExitCodes->arr; 
        fieldPropIn.lenSet           = *(reds4->numFieldObjs);

        // propagate 5th round of target objects
        if ( exoIn.rank == 0 )
        {
            printf("propagate 5th round target of objects\n");
            //printf("reqd pts: %d - - - allocd pts: %d",*(reds2->mxpts), *(reds2->targetTraj->rows));
        }
        propTargObjs(targPropIn);

        // propagate 5th round of field objects
        if ( exoIn.rank == 0 )
            printf("propagate 5th round field objects -> %d\n", *(reds4->mxpts));
        propFieldObjs(fieldPropIn);            
        
        // populate traj comp input 5
        trajCompIn.targTrajLengths    = reds4->targetTrajLengths->arr;
        trajCompIn.fieldTrajLengths   = reds4->fieldTrajLengths->arr;
        trajCompIn.targetTrajectories = reds4->targetTraj->arr; 
        trajCompIn.fieldTrajectories  = reds4->fieldTraj->arr; 
        trajCompIn.jobParams          = exoJobs5; 
        trajCompIn.mxpts              = *(reds4->mxpts); 
        trajCompIn.P                  = reds4->P->arr; 
        trajCompIn.C                  = reds4->C; 
        trajCompIn.trajCompJobs       = reds4->trajCompJobs->arr; 
        trajCompIn.targetMap          = reds4->targetMapping->arr; 
        trajCompIn.fieldMap           = reds4->fieldMapping->arr; 
        trajCompIn.dApproach          = exoIn.d5; 
        trajCompIn.combs              = pairs4; 
        trajCompIn.pairs              = &pairs5; 
        trajCompIn.locApproaches      = reds4->locApproaches; 
        trajCompIn.sepArr             = reds4->sepArr;
        trajCompIn.mjdArr             = reds4->mjdArr; 
        trajCompIn.nextRed            = reds5; 
        trajCompIn.targExitCodes      = reds4->targExitCodes->arr; 
        trajCompIn.fieldExitCodes     = reds4->fieldExitCodes->arr;

        // compare trajectories
        if ( exoIn.rank == 0 )
            printf("compare round 5 trajectories\n");
        appr5 = compareExoTraj(trajCompIn);
        
        // exit if no close approaches recorded
        if (appr5 == 0)
        {
            if ( exoIn.rank == 0 )
                printf("no close approaches recorded\n");
            updateExoStates(updateIn);

            // update memory
            deltaMem = exoIn.allocatedMem - prevMem;
            prevMem = exoIn.allocatedMem;

            // BREAK IF NO MORE JOBS REQUIRED
            if (numCompJobs == 0 || numTargJobs == 0 || numFieldJobs == 0)
            {
                // printf("no more jobs required\n");
                break;
            }
            else {
                continue;
            }
        }

        if (exoIn.rank == 0)
            printf("stage 5 collision detected\n");

        // populate reduction input 5
        reduceIn.C               = reds4->C;
        reduceIn.nApproach       = appr5;
        reduceIn.d_rec           = exoIn.d5;
        reduceIn.reds            = reds5;
        reduceIn.combos          = pairs5;
        
        // reduce sets 5
        if ( exoIn.rank == 0 )
            printf("reduce round 5 exo states\n");
        reduceExoStates(reduceIn);
        
        // populate print input 5
        poutIn.targTraj           = reds4->targetTraj->arr;
        poutIn.fieldTraj          = reds4->fieldTraj->arr;
        poutIn.targStates         = reds4->targetStates->arr;
        poutIn.fieldStates        = reds4->fieldStates->arr;
        poutIn.tag                = 5;
        poutIn.numApproaches      = appr5;
        poutIn.targTrajLengths    = reds4->targetTrajLengths->arr;
        poutIn.fieldTrajLengths   = reds4->fieldTrajLengths->arr;
        poutIn.C                  = reds4->C->arr;
        poutIn.targMapping        = reds4->targetMapping->arr;
        poutIn.fieldMapping       = reds4->fieldMapping->arr;
        poutIn.fieldStatus        = reds5->fieldStatus->arr;
        poutIn.targStatus         = reds5->targStatus->arr; 

        // print output for filter 5
        if (exoIn.inputs.print5)
        {
            if ( exoIn.rank == 0 )
                printf("printing round 5 output\n");
            printOutput(poutIn, exoIn.inputs.outputDir);
        }

        // synchronize
        MPI_Barrier(MPI_COMM_WORLD);

        //UPDATE FOR NEXT ITERATION
        if ( exoIn.rank == 0 )
            printf("updating for next iteration\n");
        updateExoStates(updateIn);
        
        // BREAK IF NO MORE JOBS REQUIRED
        if (numCompJobs == 0 || numTargJobs == 0 || numFieldJobs == 0)
        {
            // printf("no more jobs required\n");
            break;
        }

        // update memory
        deltaMem = exoIn.allocatedMem - prevMem;
        prevMem = exoIn.allocatedMem;


    }

    return 0;
        
}  
 