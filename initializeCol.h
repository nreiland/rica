#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <math.h>
#include <mpi.h>

/* function to calculate maximum Keplerian Velocity */
double calcVmax(double **targetStates, double **fieldStates, int numTargetObjs, int numFieldObjs, int endo)
{
    int i, j;
    double v, a, e, rp, vmax;
    double mu = 3.986004414498200E+05;

    // calculate maximum Keplerian velocity
    a = targetStates[0][1];
    e = targetStates[0][2];
    rp = a*(1 - e);
    vmax = sqrt(mu*(2/rp - 1/a));
    for (i = 1; i < numTargetObjs; i++)
    {
        a = targetStates[i][1];
        e = targetStates[i][2];
        rp = a*(1 - e*e);
        v = sqrt(mu*(2/rp - 1/a));

        if (v < vmax)
            vmax = v;
    }
    if (endo == 0)
    {
        for (i = 0; i < numFieldObjs; i++)
        {
            a = fieldStates[i][1];
            e = fieldStates[i][2];
            rp = a*(1 - e*e);
            v = sqrt(mu*(2/rp - 1/a));

            if (v < vmax)
                vmax = v;
        }
    }
    else if (endo != 1)
    {
        printf("please enter a valid value for ''endogenous'' \n");
        exit(-1);
    }

    return 2*vmax;
    
}

/* function to calculate number of combinations of collision events */
int calcEventCombs(struct Input input)
{
    int i, j;

    // calculate set of collision pairs and events
    int combs = 0;
    if (input.endogenous == 1)
    {
        for (i = 0; i < input.numTargetObjs; i++)
        {
            for (j = i + 1; j < input.numTargetObjs; j++)
                combs++;
        }
        
    }
    else if (input.endogenous == 0)
    {
        for (i = 0; i < input.numTargetObjs; i++)
        {
            for (j = 0; j < input.numFieldObjs; j++)
                combs++;
        }
    }

    return combs;
}

/* function to populate collision events and collision possibilities arrays */
int popP(int **P, struct Input input)
{
    int i, j;
    int combs;

    if (input.endogenous == 1)
    {
        combs = 0;
        for (i = 0; i < input.numTargetObjs; i++)
        {
            for (j = i + 1; j < input.numTargetObjs; j++)
            {
                P[combs][0] = i;
                P[combs][1] = j;
                // C[combs][0] = 0;
                // C[combs][1] = 0;
                // C[combs][2] = 0;
                combs++;
            }
        }
        
    }
    else if (input.endogenous == 0)
    {
        combs = 0;
        for (i = 0; i < input.numTargetObjs; i++)
        {
            for (j = 0; j < input.numFieldObjs; j++)
            {
                P[combs][0] = i;
                P[combs][1] = j;
                // C[combs][0] = 0;
                // C[combs][1] = 0;
                // C[combs][2] = 0;
                combs++;
            }
        }
    }

    return 0;
}

/* calculate length of propagation jobs and trajectory comparison jobs for endogenous case */
struct endoJobParams calcEndoJobLengths(int numCores, int numTargetObjs, int combs)
{
    int remainder;
    struct endoJobParams jobs;
  
    // calculate number of propagation jobs
    remainder = numTargetObjs % numCores;
    jobs.nomNumPropJobs = (numTargetObjs - remainder)/numCores;
    if (remainder == 0)
        jobs.rootNumPropJobs = jobs.nomNumPropJobs;
    else
        jobs.rootNumPropJobs = jobs.nomNumPropJobs + 1;
    jobs.propRemainder = remainder;

    // calculate number of trajectory jobs
    numCores = numCores - 1;
    remainder = combs % numCores;
    jobs.nomNumTrajJobs = (combs - remainder)/numCores;
    if (remainder == 0)
        jobs.rootNumTrajJobs = jobs.nomNumTrajJobs;
    else
        jobs.rootNumTrajJobs = jobs.nomNumTrajJobs + 1;
    jobs.trajRemainder = remainder;

    return jobs;
    
}

/* calculate length of propagation jobs and trajectory comparison jobs for exogenous case */
struct exoJobParams calcExoJobLengths(int numCores, int numTargetObjs, int numFieldObjs, int combs)
{
    int remainder;
    struct exoJobParams jobs;

    // don't give jobs to root rank
    numCores = numCores - 1;
  
    // calculate number of propagation jobs for target objects
    remainder = numTargetObjs % numCores;
    jobs.nomNumTargPropJobs = (numTargetObjs - remainder)/numCores;
    if (remainder == 0)
        jobs.rootNumTargPropJobs = jobs.nomNumTargPropJobs;
    else
        jobs.rootNumTargPropJobs = jobs.nomNumTargPropJobs + 1;
    jobs.targPropRemainder = remainder;

    // calculate number of propagation jobs for field objects
    remainder = numFieldObjs % numCores;
    jobs.nomNumFieldPropJobs = (numFieldObjs - remainder)/numCores;
    if (remainder == 0)
        jobs.rootNumFieldPropJobs = jobs.nomNumFieldPropJobs;
    else
        jobs.rootNumFieldPropJobs = jobs.nomNumFieldPropJobs + 1;
    jobs.fieldPropRemainder = remainder;

    // calculate number of trajectory jobs root is now rank 1
    remainder = combs % numCores;
    jobs.nomNumTrajJobs = (combs - remainder)/numCores;
    if (remainder == 0)
        jobs.rootNumTrajJobs = jobs.nomNumTrajJobs;
    else
        jobs.rootNumTrajJobs = jobs.nomNumTrajJobs + 1;
    jobs.trajRemainder = remainder;

    return jobs;
    
}

/* function to populate propagation jobs array */
int initPropArr(int *propJobs, int numObjs)
{
    int i;

    for (i = 0; i < numObjs; i++)
        propJobs[i] = i;
    
    return 0;
}

/* function to populate comparison jobs array */
int initTrajCompArr(int *trajCompJobs, int length)
{
    int i;

    for (i = 0; i < length; i++)
        trajCompJobs[i] = i;
    
    return 0;
}

/* calculate maximum number of points for integrations */
int calcMxpts(double partialTspan, double tstep)
{
    int mxpts, tratio;
    int remainder;
    //double partialTspan;

    tratio = partialTspan/tstep;
    mxpts = tratio + 3000;

    return mxpts;
    
}

/* function to print trajectory length */
int printTrajLen(int *trajLength, int numObjs)
{
    int i;

    for (i = 0; i < numObjs; i ++)
        printf("%d\n",trajLength[i]);
    
    return 0;
}

/* function to print C array */
int printC(int *C, int combs)
{
    int i;

    for (i = 0; i < combs; i ++)
        printf("%d %d %d %d %d\n", C[i * 5 + 0], C[i * 5 + 1], C[i * 5 + 2],
               C[i * 5 + 3], C[i * 5 + 4]);
    
    return 0;
}

/* function to print P array */
int printP(int **P, int combs)
{
    int i;

    for (i = 0; i < combs; i ++)
        printf("%d %d\n",P[i][0], P[i][1]);
    
    return 0;
}

/* function to free 3d semi contiguous array */
int free3dSemiArray(double **arr, int m)
{
    int i;
    for (i = 0; i < m; i++)
    {
        free(arr[i]);
    }
    free(arr);

    return 0;
}

/* function to initialize mapping */
int initMapping(int *map, int numObjs)
{
    int i;
    for ( i = 0; i < numObjs; i++)
        map[i] = i;
    return 0;
}




