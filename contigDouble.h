// header file

struct contigDouble
{
    int *elements;
    double *arr;
    double *mem;
    double *prevmem;

    
};

/* function to initialize 3d semi contiguous array structure*/
 void allocContigDouble(struct contigDouble *array, int newElements) {

    int i;

    // allocate mem for organizational pointers
    array->elements = malloc(sizeof(int));
    array->mem = malloc(sizeof(double));
    array->prevmem = malloc(sizeof(double));

    // // define pointer values for organization pointers
    *(array->elements) = newElements;

    // allocate memory for main array
    array->arr = malloc(newElements * sizeof(double));
    memset(array->arr, 0, newElements*sizeof(double));

    // count memory
    *(array->mem) = 1*sizeof(int *);
    *(array->mem) += newElements*sizeof(double);
    *(array->mem) += 3*sizeof(double *);
    *(array->mem) = *(array->mem)/1e6;
    *(array->prevmem) = 0;

}

int reallocContigDouble(struct contigDouble *array, int elements) {

    // define vars
    int i;
    int flag = 0;
    int prevElements = *(array->elements);
    int newElements = elements;
    double newmem = 0;
    double prevmem = *(array->mem);

    // define factor of safety
    double fs = 1.0;
    if ( newElements > prevElements )
        newElements = fs*newElements;

    // realloc if number of elements has increased
    if (newElements > prevElements) {
        array->arr = realloc(array->arr, newElements*sizeof(double));
        // memset(array->arr, 0, newElements*sizeof(double));

        flag = 1;
    }
    // set memory to zero if no reallocation
    else {
        // memset(array->arr, 0, prevElements*sizeof(double));

        flag = 2;
    }

    // calculate new memory based on flag

    // newrows
    if ( flag == 1 ) {
        newmem = sizeof(int *);
        newmem += newElements*sizeof(double);
        newmem += 3*sizeof(double *);

        *(array->elements) = newElements;
    }
    // prevrows
    else if (flag == 2) {
        newmem = sizeof(int *);
        newmem += prevElements*sizeof(double);
        newmem += 3*sizeof(double *);

        *(array->elements) = prevElements;
    }

    // save memory
    *(array->prevmem) = prevmem;
    *(array->mem) = newmem/1e6;

    // return int
    return 0;
}