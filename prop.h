#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <math.h>

// #include "contigDouble.h"

/*function definitions*/
extern void thalassasub_( double *MJD0, double *COE0, double *tspan, double *tstep, int *insgrav, int *isun, int *imoon, 
    int *idrag, int *iF107, int *iSRP, int *iephem, int *gdeg, int *gord, int *rmxstep, double *tol, int *imcoll,
    int *eqs, double *SCMass, double *ADrag, double *ASRP, double *CD, double *CR, int *mxpts, int *npts, double cart[], int *tag,
    int *exitcode  );

/* function to propagate target objects */
int propTargObjs(struct exoTargPropInput propIn)
{
    if ( propIn.bugtag == 0 )
        printf("starting propagation - on core: %d\n",propIn.rank);
    // need pointers to what trajectories and states are held by what processor - master will be in charge of this
    // define variables
    int i, j, k, l, idx, njobs, tag, tag2, npts;
    int orbelType, count, objIdx, jobIdx, start;
    int exitcode;
    int exitCodeBuff[propIn.jobParams.rootNumTargPropJobs + 1];
    int locTrajIdx;
    int propJobs[propIn.jobParams.rootNumTargPropJobs + 1];
    int cpuIdx;
    int exitCode;
    int locNumJobs;
    double COE0[6];

    // define MPI Status
    MPI_Status stat;

    // create contiguous derived data type for propagation jobs
    MPI_Datatype trajSend;
    MPI_Datatype ecodeArr;
    MPI_Datatype trajLocation;
    MPI_Type_contiguous(12*propIn.mxpts, MPI_DOUBLE, &trajSend);
    MPI_Type_contiguous(propIn.jobParams.rootNumTargPropJobs + 1, MPI_INT, &ecodeArr);
    MPI_Type_contiguous(propIn.numJobs + 1, MPI_INT, &trajLocation );
    MPI_Type_commit(&trajSend);
    MPI_Type_commit(&ecodeArr);
    MPI_Type_commit(&trajLocation);

    // assign jobs
    cpuIdx = 1;
    int localEntryIdx = 0;
    for(i = 0; i < propIn.numJobs; i++)
    {
        // assign
        if(cpuIdx == propIn.rank)
        {
            propJobs[localEntryIdx] = i;
            localEntryIdx++;
        }

        // update index
        cpuIdx++;

        // reset cpuIdx if it exceeds number of propIn.cpus
        if(cpuIdx == propIn.cpus)
            cpuIdx = 1;

    }

    // save local number of jobs - no jobs on root rank
    locNumJobs = localEntryIdx;


    // MASTER PROCESSOR no longer has jobs - administrator
    if (propIn.rank == 0)
    {
        // collect trajectories
        int runFlag = 1;
        int completedJobs = 0;
        while(runFlag == 1)
        {
            // get object index
            // printf("scanning on root...\n");
            MPI_Recv(&objIdx, 1, MPI_INT, MPI_ANY_SOURCE, MPI_ANY_TAG, MPI_COMM_WORLD, &stat);

            // get trajectory for object, tag - 1
            tag = objIdx;
            // printf("looking for npts of object: %d\n", tag);
            MPI_Recv(&npts, 1, MPI_INT, MPI_ANY_SOURCE, tag, MPI_COMM_WORLD, &stat);
            // printf("looking for exitCode of object: %d\n", tag);
            MPI_Recv(&exitCode, 1, MPI_INT, MPI_ANY_SOURCE, tag, MPI_COMM_WORLD, &stat);
            // printf("looking for trajectory of object: %d\n", tag);
            MPI_Recv(propIn.targetTraj[objIdx], 12*propIn.mxpts, MPI_DOUBLE, MPI_ANY_SOURCE, tag, MPI_COMM_WORLD, &stat);
            propIn.targTrajLengths[objIdx] = npts;
            propIn.targExitCodes[objIdx] = exitCode;
            // printf("COMPLETED OBJ: %d\n",tag);

            // update number of jobs completed
            completedJobs++;

            // exit if all jobs have been completed
            if(completedJobs == propIn.numJobs)
                runFlag = 0; 

        }

    }

    // SLAVE PROCESS
    else
    {
        
        // call thalassa subroutine and propagate
        double MJD0;
        int npts, propStateIdx;
        double SCMass, ADrag, ASRP, CD, CR;

        for (i = 0; i < locNumJobs; i++)
        {

            // get propagation state
            jobIdx = propJobs[i];
            objIdx = propIn.targPropJobs[jobIdx];

            // define object properties (to be placed in for-loop)
            MJD0 = propIn.targetStates[objIdx][0];
            SCMass = propIn.targetStates[objIdx][7];
            ADrag = propIn.targetStates[objIdx][8];
            ASRP = propIn.targetStates[objIdx][9];
            CD = propIn.targetStates[objIdx][10];
            CR = propIn.targetStates[objIdx][11];
            COE0[0] = propIn.targetStates[objIdx][1];
            COE0[1] = propIn.targetStates[objIdx][2];
            COE0[2] = propIn.targetStates[objIdx][3];
            COE0[3] = propIn.targetStates[objIdx][4];
            COE0[4] = propIn.targetStates[objIdx][5];
            COE0[5] = propIn.targetStates[objIdx][6];

            tag2 = objIdx;

            // DEBUG
            // printf("propagating %d on core %d\n", objIdx, rank);
            propIn.input.mxstep = propIn.mxpts;
            double clockStart = clock();
            thalassasub_( &MJD0, &COE0[0], &propIn.partialTspan, &propIn.tstep, &propIn.input.insgrav, &propIn.input.isun, &propIn.input.imoon,
            &propIn.input.idrag, &propIn.input.iF107, &propIn.input.iSRP, &propIn.input.iephem, &propIn.input.gdeg, &propIn.input.gord, &propIn.input.mxstep, 
            &propIn.input.tol, &propIn.input.imcoll, &propIn.input.eqs, &SCMass, &ADrag, &ASRP, &CD, &CR, &propIn.mxpts, &npts, 
            propIn.targetTraj[0], &tag2, &exitcode);

            // check if mxpts was reached
            while (exitcode == -3)
            {
                // update max number of points
                propIn.mxpts += 50000;
                propIn.input.mxstep = propIn.mxpts;

                // repropagate
                thalassasub_( &MJD0, &COE0[0], &propIn.partialTspan, &propIn.tstep, &propIn.input.insgrav, &propIn.input.isun, &propIn.input.imoon,
                &propIn.input.idrag, &propIn.input.iF107, &propIn.input.iSRP, &propIn.input.iephem, &propIn.input.gdeg, &propIn.input.gord, &propIn.input.mxstep, 
                &propIn.input.tol, &propIn.input.imcoll, &propIn.input.eqs, &SCMass, &ADrag, &ASRP, &CD, &CR, &propIn.mxpts, &npts, 
                propIn.targetTraj[0], &tag2, &exitcode);
                }

            double clockEnd = clock();
            double timeUsed = (clockEnd - clockStart)/CLOCKS_PER_SEC;
            printf("%f on job %d on core %d with %d points allocated and %d points used\n",timeUsed, i,propIn.rank,propIn.mxpts,npts);

            // DEBUG
            if (propIn.bugtag == 0)
                printf("successfully retrieved trajectory %d on core %d\n", objIdx, propIn.rank);
            
            // update trajectory length
            propIn.targTrajLengths[objIdx] = npts;

            // fill trajectory with physical parameters
            for (j = 0; j < npts; j++)
            {
                propIn.targetTraj[0][j * 12 + 7] = SCMass;
                propIn.targetTraj[0][j * 12 + 8] = ADrag;
                propIn.targetTraj[0][j * 12 + 9] = ASRP;
                propIn.targetTraj[0][j * 12 + 10] = CD;
                propIn.targetTraj[0][j * 12 + 11] = CR;

            }

            // define exit code
            if (exitcode != 0)
            {
                printf("prop failure on target object %d with exit code %d detected\n",objIdx,exitcode);
                exitCode = 1;
            }
            else
                exitCode = 0;

            // DEBUG
            if (debug == 0)
                printf("saved trajectory %d on core %d\n", objIdx, propIn.rank);

            // send trajectory
            tag = objIdx;
            // printf("sending traj: %d - job: %d from cpu: %d\n", tag, jobIdx, rank);
            MPI_Send(&objIdx, 1, MPI_INT, 0, tag, MPI_COMM_WORLD);
            MPI_Send(&npts, 1, MPI_INT, 0, tag, MPI_COMM_WORLD);
            MPI_Send(&exitCode, 1, MPI_INT, 0, tag, MPI_COMM_WORLD);
            MPI_Send(propIn.targetTraj[0], 1, trajSend, 0, tag, MPI_COMM_WORLD);
             
        }

        

    }

     // synchronize processors
    MPI_Barrier (MPI_COMM_WORLD);

    // send all other trajectory lengths
    MPI_Bcast(propIn.targTrajLengths, propIn.lenSet, MPI_INT, 0, MPI_COMM_WORLD);

    // send all other processors exit codes
    MPI_Bcast(propIn.targExitCodes, propIn.lenSet, MPI_INT, 0, MPI_COMM_WORLD);

    // deallocate MPI derived type
    MPI_Type_free(&trajSend);
    MPI_Type_free(&ecodeArr);
    MPI_Type_free(&trajLocation);


    return 0;

}

/* function to propagate field objects */
int propFieldObjs(struct exoFieldPropInput propIn)
{
   
    // define variables
    int i, j, k, l, idx, njobs, tag, tag2, npts;
    int recPropJobs[propIn.jobParams.rootNumFieldPropJobs + 1];
    int orbelType, count, objIdx, jobIdx, start;
    int exitcode;
    int exitCodeBuff[propIn.jobParams.rootNumFieldPropJobs + 1];
    int propJobs[propIn.jobParams.rootNumFieldPropJobs + 1];
    int cpuIdx;
    int exitCode;
    int locNumJobs;
    double COE0[6];
    int locTrajIdx;

    // define MPI Status
    MPI_Status stat;

    // create contiguous derived data type for propagation jobs
    MPI_Datatype trajSend;
    MPI_Datatype ecodeArr;
    MPI_Type_contiguous(12*propIn.mxpts, MPI_DOUBLE, &trajSend);
    MPI_Type_contiguous(propIn.jobParams.rootNumFieldPropJobs + 1, MPI_INT, &ecodeArr);
    MPI_Type_commit(&trajSend);
    MPI_Type_commit(&ecodeArr);

    // assign jobs
    cpuIdx = 1;
    int localEntryIdx = 0;
    for(i = 0; i < propIn.numJobs; i++)
    {
        // assign
        if(cpuIdx == propIn.rank)
        {
            propJobs[localEntryIdx] = i;
            localEntryIdx++;
        }

        // update index
        cpuIdx++;

        // reset cpuIdx if it exceeds number of propIn.cpus
        if(cpuIdx == propIn.cpus)
            cpuIdx = 1;

    }

    // save local number of jobs - no jobs on root rank
    locNumJobs = localEntryIdx;

    // MASTER PROCESSOR
    if (propIn.rank == 0)
    {
        // collect trajectories
        int runFlag = 1;
        int completedJobs = 0;
        while(runFlag == 1)
        {
            // get object index
            // printf("scanning on root...\n");
            MPI_Recv(&objIdx, 1, MPI_INT, MPI_ANY_SOURCE, MPI_ANY_TAG, MPI_COMM_WORLD, &stat);

            // get trajectory for object, tag - 1
            tag = objIdx;
            // printf("looking for npts of object: %d\n", tag);
            MPI_Recv(&npts, 1, MPI_INT, MPI_ANY_SOURCE, tag, MPI_COMM_WORLD, &stat);
            // printf("looking for exitCode of object: %d\n", tag);
            MPI_Recv(&exitCode, 1, MPI_INT, MPI_ANY_SOURCE, tag, MPI_COMM_WORLD, &stat);
            // printf("looking for trajectory of object: %d\n", tag);
            MPI_Recv(propIn.fieldTraj[objIdx], 12*propIn.mxpts, MPI_DOUBLE, MPI_ANY_SOURCE, tag, MPI_COMM_WORLD, &stat);
            propIn.fieldTrajLengths[objIdx] = npts;
            propIn.fieldExitCodes[objIdx] = exitCode;
            // printf("COMPLETED OBJ: %d\n",tag);

            // update number of jobs completed
            completedJobs++;

            // exit if all jobs have been completed
            if(completedJobs == propIn.numJobs)
                runFlag = 0; 

        }

    }

    // SLAVE PROCESS
    else
    {
        // call thalassa subroutine and propagate
        double MJD0;
        int npts, propStateIdx;
        double SCMass, ADrag, ASRP, CD, CR;

        for (i = 0; i < locNumJobs; i++)
        {
    
            // get propagation state
            jobIdx = propJobs[i];
            objIdx = propIn.fieldPropJobs[jobIdx];

            // define object properties (to be placed in for-loop)
            MJD0 = propIn.fieldStates[objIdx][0];
            SCMass = propIn.fieldStates[objIdx][7];
            ADrag = propIn.fieldStates[objIdx][8];
            ASRP = propIn.fieldStates[objIdx][9];
            CD = propIn.fieldStates[objIdx][10];
            CR = propIn.fieldStates[objIdx][11];
            COE0[0] = propIn.fieldStates[objIdx][1];
            COE0[1] = propIn.fieldStates[objIdx][2];
            COE0[2] = propIn.fieldStates[objIdx][3];
            COE0[3] = propIn.fieldStates[objIdx][4];
            COE0[4] = propIn.fieldStates[objIdx][5];
            COE0[5] = propIn.fieldStates[objIdx][6];

            if(propIn.bugtag == 20)
            {
            printf("%d --- %.12f %.12f %.12f %.12f %.12f %.12f %.12f %.12f %.12f %.12f %.12f %.12f\n",
                    propIn.rank,MJD0, COE0[0], COE0[1], COE0[2],
                    COE0[3], COE0[4], COE0[5], SCMass,
                    ADrag, ASRP, CD, CR);
            }

            tag2 = objIdx;
            double clockStart = clock();
            propIn.input.mxstep = propIn.mxpts;
            thalassasub_( &MJD0, &COE0[0], &propIn.partialTspan, &propIn.tstep, &propIn.input.insgrav, &propIn.input.isun, &propIn.input.imoon,
            &propIn.input.idrag, &propIn.input.iF107, &propIn.input.iSRP, &propIn.input.iephem, &propIn.input.gdeg, &propIn.input.gord, &propIn.input.mxstep, 
            &propIn.input.tol, &propIn.input.imcoll, &propIn.input.eqs, &SCMass, &ADrag, &ASRP, &CD, &CR, &propIn.mxpts, &npts, 
            propIn.fieldTraj[0], &tag2, &exitcode);

            // check if mxpts was reached
            while (exitcode == -3)
            {
                // update max number of points
                propIn.mxpts += 50000;
                propIn.input.mxstep = propIn.mxpts;

                // repropagate
                thalassasub_( &MJD0, &COE0[0], &propIn.partialTspan, &propIn.tstep, &propIn.input.insgrav, &propIn.input.isun, &propIn.input.imoon,
                &propIn.input.idrag, &propIn.input.iF107, &propIn.input.iSRP, &propIn.input.iephem, &propIn.input.gdeg, &propIn.input.gord, &propIn.input.mxstep, 
                &propIn.input.tol, &propIn.input.imcoll, &propIn.input.eqs, &SCMass, &ADrag, &ASRP, &CD, &CR, &propIn.mxpts, &npts, 
                propIn.fieldTraj[0], &tag2, &exitcode);
            }

            double clockEnd = clock();
            double timeUsed = (clockEnd - clockStart)/CLOCKS_PER_SEC;
            // printf("%f on job %d on core %d\n",timeUsed, i,rank);

            // DEBUG
            if (debug == 0)
                printf("successfully retrieved trajectory %d on core %d\n", objIdx, propIn.rank);
            
            // update trajectory length
            propIn.fieldTrajLengths[objIdx] = npts;

            // fill trajectory with physical parameters
            for (j = 0; j < npts; j++)
            {
                propIn.fieldTraj[0][j * 12 + 7] = SCMass;
                propIn.fieldTraj[0][j * 12 + 8] = ADrag;
                propIn.fieldTraj[0][j * 12 + 9] = ASRP;
                propIn.fieldTraj[0][j * 12 + 10] = CD;
                propIn.fieldTraj[0][j * 12 + 11] = CR;

            }

            // define exit code
            if (exitcode != 0)
            {
                printf("prop failure on field object %d with exit code %d detected\n",objIdx,exitcode);
                exitCode = 1;
            }
            else
                exitCode = 0;

            // DEBUG
            if (debug == 0)
                printf("saved trajectory %d on core %d\n", objIdx, propIn.rank);
            
            // send trajectory
            tag = objIdx;
            // printf("sending traj: %d - job: %d from cpu: %d\n", tag, jobIdx, rank);
            MPI_Send(&objIdx, 1, MPI_INT, 0, tag, MPI_COMM_WORLD);
            MPI_Send(&npts, 1, MPI_INT, 0, tag, MPI_COMM_WORLD);
            MPI_Send(&exitCode, 1, MPI_INT, 0, tag, MPI_COMM_WORLD);
            MPI_Send(propIn.fieldTraj[0], 1, trajSend, 0, tag, MPI_COMM_WORLD);
            
            
        }

    }

    // synchronize processors
    MPI_Barrier (MPI_COMM_WORLD);

    // send all other processors trajectory lengths
    MPI_Bcast(propIn.fieldTrajLengths, propIn.lenSet, MPI_INT, 0, MPI_COMM_WORLD);

    // send all other processors exit codes
    MPI_Bcast(propIn.fieldExitCodes, propIn.lenSet, MPI_INT, 0, MPI_COMM_WORLD);

    // dellocate derived types
    MPI_Type_free(&trajSend);
    MPI_Type_free(&ecodeArr);


    return 0;

}


