/* Header for trajectory sharing subroutines */

/* include modules */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <math.h>
#include <mpi.h>

/* define structure */
struct trajShare
{
    struct contigInt *location;
    int *nCpus;
    int *nObjs; 
};

/* function to initialize trajectory sharing structure*/
void initTrajShare(struct trajShare *locs, int objs, double *memOut, int cpus)
{
    double buff = 0;

    // allocate memory
    locs->nCpus = malloc(sizeof(int));
    locs->nObjs = malloc(sizeof(int));
    locs->location = malloc(sizeof(struct contigInt));

    // assign values
    *(locs->nCpus) = cpus;
    *(locs->nObjs) = objs;
    allocContigInt(locs->location, objs + 1);

    // count memory
    buff = 2*sizeof(int);
    buff += sizeof(struct contigInt);
    buff += *(locs->location->mem);
    *(memOut) = buff;

    return;
}

/* function to assign each processor a job from the P array */
void assignCompJobs(int *compJobs, int ncpus, int njobs, int rank, int *locNumJobs)
{
    // vars
    int i, cpuIdx, localEntryIdx;

    // assign jobs -> not to root rank
    cpuIdx = 1;
    localEntryIdx = 0;
    for(i = 0; i < njobs; i++)
    {
        // assign
        if(cpuIdx == rank)
        {
            compJobs[localEntryIdx] = i;
            localEntryIdx++;
        }

        // update index
        cpuIdx++;

        // reset cpuIdx if exceeds num cpus
        if(cpuIdx == ncpus)
            cpuIdx = 1;
        
    }

    // save local number of jobs
    *(locNumJobs) = localEntryIdx;
}

