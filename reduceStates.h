// MODULES
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <math.h>

struct redStates
{

    // 1D variables
    double *vmax;
    double *tstep;
    int *mxpts;
    int *numTargetObjs;
    int *numFieldObjs;
    int *combs;
    int *approaches;

    // structures
    struct contigInt *targetTrajLengths, *fieldTrajLengths;
    struct contigInt *fieldPropJobs, *targetPropJobs;
    struct contigInt *targetMapping, *fieldMapping;
    struct contigInt *targExitCodes, *fieldExitCodes;
    struct contigInt *trajCompJobs;
    struct contigInt *targStatus, *fieldStatus;
    struct doubleSemi *targetTraj, *fieldTraj;
    struct rowDouble *targetStates, *fieldStates;
    struct rowInt *P;
    struct contigDouble *C;
    struct contigInt *locApproaches;
    struct contigDouble *sepArr;
    struct contigDouble *mjdArr;
};

/* function to initialize reduction structure*/
 void initReds(struct redStates *reds, double *memOut, int numTargs, int numFields, int mxpts, int rank) {
    double buff = 0;
    double buff2 = 0;
    double buff3 = 0;

    // allocate memory
    MPI_Barrier(MPI_COMM_WORLD);
    // printf("a\n");

   

    // 1D variables
    reds->vmax = malloc(sizeof(double));
    reds->tstep = malloc(sizeof(double));
    reds->mxpts = malloc(sizeof(int));
    reds->numTargetObjs = malloc(sizeof(int));
    reds->numFieldObjs = malloc(sizeof(int));
    reds->combs = malloc(sizeof(int));
    reds->approaches = malloc(sizeof(int));
    *(reds->vmax) = 0;
    // printf("1\n");
    *(reds->tstep) = 0;
    // printf("1.2\n");
    MPI_Barrier(MPI_COMM_WORLD);
    (*reds->mxpts) = 0;
    // printf("1.5\n");
    *(reds->numTargetObjs) = 0;
    *(reds->numFieldObjs) = 0;
    *(reds->combs) = 0;
    *(reds->approaches) = 0;

    //object status
    reds->targStatus = malloc(sizeof(struct contigInt));
    reds->fieldStatus = malloc(sizeof(struct contigInt));
    

    // alloc space for pointers
    reds->targetTraj = malloc(sizeof(struct doubleSemi));
    reds->fieldTraj = malloc(sizeof(struct doubleSemi));
    reds->fieldStates = malloc(sizeof(struct rowDouble));
    reds->targetStates = malloc(sizeof(struct rowDouble));
    reds->targetTrajLengths = malloc(sizeof(struct contigInt));
    reds->fieldTrajLengths = malloc(sizeof(struct contigInt));
    reds->targetPropJobs = malloc(sizeof(struct contigInt));
    reds->fieldPropJobs = malloc(sizeof(struct contigInt));
    reds->targetMapping = malloc(sizeof(struct contigInt));
    reds->fieldMapping = malloc(sizeof(struct contigInt));
    reds->targExitCodes = malloc(sizeof(struct contigInt));
    reds->fieldExitCodes = malloc(sizeof(struct contigInt));
    reds->trajCompJobs = malloc(sizeof(struct contigInt));
    reds->C = malloc(sizeof(struct contigDouble));
    reds->locApproaches = malloc(sizeof(struct contigInt));
    reds->sepArr = malloc(sizeof(struct contigDouble));
    reds->mjdArr = malloc(sizeof(struct contigDouble));
    reds->P = malloc(sizeof(struct rowInt));

    // arrays
    int lenTargs = 0;
    int lenFields = 0;
    if(rank == 0)
    {
        lenTargs = 1;
        lenFields = 1;
    }
    else
    {
        lenTargs = 1;
        lenFields = 1;
    }
    
    alloc3DDoubleSemi(reds->targetTraj, lenTargs, mxpts, 12 );
    alloc3DDoubleSemi(reds->fieldTraj, lenFields, mxpts, 12);
    allocRowDouble(reds->targetStates, numTargs+1, 12);
    allocRowDouble(reds->fieldStates, numFields+1, 12);
    allocContigInt(reds->targetTrajLengths, numTargs+1);
    allocContigInt(reds->fieldTrajLengths, numFields+1);
    if(rank == 0)
        allocContigDouble(reds->C, 4*1000);
    else
        allocContigDouble(reds->C, 1);
    
    allocContigInt(reds->locApproaches, 1000);
    allocContigDouble(reds->sepArr, 1000);
    allocContigDouble(reds->mjdArr, 1000);
    allocContigInt(reds->targetPropJobs, numTargs+1);
    allocContigInt(reds->fieldPropJobs, numFields+1);
    allocRowInt(reds->P, 1, 2);
    allocContigInt(reds->targetMapping, numTargs+1);
    allocContigInt(reds->fieldMapping, numFields+1);
    allocContigInt(reds->targExitCodes, numTargs+1);
    allocContigInt(reds->fieldExitCodes, numFields+1);
    allocContigInt(reds->trajCompJobs, 2000);
    allocContigInt(reds->targStatus, numFields + 1);
    allocContigInt(reds->fieldStatus, numTargs + 1);


    // calculate total memory of red struct
    // if(rank == 0)
    //     printf("root buff = %f\n",buff);
    // else if(rank == 1)
    //     printf("nom buff = %f\n",buff);
    buff = 2*sizeof(double) + 4*sizeof(int);
    buff += 2*sizeof(struct doubleSemi); 
    buff += 2*sizeof(struct rowDouble);
    buff += 9*sizeof(struct contigInt);
    buff += sizeof(struct contigDouble);
    buff += sizeof(struct rowInt);
    buff = buff/1e6;

//    if(rank == 0)
//         printf("1 root buff = %f\n",buff/1e+3);
//     else if(rank == 1)
//         printf("1 nom buff = %f\n",buff/1e+3);

    buff += *(reds->targetTraj->mem) + *(reds->fieldTraj->mem);

    // if(rank == 0)
    //     printf("2 root buff = %f --- sizeofdouble = %lu lentargs = %d and lenfields = %d over %d points => traj bytes = %lu\n",buff,sizeof(double),lenTargs,lenFields,mxpts,(lenTargs +lenFields)*mxpts*12*sizeof(double));
    // else if(rank == 1)
    //     printf("2 nom buff = %f --- sizeofdouble = %lu lentargs = %d and lenfields = %d over %d points => traj bytes = %lu\n",buff,sizeof(double),lenTargs,lenFields,mxpts,(lenTargs +lenFields)*mxpts*12*sizeof(double));

    buff += *(reds->targetStates->mem) + *(reds->fieldStates->mem);

    // if(rank == 0)
    //     printf("3 root buff = %f\n",buff/1e+3);
    // else if(rank == 1)
    //     printf("3 nom buff = %f\n",buff/1e+3);
    buff += *(reds->targetTrajLengths->mem) + *(reds->fieldTrajLengths->mem);

    // if(rank == 0)
    //     printf("4 root buff = %f\n",buff/1e+3);
    // else if(rank == 1)
    //     printf("4 nom buff = %f\n",buff/1e+3);
    buff += *(reds->C->mem);

    // if(rank == 0)
    //     printf("5 root buff = %f\n",buff/1e+3);
    // else if(rank == 1)
    //     printf("5 nom buff = %f\n",buff/1e+3);

    buff += *(reds->targetPropJobs->mem) + *(reds->fieldPropJobs->mem);

    // if(rank == 0)
    //     printf("6 root buff = %f\n",buff/1e+3);
    // else if(rank == 1)
    //     printf("6 nom buff = %f\n",buff/1e+3);
    buff += *(reds->P->mem);

    if(rank == 0)
    //     printf("7 root buff = %f\n",buff/1e+3);
    // else if(rank == 1)
    //     printf("7 nom buff = %f\n",buff/1e+3);
    buff += *(reds->targetMapping->mem) + *(reds->fieldMapping->mem);

    // if(rank == 0)
    //     printf("8 root buff = %f\n",buff/1e+3);
    // else if(rank == 1)
    //     printf("8 nom buff = %f\n",buff/1e+3);
    buff += *(reds->targExitCodes->mem) + *(reds->fieldExitCodes->mem);

    // if(rank == 0)
    //     printf("9 root buff = %f\n",buff/1e+3);
    // else if(rank == 1)
    //     printf("9 nom buff = %f\n",buff/1e+3);
    buff += *(reds->trajCompJobs->mem);

    // if(rank == 0)
    //     printf("10 root buff = %f\n",buff/1e+3);
    // else if(rank == 1)
    //     printf("10 nom buff = %f\n",buff/1e+3);
    buff += *(reds->locApproaches->mem);

    // if(rank == 0)
    //     printf("11 root buff = %f\n",buff/1e+3);
    // else if(rank == 1)
    //     printf("11 nom buff = %f\n",buff/1e+3);
    buff += *(reds->sepArr->mem);

    // if(rank == 0)
    //     printf("12 root buff = %f\n",buff/1e+3);
    // else if(rank == 1)
    //     printf("12 nom buff = %f\n",buff/1e+3);
    buff += *(reds->mjdArr->mem);

    buff += *(reds->targStatus->mem) + *(reds->fieldStatus->mem);

    // if(rank == 0)
    //     printf("13 root buff = %f\n",buff/1e+3);
    // else if(rank == 1)
    //     printf("13 nom buff = %f\n",buff/1e+3);

    *(memOut) = buff;
    
}


/* function to reduce the number of endogenous states that need to be propagated */
void reduceEndoStates(struct endoReduceIn in)
{
    int i, j, k, idx, targIdx;
    int targObjectStatus[in.numTargObjs];
    int localTargMap[in.numTargObjs];
    int obj1, obj2;
    int objIdx, nextApproaches, objIdx1, objIdx2;
    int targMapId;
    double buff = 0;
    int targL, fieldL;
    double prevTarg;
    double initialmem = *(in.totalMem);

    // initialize arrays with zeros
    for (i = 0; i < in.numTargObjs; i++)
        targObjectStatus[i] = 0;

    if (in.rank == 0)
    {
        targIdx = 0;

        for (i = 0; i < in.nApproach; i++)
        {
            // get designations
            objIdx1 = in.C->arr[i * 4 + 0];
            objIdx2 = in.C->arr[i * 4 + 1];

            // check if objects have already been flagged
            if (targObjectStatus[objIdx1] == 0)
            {
                targObjectStatus[objIdx1] = 1;
                localTargMap[targIdx] = objIdx1;
                targIdx++;
            }
            if (targObjectStatus[objIdx2] == 0)
            {
                targObjectStatus[objIdx2] = 1;
                localTargMap[targIdx] = objIdx2;
                targIdx++;
            }
        }
    }

    // broadcast
    MPI_Bcast(&targIdx, 1, MPI_INT, 0, MPI_COMM_WORLD);
    MPI_Bcast(&targObjectStatus[0], in.numTargObjs, MPI_INT, 0, MPI_COMM_WORLD);
    MPI_Bcast(&localTargMap[0], in.numTargObjs, MPI_INT, 0, MPI_COMM_WORLD);


    // allocate memory for new pointers
    
    // exit codes
    reallocContigInt(in.reds->targExitCodes, targIdx);
    *(in.totalMem) += ( *(in.reds->targExitCodes->mem) );
    *(in.totalMem) -= ( *(in.reds->targExitCodes->prevmem) );

    // states
    reallocRowDouble(in.reds->targetStates, targIdx);
    *(in.totalMem) += ( *(in.reds->targetStates->mem) );
    *(in.totalMem) -= ( *(in.reds->targetStates->prevmem) );

    // status                                                                                                                                                                                                                                                                 
    reallocContigInt(in.reds->targStatus, in.numTargObjs );

    // collisions arrays
    if(in.rank == 0)
    {
        reallocContigDouble(in.reds->C, in.nApproach*4);
        *(in.totalMem) -= *(in.reds->C->prevmem);
        *(in.totalMem) += *(in.reds->C->mem);
    }
    
    // propagation jobs
    reallocContigInt(in.reds->targetPropJobs, targIdx);
    *(in.totalMem) += ( *(in.reds->targetPropJobs->mem) );
    *(in.totalMem) -= ( *(in.reds->targetPropJobs->prevmem) );

    // comparison jobs
    reallocContigInt(in.reds->trajCompJobs, in.combos);
    *(in.totalMem) += *(in.reds->trajCompJobs->mem);
    *(in.totalMem) -= *(in.reds->trajCompJobs->prevmem);

    // trajectory lengths
    reallocContigInt(in.reds->targetTrajLengths, targIdx);
    *(in.totalMem) += ( *(in.reds->targetTrajLengths->mem) );
    *(in.totalMem) -= ( *(in.reds->targetTrajLengths->prevmem) );


    // update number of objects
    int prevTargObjs = *(in.reds->numTargetObjs);
    *(in.reds->numTargetObjs) = targIdx;

    // update target states
    for (i = 0; i < targIdx; i++)
    {
  
        // get object index
        objIdx = localTargMap[i];

        // add to states
        memcpy(in.reds->targetStates->arr[i], in.origTargStates[objIdx], 12*sizeof(double));

        // add to prop jobs
        in.reds->targetPropJobs->arr[i] = i;

        // mapping
        in.reds->targetMapping->arr[objIdx] = i;
    
    }

    // calculate new vmax
    *(in.reds->vmax) = calcVmax(in.reds->targetStates->arr, in.reds->targetStates->arr, *(in.reds->numTargetObjs), *(in.reds->numTargetObjs), 1);

    // calculate new tstep
    *(in.reds->tstep) = (in.d_rec/(*(in.reds->vmax)))/2/60/60/24;

    // calculate new mxpts
    *(in.reds->mxpts) = calcMxpts(in.tspan, *(in.reds->tstep));

    // traj per core
    if (in.rank == 0)
    {
        targL = *(in.reds->numTargetObjs);
        fieldL = 1;
    }
    else
    {
        targL = 1;
        fieldL = 1;
    }

    // allocate array for reduced trajectories
    reallocDouble3Dsemi(in.reds->targetTraj, targL, *(in.reds->mxpts));
    reallocDouble3Dsemi(in.reds->fieldTraj, fieldL, *(in.reds->mxpts));
    *(in.totalMem) += ( *(in.reds->targetTraj->mem) + *(in.reds->fieldTraj->mem) );
    *(in.totalMem) -= ( *(in.reds->targetTraj->prevmem) + *(in.reds->fieldTraj->prevmem) );


    // copy target status
    memcpy(in.reds->targStatus->arr, &targObjectStatus, in.numTargObjs*sizeof(int));

    // update collision and comparison job arrays
    
    if (in.rank == 0)
    {
        for (i = 0; i < in.nApproach; i++)
        {
            
            in.reds->C->arr[i * 4 + 0] = in.C->arr[i * 4 + 0];
            in.reds->C->arr[i * 4 + 1] = in.C->arr[i * 4 + 1];
            in.reds->C->arr[i * 4 + 2] = in.C->arr[i * 4 + 2];
            in.reds->C->arr[i * 4 + 3] = in.C->arr[i * 4 + 3];

            
        }
    }

    // update traj comparison arrays
    for (i = 0; i < in.combos; i++)
        in.reds->trajCompJobs->arr[i] = i;


}

/* function to reduce the number of exogenous states that need to be propagated */
void reduceExoStates(struct exoReduceIn in)
{
    int i, j, k, idx, targIdx, fieldIdx;
    int targObjectStatus[in.numTargObjs], fieldObjectStatus[in.numFieldObjs];
    int localTargMap[in.numTargObjs], localFieldMap[in.numFieldObjs];
    int obj1, obj2;
    int objIdx, nextApproaches, objIdx1, objIdx2;
    // struct redStates reds;
    int targMapId, fieldMapId;
    double buff = 0;
    int targL, fieldL;
    double prevTarg, prevField;
    struct exoJobParams exoJobs;
    double initialmem = *(in.totalMem);

    // initialize arrays with zeros
    for (i = 0; i < in.numTargObjs; i++)
        targObjectStatus[i] = 0;

    for (i = 0; i < in.numFieldObjs; i++)
        fieldObjectStatus[i] = 0;
    
    // printf("reducing on rank %d with %d field objects and %d target objects --- with %d approaches and %d object combinations\n",rank,numFieldObjs,numTargObjs,nApproach,combos);

    // printf(" INNER 1 -- REDS 2 STATS on rank %d: num rows: %d --- num columns: %d --- mem: %f\n",rank,*(nextReds->P->rows),*(nextReds->P->columns),*(nextReds->P->mem));

    // printf("check if object needs to be propagated -- %d objs on %d\n",nApproach,rank);
    // check if object needs to be propagated
    if (in.rank == 0)
    {
        targIdx = 0;
        fieldIdx = 0;
        for (i = 0; i < in.nApproach; i++)
        {
            // printf("get designations - %d\n", i);
            // get designations
            objIdx1 = in.C->arr[i * 4 + 0];
            objIdx2 = in.C->arr[i * 4 + 1];
            // printf("%d --- %d :::::::: || %.0f x %.0f\n", i,rank,C[i * 5 + 0],C[i * 5 + 1]);

            // printf("check if objects have already been flagged - %d\n", i);
            // check if objects have already been flagged
            if (targObjectStatus[objIdx1] == 0)
            {
                targObjectStatus[objIdx1] = 1;
                localTargMap[targIdx] = objIdx1;
                targIdx++;
            }
            if (fieldObjectStatus[objIdx2] == 0)
            {
                fieldObjectStatus[objIdx2] = 1;
                localFieldMap[fieldIdx] = objIdx2;
                fieldIdx++;
            }
        }
    }

    // printf(" INNER 2 -- REDS 2 STATS on rank %d: num rows: %d --- num columns: %d --- mem: %f\n",rank,*(nextReds->P->rows),*(nextReds->P->columns),*(nextReds->P->mem));

    // broadcast
    MPI_Bcast(&targIdx, 1, MPI_INT, 0, MPI_COMM_WORLD);
    MPI_Bcast(&fieldIdx, 1, MPI_INT, 0, MPI_COMM_WORLD);
    MPI_Bcast(&targObjectStatus[0], in.numTargObjs, MPI_INT, 0, MPI_COMM_WORLD);
    MPI_Bcast(&fieldObjectStatus[0], in.numFieldObjs, MPI_INT, 0, MPI_COMM_WORLD);
    MPI_Bcast(&localTargMap[0], in.numTargObjs, MPI_INT, 0, MPI_COMM_WORLD);
    MPI_Bcast(&localFieldMap[0], in.numFieldObjs, MPI_INT, 0, MPI_COMM_WORLD);

    // printf(" INNER 3 -- REDS 2 STATS on rank %d: num rows: %d --- num columns: %d --- mem: %f\n",rank,*(nextReds->P->rows),*(nextReds->P->columns),*(nextReds->P->mem));

    MPI_Barrier(MPI_COMM_WORLD);

    // allocate memory for new pointers
    
    // exit codes
    reallocContigInt(in.reds->targExitCodes, targIdx);
    reallocContigInt(in.reds->fieldExitCodes, fieldIdx);
    *(in.totalMem) += ( *(in.reds->targExitCodes->mem) + *(in.reds->fieldExitCodes->mem) );
    *(in.totalMem) -= ( *(in.reds->targExitCodes->prevmem) + *(in.reds->fieldExitCodes->prevmem) );

    MPI_Barrier(MPI_COMM_WORLD);
    // states
    reallocRowDouble(in.reds->targetStates, targIdx);
    reallocRowDouble(in.reds->fieldStates, fieldIdx);
    *(in.totalMem) += ( *(in.reds->targetStates->mem) + *(in.reds->fieldStates->mem) );
    *(in.totalMem) -= ( *(in.reds->targetStates->prevmem) + *(in.reds->fieldStates->prevmem) );

    // status                                                                                                                                                                                                                                                                 
    reallocContigInt(in.reds->targStatus, in.numTargObjs );
    reallocContigInt(in.reds->fieldStatus, in.numFieldObjs );

    // collisions arrays
    if(in.rank == 0)
    {
        reallocContigDouble(in.reds->C, in.nApproach*4);
        *(in.totalMem) -= *(in.reds->C->prevmem);
        *(in.totalMem) += *(in.reds->C->mem);
    }
    
    // propagation jobs
    reallocContigInt(in.reds->targetPropJobs, targIdx);
    reallocContigInt(in.reds->fieldPropJobs, fieldIdx);
    *(in.totalMem) += ( *(in.reds->targetPropJobs->mem) + *(in.reds->fieldPropJobs->mem) );
    *(in.totalMem) -= ( *(in.reds->targetPropJobs->prevmem) + *(in.reds->fieldPropJobs->prevmem) );

    // comparison jobs
    reallocContigInt(in.reds->trajCompJobs, in.combos);
    *(in.totalMem) += *(in.reds->trajCompJobs->mem);
    *(in.totalMem) -= *(in.reds->trajCompJobs->prevmem);

    // trajectory lengths
    reallocContigInt(in.reds->targetTrajLengths, targIdx);
    reallocContigInt(in.reds->fieldTrajLengths, fieldIdx);
    *(in.totalMem) += ( *(in.reds->targetTrajLengths->mem) + *(in.reds->fieldTrajLengths->mem) );
    *(in.totalMem) -= ( *(in.reds->targetTrajLengths->prevmem) + *(in.reds->fieldTrajLengths->prevmem) );

    // printf(" INNER 4 -- REDS 2 STATS on rank %d: num rows: %d --- num columns: %d --- mem: %f\n",rank,*(nextReds->P->rows),*(nextReds->P->columns),*(nextReds->P->mem));

    // update number of objects
    int prevTargObjs = *(in.reds->numTargetObjs);
    int prevFieldObjs = *(in.reds->numFieldObjs);
    *(in.reds->numTargetObjs) = targIdx;
    *(in.reds->numFieldObjs) = fieldIdx;

    // update target states
    // printf("updating target states on %d\n",rank);
    for (i = 0; i < targIdx; i++)
    {
  
        // get object index
        // printf("object index\n");
        objIdx = localTargMap[i];

        // add to states
        // printf("memcpy target states (add to states)\n");
        memcpy(in.reds->targetStates->arr[i], in.origTargStates[objIdx], 12*sizeof(double));

        // add to prop jobs
        // printf("add to prop jobs\n");
        in.reds->targetPropJobs->arr[i] = i;

        // mapping
        in.reds->targetMapping->arr[objIdx] = i;
    
    }

    // update field states and prop jobs
    // printf("updating field states on %d\n",rank);
    for (i = 0; i < fieldIdx; i++)
    {
       
        // get object index
        // printf("get object idx\n");
        objIdx = localFieldMap[i];

        // add to states
        // printf("add to field states\n");
        memcpy(in.reds->fieldStates->arr[i], in.origFieldStates[objIdx], 12*sizeof(double));

        // add to prop jobs
        // printf("add to prop jobs\n");
        in.reds->fieldPropJobs->arr[i] = i;

        // mapping
        // printf("global field map\n");
        in.reds->fieldMapping->arr[objIdx] = i;

    }

    // printf(" INNER 5 -- REDS 2 STATS on rank %d: num rows: %d --- num columns: %d --- mem: %f\n",rank,*(nextReds->P->rows),*(nextReds->P->columns),*(nextReds->P->mem));

    // calculate new vmax
    // printf("calculating new max v\n");
    *(in.reds->vmax) = calcVmax(in.reds->targetStates->arr, in.reds->fieldStates->arr, *(in.reds->numTargetObjs), *(in.reds->numFieldObjs), 0);

    // calculate new tstep
    // printf("calculating new tstep \n");
    *(in.reds->tstep) = (in.d_rec/(*(in.reds->vmax)))/2/60/60/24;

    // calculate new mxpts
    // printf("calculating new mxpts on %d\n",rank);
    *(in.reds->mxpts) = calcMxpts(in.tspan, *(in.reds->tstep));

    // printf("REDUCED POINTS = %d\n",*(reds->mxpts));

    // calculate number of trajectories per cores
    exoJobs = calcExoJobLengths(in.cpus, *(in.reds->numTargetObjs), *(in.reds->numFieldObjs), in.combos);
    if (in.rank == 0)
    {
        targL = *(in.reds->numTargetObjs);
        fieldL = *(in.reds->numFieldObjs);
    }
    else
    {
        targL = 1;
        fieldL = 1;
    }

    // printf(" INNER 6 -- REDS 2 STATS on rank %d: num rows: %d --- num columns: %d --- mem: %f\n",rank,*(nextReds->P->rows),*(nextReds->P->columns),*(nextReds->P->mem));

    // allocate array for reduced trajectories
    // printf("CHUNK: %d --------------- STAGE: %d\n",chunk, stage);
    reallocDouble3Dsemi(in.reds->targetTraj, targL, *(in.reds->mxpts));
    reallocDouble3Dsemi(in.reds->fieldTraj, fieldL, *(in.reds->mxpts));
    *(in.totalMem) += ( *(in.reds->targetTraj->mem) + *(in.reds->fieldTraj->mem) );
    *(in.totalMem) -= ( *(in.reds->targetTraj->prevmem) + *(in.reds->fieldTraj->prevmem) );

    // printf(" INNER 7 -- REDS 2 STATS on rank %d: num rows: %d --- num columns: %d --- mem: %f\n",rank,*(nextReds->P->rows),*(nextReds->P->columns),*(nextReds->P->mem));
    
    MPI_Barrier(MPI_COMM_WORLD);

    // copy target status
    memcpy(in.reds->targStatus->arr, &targObjectStatus, in.numTargObjs*sizeof(int));

    // copy field status
    memcpy(in.reds->fieldStatus->arr, &fieldObjectStatus, in.numFieldObjs*sizeof(int));

    // printf(" INNER 8 -- REDS 2 STATS on rank %d: num rows: %d --- num columns: %d --- mem: %f\n",rank,*(nextReds->P->rows),*(nextReds->P->columns),*(nextReds->P->mem));

    // update collision and comparison job arrays
    
    if (in.rank == 0)
    {
        // printf("update %d approaches collision and comparison job arrays on %d\n",nApproach,rank);
        for (i = 0; i < in.nApproach; i++)
        {
            
            in.reds->C->arr[i * 4 + 0] = in.C->arr[i * 4 + 0];
            in.reds->C->arr[i * 4 + 1] = in.C->arr[i * 4 + 1];
            in.reds->C->arr[i * 4 + 2] = in.C->arr[i * 4 + 2];
            in.reds->C->arr[i * 4 + 3] = in.C->arr[i * 4 + 3];

            
        }
    }

    // printf(" INNER 9 -- REDS 2 STATS on rank %d: num rows: %d --- num columns: %d --- mem: %f\n",rank,*(nextReds->P->rows),*(nextReds->P->columns),*(nextReds->P->mem));

    // update traj comparison arrays
    for (i = 0; i < in.combos; i++)
        in.reds->trajCompJobs->arr[i] = i;

    // printf(" INNER 10 -- REDS 2 STATS on rank %d: num rows: %d --- num columns: %d --- mem: %f\n",rank,*(nextReds->P->rows),*(nextReds->P->columns),*(nextReds->P->mem));

    // save number of approaches
    // printf("save number of approaches\n");
    // *(reds->combs) = combos;
    // *(reds->approaches) = nApproach;
    // printf("reds delta mem = %f\n",*(in.totalMem)-initialmem);

    // printf("returning reds on %d\n",rank);

}
