// header file for thalassa batch propagation
// created by Nathan Reiland
// 3/15/2019

/* DEFINE CONSTANTS */
#define MAXLINE 1000
#define debug 6

/* STRUCTURE DEFINITIONS */
struct Input
{
    char outputDir[MAXLINE];
    char thalassaDir[MAXLINE];
    char fieldObjects[MAXLINE];
    char targetObjects[MAXLINE];
    int numCores;
    int numTargetObjs;
    int numFieldObjs;
    int endogenous;
    int numIntervals;
    int insgrav;
    int isun;
    int imoon;
    int idrag;
    int iF107;
    int iSRP;
    int iephem;
    int gdeg;
    int gord;
    double tol;
    double tspan;
    double tstep;
    int mxstep;
    int imcoll;
    int eqs;
    int verb;
    int numFilters;
    double d1;
    double d2;
    double d3;
    double d4;
    double d5;
    int print1;
    int print2;
    int print3;
    int print4;
    int print5;
};

struct exoJobParams
{
    int rootNumFieldPropJobs;
    int rootNumTargPropJobs;
    int nomNumFieldPropJobs;
    int nomNumTargPropJobs;
    int rootNumTrajJobs;
    int nomNumTrajJobs;
    int targPropRemainder;
    int fieldPropRemainder;
    int trajRemainder;
    
};

struct endoJobParams
{
    int rootNumPropJobs;
    int nomNumPropJobs;
    int rootNumTrajJobs;
    int nomNumTrajJobs;
    int propRemainder;
    int trajRemainder;
};

struct allocIntOut
{
    int **point;
    double mem;

};

struct allocDoubOut
{
    double **point;
    double mem;
};

/* function to read input initial conditions files*/
double** readObjects(char objectsPath[], int numObjs)
{

    // function definitions
    double** allocateRowArray(int rows, int columns, double *mem);

    char line[MAXLINE];
    int i;
    int j;
    // int numObjs;
    char c;
    int count;
    char delim[] = ",";
    char *str;
    double **objects;
    double orbels[numObjs][12];
    double buff = 0;

    FILE *inFile = fopen(objectsPath, "r");
    
    // test for files not existing.
    if (inFile == NULL)
    {
        printf("Error! Could not open file\n");
        exit(-1); // must include stdlib.h
    }
    
    // count number of lines
    // count = 0;
    // for (c = getc(inFile); c != EOF; c = getc(inFile))
    //     if (c == '\n')
    //         count = count + 1;
    
    fclose(inFile);
    
    // define lines variable
    char lines[numObjs + 1][MAXLINE];
    
    // grab objects
    inFile = fopen(objectsPath, "r");
    
    // printf("getting %d lines from : %s\n",numObjs,objectsPath);
    for (i = 0; i < numObjs; i++) {
        fgets(line, sizeof(line), inFile);
        strcpy(lines[i], line);
        // printf("%d --- %s\n",i,lines[i]);
    }
    // printf("DONE getting lines from : %s\n",objectsPath);
    fclose(inFile);
        
    if (debug == 1)
    {
        printf("the input file is '%s'\n", objectsPath);
        
         printf("count = %d\n", count);
        
        printf("numObjs = %d\n", numObjs);
        
        printf("the objects are:\n");
        
        for (i = 0; i <= numObjs-1; i = i + 1)
        {
            printf("%s\n", lines[i]);
        }
        
    }
    
    // allocate off stack memory for array of orbital elements
    objects = allocateRowArray(numObjs, 12, &buff);
    
    // parse objects
    
    // remove new line character
    for (i = 0; i <= numObjs-1; i = i + 1)
    {
        str = strtok(lines[i], "\n");
    }
    
    for (i = 0; i <= numObjs-1; i = i + 1)
    {
        str = strtok(lines[i], delim);
        j = 0;
        while (str != NULL)
        {
            if (j<11)
            {
                orbels[i][j] = strtof(str, NULL);
                j++;
                
            }
            else
            {
                orbels[i][j] = strtof(str, NULL);
                j = 0;
            }
 
            str = strtok(NULL, delim);
        }
        
    }
    printf("storing\n");
    // store in allocated memory
    for (i = 0; i<= numObjs-1; i++)
    {
        for (j = 0; j<=11; j++)
            objects[i][j] = orbels[i][j];
    }
    
    //debug commands
    if (debug == 1)
    {
        for (i = 0; i<= numObjs-1; i++)
        {
            for (j = 0; j<=11; j++)
                printf("%f\n",objects[i][j]);
            printf("\n");
        }
    }
    
    return objects;
}

/*function to allocate and array of pointers to rows 1D array (non contiguous)*/
double** allocateRowArray(int rows, int columns, double *mem)
{
    double **arr = (double **)malloc(rows * sizeof(double *));
    (*mem) = rows*sizeof(double *); 
    for (int i=0; i<rows; i++) {
        arr[i] = (double *)malloc(columns * sizeof(double));
        (*mem) += columns*sizeof(double);
    }

    return arr; 
}

double** reallocateRowArray(double **arr, int prevRows, int rows, int columns, double *mem)
{   
    int i;

    arr = realloc(arr, rows*sizeof(double *));
    (*mem) = rows*sizeof(double *);

    // if ( rows < prevRows ) {

    // }
    // for (i = 0; i < prevRows; i++)
    //     free(arr[i]);

    for (int i = 0; i < rows; i++) {
        arr[i] = realloc(arr[i], columns*sizeof(double));
        (*mem) += columns*sizeof(double);
    }

    return arr;
}

/*function to read values from input file*/
struct Input getPropInputs(char inputFile[])
{
    //int debug = 1;
    char line[MAXLINE];
    char lines[71][MAXLINE];
    char inputStr[33][MAXLINE];
    char inputStr2[33][MAXLINE];
    int i;
    int numIn;
    double conv;
    char delim[] = " = ";
    char delim2[] = "\0";
    char *str;
    char *testStr;
    char test[MAXLINE];
    struct Input inputs;
    
    FILE *inFile = fopen(inputFile, "r");
    
    if (debug == 1)
    {
        printf("the input file is '%s'\n", inputFile);
        
    }
    
    // test for files not existing.
    if (inFile == NULL)
    {
        printf("Error! Could not open file\n");
        exit(-1); // must include stdlib.h
    }
    
    i = 0;
    while (fgets(line, sizeof(line), inFile)) {

        strcpy(lines[i], line);
        i++;
    }
    
    fclose(inFile);
    
    strcpy(inputStr[0], lines[1]);
    strcpy(inputStr[1], lines[6]);
    strcpy(inputStr[2], lines[7]);
    strcpy(inputStr[3], lines[8]);
    strcpy(inputStr[4], lines[9]);
    strcpy(inputStr[5], lines[10]);
    strcpy(inputStr[6], lines[11]);
    strcpy(inputStr[7], lines[25]);
    strcpy(inputStr[8], lines[26]);
    strcpy(inputStr[9], lines[27]);
    strcpy(inputStr[10], lines[28]);
    strcpy(inputStr[11], lines[29]);
    strcpy(inputStr[12], lines[30]);
    strcpy(inputStr[13], lines[31]);
    strcpy(inputStr[14], lines[32]);
    strcpy(inputStr[15], lines[33]);
    strcpy(inputStr[16], lines[42]);
    strcpy(inputStr[17], lines[43]);
    strcpy(inputStr[18], lines[44]);
    strcpy(inputStr[19], lines[45]);
    strcpy(inputStr[20], lines[46]);
    strcpy(inputStr[21], lines[52]);
    strcpy(inputStr[22], lines[57]);
    strcpy(inputStr[23], lines[58]);
    strcpy(inputStr[24], lines[59]);
    strcpy(inputStr[25], lines[60]);
    strcpy(inputStr[26], lines[61]);
    strcpy(inputStr[27], lines[62]);
    strcpy(inputStr[28], lines[66]);
    strcpy(inputStr[29], lines[67]);
    strcpy(inputStr[30], lines[68]);
    strcpy(inputStr[31], lines[69]);
    strcpy(inputStr[32], lines[70]);
    
    numIn = sizeof(inputStr)/sizeof(inputStr[0]) - 1;
    
    for (i = 0; i <= numIn; i = i + 1)
    {
        testStr = strtok(inputStr[i], delim);
        while (testStr != NULL)
        {
            strcpy(inputStr2[i], testStr);
            testStr = strtok(NULL, delim);
        }
        str = strtok(inputStr2[i], delim);
    }
    
    for (i = 0; i <= numIn; i = i + 1)
    {
        testStr = strtok(inputStr2[i], "\n");
    }
    
    strcpy(inputs.outputDir, inputStr2[0]);
    strcpy(inputs.fieldObjects, inputStr2[1]);
    strcpy(inputs.targetObjects, inputStr2[2]);
    conv = strtod(inputStr2[3], NULL);
    inputs.numTargetObjs = conv;
    conv = strtod(inputStr2[4], NULL);
    inputs.numFieldObjs = conv;
    conv = strtod(inputStr2[5], NULL);
    inputs.endogenous = conv;
    conv = strtod(inputStr2[6], NULL);
    inputs.numIntervals = conv;
    conv = strtod(inputStr2[7], NULL);
    inputs.insgrav = conv;
    conv = strtod(inputStr2[8], NULL);
    inputs.isun = conv;
    conv = strtod(inputStr2[9], NULL);
    inputs.imoon = conv;
    conv = strtod(inputStr2[10], NULL);
    inputs.idrag = conv;
    conv = strtod(inputStr2[11], NULL);
    inputs.iF107 = conv;
    conv = strtod(inputStr2[12], NULL);
    inputs.iSRP = conv;
    conv = strtod(inputStr2[13], NULL);
    inputs.iephem = conv;
    conv = strtod(inputStr2[14], NULL);
    inputs.gdeg = conv;
    conv = strtod(inputStr2[15], NULL);
    inputs.gord = conv;
    inputs.tol = strtod(inputStr2[16], NULL);
    inputs.tspan = strtod(inputStr2[17], NULL);
    inputs.tstep = strtod(inputStr2[18], NULL);
    conv = strtod(inputStr2[19], NULL);
    inputs.mxstep = conv;
    conv = strtod(inputStr2[20], NULL);
    inputs.imcoll = conv;
    conv = strtod(inputStr2[21], NULL);
    inputs.eqs = conv;
    conv = strtod(inputStr2[22], NULL);
    inputs.numFilters = conv;
    conv = strtod(inputStr2[23], NULL);
    inputs.d1 = conv;
    conv = strtod(inputStr2[24], NULL);
    inputs.d2 = conv;
    conv = strtod(inputStr2[25], NULL);
    inputs.d3 = conv;
    conv = strtod(inputStr2[26], NULL);
    inputs.d4 = conv;
    conv = strtod(inputStr2[27], NULL);
    inputs.d5 = conv;
    conv = strtod(inputStr2[28], NULL);
    inputs.print1 = conv;
    conv = strtod(inputStr2[29], NULL);
    inputs.print2 = conv;
    conv = strtod(inputStr2[30], NULL);
    inputs.print3 = conv;
    conv = strtod(inputStr2[31], NULL);
    inputs.print4 = conv;
    conv = strtod(inputStr2[32], NULL);
    inputs.print5 = conv;
    if (debug == 7)
    {
        for (i = 0; i <= numIn; i = i + 1)
        {
            printf("%s\n", inputStr2[i]);
        }
        printf("---------------------\n");
        printf("%s\n", inputs.outputDir);
        printf("%s\n", inputs.thalassaDir);
        printf("%s\n", inputs.fieldObjects);
        printf("%d\n", inputs.numCores);
        printf("%d\n", inputs.insgrav);
        printf("%d\n", inputs.isun);
        printf("%d\n", inputs.imoon);
        printf("%d\n", inputs.idrag);
        printf("%d\n", inputs.iF107);
        printf("%d\n", inputs.iSRP);
        printf("%d\n", inputs.iephem);
        printf("%d\n", inputs.gdeg);
        printf("%d\n", inputs.gord);
        printf("%10.10f\n", inputs.tol);
        printf("%10.10f\n", inputs.tspan);
        printf("%10.10f\n", inputs.tstep);
        printf("%d\n", inputs.mxstep);
        printf("%d\n", inputs.imcoll);
        printf("%d\n", inputs.eqs);
        printf("%d\n", inputs.verb);

    }
    
    return inputs;
}

/* allocate memory for 2D contiguous array of ints */
int** allocateInt2DArray(int rows, int columns, double *mem)
{
    int **arr = (int **)malloc(rows * sizeof(int *));
    (*mem) = rows*sizeof(int *); 
    for (int i=0; i<rows; i++) {
        arr[i] = (int *)malloc(columns * sizeof(int));
        (*mem) += columns*sizeof(int);
    }
    
    return arr; 
}

int** reallocateInt2DArray(int **arr, int prevRows, int rows, int columns, double *mem)
{
    int i;

    arr = (int **)realloc(arr, rows * sizeof(int *));
    (*mem) = rows*sizeof(int *);

    for (i = 0; i < prevRows; i++) {
        free(arr[i]);
    }

    for (int i=0; i < rows; i++) {
        arr[i] = (int *)malloc(columns * sizeof(int));
        (*mem) += columns*sizeof(int);
    }

    return arr;
}

/* free 2d int array */
int free2dInt(int **arr, int m)
{
    int i;
    for (i = 0; i < m; i ++)
        free(arr[i]);
    free(arr);
    return 0;
}

int printTrajectory(int npts, int objDesig, double **traj)
{
    int i, j, k;

    for (i = 0; i < npts; i++)
    {
        printf("%.15f %.15f %.15f %.15f %.15f %.15f %.15f %.15f %.15f %.15f %.15f %.15f\n", traj[objDesig][i * 12 + 0],
                traj[objDesig][i * 12 + 1], traj[objDesig][i * 12 + 2],traj[objDesig][i * 12 + 3],traj[objDesig][i * 12 + 4],
                traj[objDesig][i * 12 + 5], traj[objDesig][i * 12 + 6],traj[objDesig][i * 12 + 7],traj[objDesig][i * 12 + 8],
                traj[objDesig][i * 12 + 9], traj[objDesig][i * 12 + 10],traj[objDesig][i * 12 + 11]);
    }
    return 0;
}

int printStates(int nObjs, double **states)
{
    int i, j, k;

    for ( j = 0; j < nObjs; j++)
    {
    printf("%.12f %.12f %.12f %.12f %.12f %.12f %.12f %.12f %.12f %.12f %.12f %.12f\n",
                    states[j][0], states[j][1], states[j][2], states[j][3],
                    states[j][4], states[j][5], states[j][6], states[j][7],
                    states[j][8], states[j][9], states[j][10], states[j][11]);
    }
    return 0;
}
