// header file

struct contigInt
{
    int *elements;
    int *arr;
    double *mem;
    double *prevmem;

    
};

/* function to initialize 3d semi contiguous array structure*/
 void allocContigInt(struct contigInt *array, int newElements) {

    int i;

    // allocate mem for organizational pointers
    array->elements = malloc(sizeof(int));
    array->mem = malloc(sizeof(double));
    array->prevmem = malloc(sizeof(double));

    // // define pointer values for organization pointers
    *(array->elements) = newElements;

    // allocate memory for main array
    array->arr = malloc(newElements * sizeof(int));
    memset(array->arr, 0, newElements*sizeof(int));

    // count memory
    *(array->mem) = 2*sizeof(int *);
    *(array->mem) += newElements*sizeof(int);
    *(array->mem) += 2*sizeof(double *);
    *(array->mem) = *(array->mem)/1e6;
    *(array->prevmem) = 0;

}

int reallocContigInt(struct contigInt *array, int elements) {

    // define vars
    int i;
    int flag = 0;
    int prevElements = *(array->elements);
    int newElements = elements;
    double newmem = 0;
    double prevmem = *(array->mem);

    // define factor of safety
    double fs = 1.0;
    if ( newElements > prevElements )
        newElements = fs*newElements;

    // realloc if number of elements has increased
    if (newElements > prevElements) {
        array->arr = realloc(array->arr, newElements*sizeof(int));
        // memset(array->arr, 0, newElements*sizeof(int));

        flag = 1;
    }
    // set memory to zero if no reallocation
    else {
        // memset(array->arr, 0, prevElements*sizeof(int));

        flag = 2;
    }

    // calculate new memory based on flag

    // newrows
    if ( flag == 1 ) {
        newmem = 2*sizeof(int *);
        newmem += newElements*sizeof(int);
        newmem += 2*sizeof(double *);

        *(array->elements) = newElements;
    }
    // prevrows
    else if (flag == 2) {
        newmem = 2*sizeof(int *);
        newmem += prevElements*sizeof(int);
        newmem += 2*sizeof(double *);

        *(array->elements) = prevElements;
    }

    // save memory
    *(array->prevmem) = prevmem;
    *(array->mem) = newmem/1e6;

    // return int
    return 0;
}