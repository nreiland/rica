#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <math.h>
#include <mpi.h>

int launchEndoCase( struct branchInput endoIn )
{
    // DEFINE VARIABLES
    int deltaT;
    double vmax;
    double tstep1;
    struct endoJobParams endoJobs1, endoJobs2, endoJobs3, endoJobs4, endoJobs5;
    struct exoJobParams propIn1, propIn2, propIn3, propIn4, propIn5;
    double partialTspan;
    int mxpts;
    int appr1, appr2, appr3, appr4, appr5;
    struct redStates *reds, *reds2, *reds3, *reds4, *reds5;
    int *targPropJobs, *trajCompJobs, *targExitCodes;
    int *targTrajLengths;
    struct contigDouble *C;
    struct contigInt *locApproaches;
    struct contigDouble *sepArr;
    struct contigDouble *mjdArr;
    int numTargJobs, numCompJobs, *targetMapping;
    struct doubleSemi *targetTraj, *fieldTraj;
    int *globTargetMap;
    int targL, fieldL;
    int pairs1, pairs2, pairs3, pairs4, pairs5;
    int prevAppr1;

    // MEMORY VARIABLES
    double memEstimate = 0;
    double sumAllocatedMem = 0;
    double sumDeltaMem = 0;
    double allocatedMem = 0;
    double prevMem = 0;
    double deltaMem = 0;
    double buff = 0;
    double buff2 = 0;
    double buff3 = 0;
    double buff4 = 0;
    double buff5 = 0;
    double targPropJobsMem = 0;
    double trajCompJobsMem = 0;

    // INPUT STRUCT VARS
    struct exoTargPropInput targPropIn;
    struct endoTrajCompIn trajCompIn;
    struct endoUpdateIn updateIn;
    struct endoReduceIn reduceIn;
    struct printIn poutIn;

    for (deltaT = 0; deltaT < endoIn.inputs.numIntervals; deltaT++)
    {   
        // sum memory
        MPI_Allreduce(&endoIn.allocatedMem, &sumAllocatedMem, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
        MPI_Allreduce(&deltaMem, &sumDeltaMem, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);

        // print memory use summary
        if (endoIn.rank == 0) {
            printf("TIME CHUNK %d of %d\n",deltaT, endoIn.inputs.numIntervals);
            printf("\n\n=============================================================================\n");
            printf("MEMORY INFORMATION\n");
            printf("Allocated memory (gigabytes) = %f\n",(sumAllocatedMem/1e+3));
            printf("Change in allocated memory (gigabytes) = %f\n", (sumDeltaMem/1e+3));
            printf("=============================================================================\n\n\n");
        }

        // on first iteration
        if (deltaT == 0)
        {
            if (endoIn.rank == 0)
                printf("ENDOGENOUS CASE\n");
            // allocate object mapping
            targetMapping = calloc(endoIn.inputs.numTargetObjs, sizeof(int));
            initMapping(targetMapping, endoIn.inputs.numTargetObjs);
            endoIn.allocatedMem += (endoIn.inputs.numTargetObjs)*sizeof(int)/1e6;

            // allocate memory for max propagations jobs
            targPropJobs = calloc(endoIn.inputs.numTargetObjs, sizeof(int));
            endoIn.allocatedMem += (endoIn.inputs.numTargetObjs)*sizeof(int)/1e6;
            targPropJobsMem = endoIn.inputs.numTargetObjs*sizeof(int)/1e6;
    
            // allocate memory for max trajectory comparison jobs
            trajCompJobs = calloc(endoIn.combs, sizeof(int));
            endoIn.allocatedMem += endoIn.combs*sizeof(int)/1e6;
            trajCompJobsMem = endoIn.combs*sizeof(int)/1e6;

            // allocate memory for exit codes
            targExitCodes = calloc(endoIn.inputs.numTargetObjs, sizeof(int));
            endoIn.allocatedMem += (endoIn.inputs.numTargetObjs)*sizeof(int)/1e6;

            // populate propagation job arrays
            initPropArr(targPropJobs, endoIn.inputs.numTargetObjs);

            // populate comparison job arrays
            initTrajCompArr(trajCompJobs, endoIn.combs);

            // intialize output directory
            initDirOut(endoIn.inputs.outputDir, endoIn.inputs.numIntervals, endoIn.rank);

            // allocate memory for trajectory lengths
            targTrajLengths = calloc(endoIn.inputs.numTargetObjs, sizeof(int));
            endoIn.allocatedMem += (endoIn.inputs.numTargetObjs)*sizeof(int)/1e6;
            
            // print memory allocation estimate 1
            if(deltaT == 0)
            {
                MPI_Allreduce(&endoIn.allocatedMem, &memEstimate, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
                if(endoIn.rank == 0)
                    printf("Minmum memory requirement(gigabytes) 1 = %f\n",(memEstimate/1e+3));
            }
            

            // collision array
            C = malloc(sizeof(struct contigDouble));
            locApproaches = malloc(sizeof(struct contigInt));
            sepArr = malloc(sizeof(struct contigDouble));
            mjdArr = malloc(sizeof(struct contigDouble));
            if(endoIn.rank == 0)
                allocContigDouble(C, 100*4);
            else
                allocContigDouble(C, 1);
            allocContigInt(locApproaches, 100);
            allocContigDouble(sepArr, 100);
            allocContigDouble(mjdArr, 100);
            endoIn.allocatedMem += *(C->mem);
            endoIn.allocatedMem += *(locApproaches->mem);
            endoIn.allocatedMem += *(sepArr->mem);
            endoIn.allocatedMem += *(mjdArr->mem);

            // print memory allocation estimate 2
            if(deltaT == 0)
            {
                MPI_Allreduce(&endoIn.allocatedMem, &memEstimate, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
                if(endoIn.rank == 0)
                    printf("Minmum memory requirement(gigabytes) 2 = %f\n",(memEstimate/1e+3));
            }

            // number of jobs
            numTargJobs = endoIn.inputs.numTargetObjs;
            numCompJobs = endoIn.combs;

            // global mapping
            globTargetMap = calloc(endoIn.inputs.numTargetObjs, sizeof(int));
            endoIn.allocatedMem += (endoIn.inputs.numFieldObjs)*sizeof(int)/1e6;

            // initialize 1 to 1 global mapping
            // printf("initialize global mapping\n");
            initMapping(globTargetMap, endoIn.inputs.numTargetObjs);

            // alloc mem for trajectory structs
            targetTraj = malloc(sizeof(struct doubleSemi));
            fieldTraj = malloc(sizeof(struct doubleSemi));
            endoIn.allocatedMem += 2*sizeof(struct doubleSemi)/1e6;

            // allocate mem for reds
            reds = malloc(sizeof(struct redStates));
            reds2 = malloc(sizeof(struct redStates));
            reds3 = malloc(sizeof(struct redStates));
            reds4 = malloc(sizeof(struct redStates));
            reds5 = malloc(sizeof(struct redStates));

            // save previous number of combs
            prevAppr1 = endoIn.combs;
            
            // print memory allocation estimate 3
            if(deltaT == 0)
            {
                MPI_Allreduce(&endoIn.allocatedMem, &memEstimate, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
                if(endoIn.rank == 0)
                    printf("Minmum memory requirement(gigabytes) 3 = %f\n",(memEstimate/1e+3));
            }
        }


        // calculate maximum velocity
        // printf("calculate max velocity\n");
        vmax = calcVmax(endoIn.targetStates, endoIn.targetStates, endoIn.inputs.numTargetObjs, 0, endoIn.inputs.endogenous);

        // calculate tstep for first round
        // printf("calculate tstep for first round\n");
        tstep1 = ((endoIn.d1/vmax)/2)/60/60/24;
        
        // calculate max points
        // printf("calculate max points\n");
        partialTspan = endoIn.inputs.tspan/endoIn.inputs.numIntervals;
        mxpts = calcMxpts(partialTspan, tstep1);
        if(endoIn.rank == 0)
            printf("vmax = %f ||| mxpts = %d\n",vmax,mxpts);

        // initialize reds 
        if (deltaT == 0)
        {
            initReds(reds, &buff, numTargJobs, 1, mxpts*10, endoIn.rank);
            initReds(reds2, &buff2, numTargJobs, 1, mxpts*10, endoIn.rank);
            initReds(reds3, &buff3, numTargJobs, 1, mxpts*10, endoIn.rank);
            initReds(reds4, &buff4, numTargJobs, 1, mxpts*10, endoIn.rank);
            initReds(reds5, &buff5, numTargJobs, 1, mxpts*10, endoIn.rank);
            endoIn.allocatedMem += 5*sizeof(struct redStates);
            endoIn.allocatedMem += buff + buff2 + buff3;
            endoIn.allocatedMem += buff4 + buff5;
        }

        // print memory allocation estimate 4
        if(deltaT == 0)
        {
            MPI_Allreduce(&endoIn.allocatedMem, &memEstimate, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
            if(endoIn.rank == 0)
                printf("Minmum memory requirement(gigabytes) 4 = %f\n",(memEstimate/1e+3));
        }
        
        //DEBUG
        if (debug == 1)
        {
            printf("VMAX: %.15f", vmax);
            printf("TIMESTEP: %.15f",tstep1);
            printf("MAXPOINTS: %d\n",mxpts);
        }

        // ==========================================================
        // FILTER 1 =================================================
        // ==========================================================
        
        // calculate number of jobs per cpu for 1st round of filter
        // printf("calculate number of jobs per cpu for 1st round of filter\n");
        endoJobs1 = calcEndoJobLengths(endoIn.inputs.numCores, numTargJobs, numCompJobs);

        // allocate trajectories
        if ( endoIn.rank == 0)
            printf("allocating trajectories\n");
        if (endoIn.rank == 0)
        {
            targL = endoIn.inputs.numTargetObjs;
            fieldL = 1;
        }
        else
        {
            targL = 1;
            fieldL = 1;

        }
        if (deltaT == 0)
        {
            // printf("ranke %d allocating on  %d - %d - %d\n",rank,mxpts,targL,fieldL);
            alloc3DDoubleSemi(targetTraj, targL, mxpts*2, 12);
            alloc3DDoubleSemi(fieldTraj, fieldL, mxpts*2, 12);
            endoIn.allocatedMem += ( *(targetTraj->mem) + *(fieldTraj->mem) );
        }
        else
        {
            // printf("ranke %d reallocating on  %d - %d - %d\n",rank,mxpts,targL,fieldL);
            reallocDouble3Dsemi(targetTraj,targL, mxpts);
            reallocDouble3Dsemi(fieldTraj,fieldL, mxpts);
            endoIn.allocatedMem += ( *(targetTraj->mem) + *(fieldTraj->mem) );
            endoIn.allocatedMem -= ( *(targetTraj->prevmem) + *(fieldTraj->prevmem) );
            // printf("done on ranke %d\n",rank);
        }

        // print memory allocation estimate main
        if(deltaT == 0)
        {
            MPI_Allreduce(&endoIn.allocatedMem, &memEstimate, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
            if(endoIn.rank == 0)
                printf("Minmum memory requirement(gigabytes) = %f\n",(memEstimate/1e+3));
        }
        //break;

        // populate targ prop input 1
        propIn1.nomNumTargPropJobs = endoJobs1.nomNumPropJobs;
        propIn1.rootNumTargPropJobs = endoJobs1.rootNumPropJobs;
        targPropIn.rank            = endoIn.rank;
        targPropIn.jobParams       = propIn1;
        targPropIn.input           = endoIn.inputs;
        targPropIn.targetTraj      = targetTraj->arr;
        targPropIn.targetStates    = endoIn.targetStates;
        targPropIn.mxpts           = mxpts; 
        targPropIn.partialTspan    = partialTspan; 
        targPropIn.tstep           = tstep1; 
        targPropIn.targTrajLengths = targTrajLengths; 
        targPropIn.numJobs         = numTargJobs; 
        targPropIn.targPropJobs    = targPropJobs;
        targPropIn.targExitCodes   = targExitCodes; 
        targPropIn.lenSet          = endoIn.inputs.numTargetObjs; 
        targPropIn.bugtag          = 100; 
        targPropIn.cpus            = endoIn.numtasks;

        // propagate 1st round of target objects
        if ( endoIn.rank == 0 )
            printf("propagate 1st round of target objects -> %d\n",mxpts);
        propTargObjs(targPropIn);
        // printf("done with target propagations on rank: %d\n",rank);

        // populate traj comp input 1
        trajCompIn.rank               = endoIn.rank; 
        trajCompIn.targTrajLengths    = targTrajLengths;
        trajCompIn.targetTrajectories = targetTraj->arr; 
        trajCompIn.fieldTrajectories  = fieldTraj->arr; 
        trajCompIn.input              = endoIn.inputs;
        trajCompIn.jobParams          = endoJobs1; 
        trajCompIn.mxpts              = mxpts; 
        trajCompIn.P                  = endoIn.P; 
        trajCompIn.C                  = C; 
        trajCompIn.trajCompJobs       = trajCompJobs; 
        trajCompIn.targetMap          = globTargetMap; 
        trajCompIn.dApproach          = endoIn.d1; 
        trajCompIn.combs              = numCompJobs; 
        trajCompIn.cpus               = endoIn.numtasks; 
        trajCompIn.pairs              = &pairs1; 
        trajCompIn.locApproaches      = locApproaches; 
        trajCompIn.sepArr             = sepArr;
        trajCompIn.mjdArr             = mjdArr; 
        trajCompIn.cpuMem             = &endoIn.allocatedMem; 
        trajCompIn.nextRed            = reds; 
        trajCompIn.targExitCodes      = targExitCodes; 

        // compare 1st round of trajectories
        if ( endoIn.rank == 0 )
            printf("compare trajectories\n");
        appr1 = compareEndoTraj(trajCompIn);

        // populate update input structure
        updateIn.targetTrajectory  = targetTraj->arr; 
        updateIn.targetStates      = endoIn.targetStates; 
        updateIn.targetTrajLengths = targTrajLengths;
        updateIn.targExitCodes     = targExitCodes; 
        updateIn.C                 = C; 
        updateIn.P                 = endoIn.P; 
        updateIn.numTargetObjs     = endoIn.inputs.numTargetObjs; 
        updateIn.combs             = endoIn.combs; 
        updateIn.targPropJobs      = targPropJobs; 
        updateIn.trajCompJobs      = trajCompJobs; 
        updateIn.rank              = endoIn.rank; 
        updateIn.numCompJobs       = &numCompJobs; 
        updateIn.numTargJobs       = &numTargJobs; 
        updateIn.totalMem          = &endoIn.allocatedMem; 
        updateIn.trajComJobsMem    = &trajCompJobsMem; 
        updateIn.targPropJobsMem   = &targPropJobsMem; 
        
        // exit if no close approaches recorded
        // printf("exit if no close approaches recorded\n");

        if (appr1 == 0)
        {

            if ( endoIn.rank == 0 )
                printf("no close approaches\n");
            updateEndoStates(updateIn);
            
            // update memory
            deltaMem = endoIn.allocatedMem - prevMem;
            prevMem = endoIn.allocatedMem;

            // BREAK IF NO MORE JOBS REQUIRED
            if (numCompJobs == 0 || numTargJobs == 0)
            {
                // printf("no more jobs required\n");
                break;
            }
            else {
                // printf("no stage 1 ca in time chunk %d\n",deltaT);
                continue;
            }
        }

        if (endoIn.rank == 0)
            printf("stage 1 collision detected\n");
        MPI_Barrier(MPI_COMM_WORLD);

        // populate reduction input 1
        reduceIn.origTargStates  = endoIn.targetStates;
        reduceIn.C               = C;
        reduceIn.nApproach       = appr1;
        reduceIn.numTargObjs     = endoIn.inputs.numTargetObjs;
        reduceIn.rank            = endoIn.rank;
        reduceIn.d_rec           = endoIn.d2;
        reduceIn.tspan           = partialTspan;
        reduceIn.totalMem        = &endoIn.allocatedMem;
        reduceIn.reds            = reds;
        reduceIn.cpus            = endoIn.numtasks;
        reduceIn.combos          = pairs1;

        // reduce sets 
        if ( endoIn.rank == 0 )
            printf("reducing sets\n");
        reduceEndoStates(reduceIn);

        // populate print input 1
        poutIn.targTraj           = targetTraj->arr;
        poutIn.targStates         = endoIn.targetStates;
        poutIn.tag                = 1;
        poutIn.idx                = deltaT;
        poutIn.rank               = endoIn.rank;
        poutIn.endo               = endoIn.inputs.endogenous;
        poutIn.numApproaches      = appr1;
        poutIn.targTrajLengths    = targTrajLengths;
        poutIn.numTargetObjs      = endoIn.inputs.numTargetObjs;
        poutIn.C                  = C->arr;
        poutIn.ogTargetTraj       = targetTraj->arr;
        poutIn.ogTargTrajLengths  = targTrajLengths;
        poutIn.ogNumTargetObjs    = endoIn.inputs.numTargetObjs;
        poutIn.targMapping        = globTargetMap;
        poutIn.targStatus         = reds->targStatus->arr; 
    
        // print output for filter 1
        if (endoIn.inputs.print1)
        {   
            if (endoIn.rank == 0)
                printf("printing stage 1 output\n"); 
            printOutput(poutIn, endoIn.inputs.outputDir);
        }

        // synchronize
        MPI_Barrier(MPI_COMM_WORLD);

        // ==========================================================
        // FILTER 2 =================================================
        // ==========================================================

        // break condition
        if (endoIn.inputs.numFilters < 2)
        {
            updateEndoStates(updateIn);
            continue;
        }

        //calculate number of jobs per cpu for 2nd round of filter
        if ( endoIn.rank == 0 )
            printf("calculate number of jobs per cpu for 2nd round of filter on core: %d\n",endoIn.rank);
        endoJobs2 = calcEndoJobLengths(endoIn.inputs.numCores, *(reds->numTargetObjs), pairs1);

        // populate targ prop input 2
        propIn2.nomNumTargPropJobs = endoJobs2.nomNumPropJobs;
        propIn2.rootNumTargPropJobs = endoJobs2.rootNumPropJobs;
        targPropIn.jobParams       = propIn2;
        targPropIn.targetTraj      = reds->targetTraj->arr;
        targPropIn.targetStates    = reds->targetStates->arr;
        targPropIn.mxpts           = *(reds->mxpts); 
        targPropIn.tstep           = *(reds->tstep); 
        targPropIn.targTrajLengths = reds->targetTrajLengths->arr; 
        targPropIn.numJobs         = *(reds->numTargetObjs); 
        targPropIn.targPropJobs    = reds->targetPropJobs->arr;
        targPropIn.targExitCodes   = reds->targExitCodes->arr; 
        targPropIn.lenSet          = *(reds->numTargetObjs); 

        // propagate 2nd round of target objects
        if ( endoIn.rank == 0 )
            printf("propagate 2nd round target of objects -> %d\n", *(reds->mxpts));
        propTargObjs(targPropIn);


        // populate traj comp input 2
        trajCompIn.targTrajLengths    = reds->targetTrajLengths->arr;
        trajCompIn.targetTrajectories = reds->targetTraj->arr; 
        trajCompIn.jobParams          = endoJobs2; 
        trajCompIn.mxpts              = *(reds->mxpts); 
        trajCompIn.P                  = reds->P->arr; 
        trajCompIn.C                  = reds->C; 
        trajCompIn.trajCompJobs       = reds->trajCompJobs->arr; 
        trajCompIn.targetMap          = reds->targetMapping->arr; 
        trajCompIn.dApproach          = endoIn.d2; 
        trajCompIn.combs              = pairs1; 
        trajCompIn.pairs              = &pairs2; 
        trajCompIn.locApproaches      = reds->locApproaches; 
        trajCompIn.sepArr             = reds->sepArr;
        trajCompIn.mjdArr             = reds->mjdArr; 
        trajCompIn.nextRed            = reds2; 
        trajCompIn.targExitCodes      = reds->targExitCodes->arr; 

        // compare trajectories
        if ( endoIn.rank == 0 )
            printf("compare trajectories\n");
        appr2 = compareEndoTraj(trajCompIn);
        
        // exit if no close approaches recorded
        if (appr2 == 0)
        {
            if ( endoIn.rank == 0 )
                printf("no close approaches\n");
            updateEndoStates(updateIn);

            // update memory
            deltaMem = endoIn.allocatedMem - prevMem;
            prevMem = endoIn.allocatedMem;

            // BREAK IF NO MORE JOBS REQUIRED
            if (numCompJobs == 0 || numTargJobs == 0)
            {
                // printf("no more jobs required\n");
                break;
            }
            else {
                continue;
            }
        }

        if (endoIn.rank == 0)
            printf("stage 2 collision detected\n");
        
        // populate reduction input 2
        reduceIn.C               = reds->C;
        reduceIn.nApproach       = appr2;
        reduceIn.d_rec           = endoIn.d3;
        reduceIn.reds            = reds2;
        reduceIn.combos          = pairs2;
        
        // reduce round 2 sets
        if ( endoIn.rank == 0 )
            printf("reducing round 2 sets\n");
        reduceEndoStates(reduceIn);

        // populate print input 2
        poutIn.targTraj           = reds->targetTraj->arr;
        poutIn.targStates         = reds->targetStates->arr;
        poutIn.tag                = 2;
        poutIn.numApproaches      = appr2;
        poutIn.targTrajLengths    = reds->targetTrajLengths->arr;
        poutIn.C                  = reds->C->arr;
        poutIn.targMapping        = reds->targetMapping->arr;
        poutIn.targStatus         = reds2->targStatus->arr; 

        // print output for filter 2
        if (endoIn.inputs.print2)
        {
            if ( endoIn.rank == 0 )
                printf("print round 2 filter\n");
            printOutput(poutIn, endoIn.inputs.outputDir);
        }

        // synchronize
        MPI_Barrier(MPI_COMM_WORLD);
    

        // ==========================================================
        // FILTER 3 =================================================
        // ==========================================================

        // break condition
        if (endoIn.inputs.numFilters < 3)
        {
            updateEndoStates(updateIn);
            continue;
        }

        // calculate job parameters
        // printf("calculate round 3 parameters\n");
        endoJobs3 = calcEndoJobLengths(endoIn.inputs.numCores, *(reds2->numTargetObjs), pairs2);

        // populate targ prop input 3
        propIn3.nomNumTargPropJobs = endoJobs3.nomNumPropJobs;
        propIn3.rootNumTargPropJobs = endoJobs3.rootNumPropJobs;
        targPropIn.jobParams       = propIn3;
        targPropIn.targetTraj      = reds2->targetTraj->arr;
        targPropIn.targetStates    = reds2->targetStates->arr;
        targPropIn.mxpts           = *(reds2->mxpts); 
        targPropIn.tstep           = *(reds2->tstep); 
        targPropIn.targTrajLengths = reds2->targetTrajLengths->arr; 
        targPropIn.numJobs         = *(reds2->numTargetObjs); 
        targPropIn.targPropJobs    = reds2->targetPropJobs->arr;
        targPropIn.targExitCodes   = reds2->targExitCodes->arr; 
        targPropIn.lenSet          = *(reds2->numTargetObjs);

        // propagate 3rd round of target objects
        if ( endoIn.rank == 0 )
        {
            printf("propagate 3rd round target of objects\n");
            //printf("reqd pts: %d - - - allocd pts: %d",*(reds2->mxpts), *(reds2->targetTraj->rows));
        }
        propTargObjs(targPropIn);

        // populate traj comp input 3
        trajCompIn.targTrajLengths    = reds2->targetTrajLengths->arr;
        trajCompIn.targetTrajectories = reds2->targetTraj->arr; 
        trajCompIn.jobParams          = endoJobs3; 
        trajCompIn.mxpts              = *(reds2->mxpts); 
        trajCompIn.P                  = reds2->P->arr; 
        trajCompIn.C                  = reds2->C; 
        trajCompIn.trajCompJobs       = reds2->trajCompJobs->arr; 
        trajCompIn.targetMap          = reds2->targetMapping->arr; 
        trajCompIn.dApproach          = endoIn.d3; 
        trajCompIn.combs              = pairs2; 
        trajCompIn.pairs              = &pairs3; 
        trajCompIn.locApproaches      = reds2->locApproaches; 
        trajCompIn.sepArr             = reds2->sepArr;
        trajCompIn.mjdArr             = reds2->mjdArr; 
        trajCompIn.nextRed            = reds3; 
        trajCompIn.targExitCodes      = reds2->targExitCodes->arr; 

        // compare round 3 trajectories
        if ( endoIn.rank == 0 )
            printf("compare round 3 trajectories\n");
        appr3 = compareEndoTraj(trajCompIn);
        
        // exit if no close approaches recorded
        if (appr3 == 0)
        {
            if ( endoIn.rank == 0 )
                printf("no close approaches recorded\n");
            updateEndoStates(updateIn);

            // update memory
            deltaMem = endoIn.allocatedMem - prevMem;
            prevMem = endoIn.allocatedMem;

            // BREAK IF NO MORE JOBS REQUIRED
            if (numCompJobs == 0 || numTargJobs == 0)
            {
                // printf("no more jobs required\n");
                break;
            }
            else {
                continue;
            }
        }

        if (endoIn.rank == 0)
            printf("stage 3 collision detected\n");

        // populate reduction input 3
        reduceIn.C               = reds2->C;
        reduceIn.nApproach       = appr3;
        reduceIn.d_rec           = endoIn.d4;
        reduceIn.reds            = reds3;
        reduceIn.combos          = pairs3;

        // reduce round 3 sets
        if ( endoIn.rank == 0 )
            printf("reduce round 3 exo states\n");
        reduceEndoStates(reduceIn);

        // populate print input 3
        poutIn.targTraj           = reds2->targetTraj->arr;
        poutIn.targStates         = reds2->targetStates->arr;
        poutIn.tag                = 3;
        poutIn.numApproaches      = appr3;
        poutIn.targTrajLengths    = reds2->targetTrajLengths->arr;
        poutIn.fieldTrajLengths   = reds2->fieldTrajLengths->arr;
        poutIn.C                  = reds2->C->arr;
        poutIn.targMapping        = reds2->targetMapping->arr;
        poutIn.targStatus         = reds3->targStatus->arr; 

        // print output for filter 3
        if (endoIn.inputs.print3)
        {
            if ( endoIn.rank == 0 )
                printf("printing round 3 output\n");
            printOutput(poutIn, endoIn.inputs.outputDir);
        }
        
        // ==========================================================
        // FILTER 4 =================================================
        // ==========================================================

        // break condition
        if (endoIn.inputs.numFilters < 4)
        {
            updateEndoStates(updateIn);
            continue;
        }

        // calculate job parameters
        // printf("calculate round 3 parameters\n");
        endoJobs4 = calcEndoJobLengths(endoIn.inputs.numCores, *(reds3->numTargetObjs), pairs3);
        
        // populate targ prop input 4
        propIn4.nomNumTargPropJobs = endoJobs4.nomNumPropJobs;
        propIn4.rootNumTargPropJobs = endoJobs4.rootNumPropJobs;
        targPropIn.jobParams       = propIn4;
        targPropIn.targetTraj      = reds3->targetTraj->arr;
        targPropIn.targetStates    = reds3->targetStates->arr;
        targPropIn.mxpts           = *(reds3->mxpts); 
        targPropIn.tstep           = *(reds3->tstep); 
        targPropIn.targTrajLengths = reds3->targetTrajLengths->arr; 
        targPropIn.numJobs         = *(reds3->numTargetObjs); 
        targPropIn.targPropJobs    = reds3->targetPropJobs->arr;
        targPropIn.targExitCodes   = reds3->targExitCodes->arr; 
        targPropIn.lenSet          = *(reds3->numTargetObjs);

        // propagate 4th round of target objects
        if ( endoIn.rank == 0 )
        {
            printf("propagate 4th round target of objects\n");
            //printf("reqd pts: %d - - - allocd pts: %d",*(reds2->mxpts), *(reds2->targetTraj->rows));
        }
        propTargObjs(targPropIn);

        // populate traj comp input 4
        trajCompIn.targTrajLengths    = reds3->targetTrajLengths->arr;
        trajCompIn.targetTrajectories = reds3->targetTraj->arr; 
        trajCompIn.jobParams          = endoJobs4; 
        trajCompIn.mxpts              = *(reds3->mxpts); 
        trajCompIn.P                  = reds3->P->arr; 
        trajCompIn.C                  = reds3->C; 
        trajCompIn.trajCompJobs       = reds3->trajCompJobs->arr; 
        trajCompIn.targetMap          = reds3->targetMapping->arr; 
        trajCompIn.dApproach          = endoIn.d4; 
        trajCompIn.combs              = pairs3; 
        trajCompIn.pairs              = &pairs4; 
        trajCompIn.locApproaches      = reds3->locApproaches; 
        trajCompIn.sepArr             = reds3->sepArr;
        trajCompIn.mjdArr             = reds3->mjdArr; 
        trajCompIn.nextRed            = reds4; 
        trajCompIn.targExitCodes      = reds3->targExitCodes->arr; 
        
        // compare trajectories
        if ( endoIn.rank == 0 )
            printf("compare round 4 trajectories\n");
        appr4 = compareEndoTraj(trajCompIn);
        
        // exit if no close approaches recorded
        if (appr4 == 0)
        {
            if ( endoIn.rank == 0 )
                printf("no close approaches recorded\n");
            updateEndoStates(updateIn);

            // update memory
            deltaMem = endoIn.allocatedMem - prevMem;
            prevMem = endoIn.allocatedMem;

            // BREAK IF NO MORE JOBS REQUIRED
            if (numCompJobs == 0 || numTargJobs == 0)
            {
                // printf("no more jobs required\n");
                break;
            }
            else {
                continue;
            }
        }

        if (endoIn.rank == 0)
            printf("stage 4 collision detected\n");

        // populate reduction input 4
        reduceIn.C               = reds3->C;
        reduceIn.nApproach       = appr4;
        reduceIn.d_rec           = endoIn.d5;
        reduceIn.reds            = reds4;
        reduceIn.combos          = pairs4;
        
        // reduce sets
        if ( endoIn.rank == 0 )
            printf("reduce round 4 exo states -- with %d pairs\n",pairs4);
        reduceEndoStates(reduceIn);

        // populate print input 4
        poutIn.targTraj           = reds3->targetTraj->arr;
        poutIn.targStates         = reds3->targetStates->arr;
        poutIn.tag                = 4;
        poutIn.numApproaches      = appr4;
        poutIn.targTrajLengths    = reds3->targetTrajLengths->arr;
        poutIn.C                  = reds3->C->arr;
        poutIn.targMapping        = reds3->targetMapping->arr;
        poutIn.targStatus         = reds4->targStatus->arr; 
        
        // print output for filter 4
        if (endoIn.inputs.print4)
        {
            if ( endoIn.rank == 0 )
                printf("printing round 4 output\n");
            printOutput(poutIn, endoIn.inputs.outputDir);
        }

        // synchronize
        MPI_Barrier(MPI_COMM_WORLD);

        // ==========================================================
        // FILTER 5 =================================================
        // ==========================================================

        // break condition
        if (endoIn.inputs.numFilters < 5)
        {
            updateEndoStates(updateIn);
            continue;
        }

        // calculate job parameters
        // printf("calculate round 3 parameters\n");
        endoJobs5 = calcEndoJobLengths(endoIn.inputs.numCores, *(reds4->numTargetObjs), pairs4);

        // populate targ prop input 5
        propIn5.nomNumTargPropJobs = endoJobs5.nomNumPropJobs;
        propIn5.rootNumTargPropJobs = endoJobs5.rootNumPropJobs;
        targPropIn.jobParams       = propIn5;
        targPropIn.targetTraj      = reds4->targetTraj->arr;
        targPropIn.targetStates    = reds4->targetStates->arr;
        targPropIn.mxpts           = *(reds4->mxpts); 
        targPropIn.tstep           = *(reds4->tstep); 
        targPropIn.targTrajLengths = reds4->targetTrajLengths->arr; 
        targPropIn.numJobs         = *(reds4->numTargetObjs); 
        targPropIn.targPropJobs    = reds4->targetPropJobs->arr;
        targPropIn.targExitCodes   = reds4->targExitCodes->arr; 
        targPropIn.lenSet          = *(reds4->numTargetObjs);

        // propagate 5th round of target objects
        if ( endoIn.rank == 0 )
        {
            printf("propagate 5th round target of objects\n");
            //printf("reqd pts: %d - - - allocd pts: %d",*(reds2->mxpts), *(reds2->targetTraj->rows));
        }
        propTargObjs(targPropIn);     
        
        // populate traj comp input 5
        trajCompIn.targTrajLengths    = reds4->targetTrajLengths->arr;
        trajCompIn.targetTrajectories = reds4->targetTraj->arr; 
        trajCompIn.jobParams          = endoJobs5; 
        trajCompIn.mxpts              = *(reds4->mxpts); 
        trajCompIn.P                  = reds4->P->arr; 
        trajCompIn.C                  = reds4->C; 
        trajCompIn.trajCompJobs       = reds4->trajCompJobs->arr; 
        trajCompIn.targetMap          = reds4->targetMapping->arr; 
        trajCompIn.dApproach          = endoIn.d5; 
        trajCompIn.combs              = pairs4; 
        trajCompIn.pairs              = &pairs5; 
        trajCompIn.locApproaches      = reds4->locApproaches; 
        trajCompIn.sepArr             = reds4->sepArr;
        trajCompIn.mjdArr             = reds4->mjdArr; 
        trajCompIn.nextRed            = reds5; 
        trajCompIn.targExitCodes      = reds4->targExitCodes->arr; 

        // compare trajectories
        if ( endoIn.rank == 0 )
            printf("compare round 5 trajectories\n");
        appr5 = compareEndoTraj(trajCompIn);
        
        // exit if no close approaches recorded
        if (appr5 == 0)
        {
            if ( endoIn.rank == 0 )
                printf("no close approaches recorded\n");
            updateEndoStates(updateIn);

            // update memory
            deltaMem = endoIn.allocatedMem - prevMem;
            prevMem = endoIn.allocatedMem;

            // BREAK IF NO MORE JOBS REQUIRED
            if (numCompJobs == 0 || numTargJobs == 0)
            {
                // printf("no more jobs required\n");
                break;
            }
            else {
                continue;
            }
        }

        if (endoIn.rank == 0)
            printf("stage 5 collision detected\n");

        // populate reduction input 5
        reduceIn.C               = reds4->C;
        reduceIn.nApproach       = appr5;
        reduceIn.d_rec           = endoIn.d5;
        reduceIn.reds            = reds5;
        reduceIn.combos          = pairs5;
        
        // reduce sets 5
        if ( endoIn.rank == 0 )
            printf("reduce round 5 exo states\n");
        reduceEndoStates(reduceIn);
        
        // populate print input 5
        poutIn.targTraj           = reds4->targetTraj->arr;
        poutIn.targStates         = reds4->targetStates->arr;
        poutIn.tag                = 5;
        poutIn.numApproaches      = appr5;
        poutIn.targTrajLengths    = reds4->targetTrajLengths->arr;
        poutIn.C                  = reds4->C->arr;
        poutIn.targMapping        = reds4->targetMapping->arr;
        poutIn.targStatus         = reds5->targStatus->arr; 

        // print output for filter 5
        if (endoIn.inputs.print5)
        {
            if ( endoIn.rank == 0 )
                printf("printing round 5 output\n");
            printOutput(poutIn, endoIn.inputs.outputDir);
        }

        // synchronize
        MPI_Barrier(MPI_COMM_WORLD);

        //UPDATE FOR NEXT ITERATION
        if ( endoIn.rank == 0 )
            printf("updating for next iteration\n");
        updateEndoStates(updateIn);
        
        // BREAK IF NO MORE JOBS REQUIRED
        if (numCompJobs == 0 || numTargJobs == 0)
        {
            // printf("no more jobs required\n");
            break;
        }

        // update memory
        deltaMem = endoIn.allocatedMem - prevMem;
        prevMem = endoIn.allocatedMem;


    }

    return 0;
        
}  