#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <math.h>
#include <mpi.h>

// function to update states of objects and propagation, and trajectory jobs and to initialize C
extern void cart2coe4c_(double R[], double V[], double COE[], double *GM);

/* function to update endogenous values for next time chunk*/
int updateEndoStates(struct endoUpdateIn in)
{
    // VARS
    int i, j, k, idx, finalState, finalIdx;
    double COE[6], R[3], V[3];
    double mu = 3.986004414498200E+05;
    int numTargPropJobs, numTrajCompJobs, objIdx;
    int targIdx, fieldIdx, exitFlag;
    int tag;
    double r2d = 180/M_PI;
    int buggout = 0;

    // initialize MPI
    MPI_Status stat;
    MPI_Datatype stateSend;
    MPI_Type_contiguous(12, MPI_DOUBLE, &stateSend);
    MPI_Type_commit(&stateSend);

    // calculate number of target propagation jobs required
    numTargPropJobs = 0;
    for ( i= 0; i < in.numTargetObjs; i++)
    {
        if (in.targExitCodes[i] == 0)
            numTargPropJobs++;

    }

    // printf("num field jobs: %d --- num targ jobs: %d\n",numTargPropJobs,numFieldPropJobs);

    // allocate stack for propagation job array
    int targPropArray[numTargPropJobs];

    // add jobs to target propagation array
    idx = 0;
    for (i = 0; i < in.numTargetObjs; i++)
    {
        if (in.targExitCodes[i] == 0)
        {
            targPropArray[idx] = i;
            idx++;
        }

    }


    // calculate number of trajectory comparison jobs
    numTrajCompJobs = 0;
    for (i = 0; i < in.combs; i++)
    {
        // get object Ids
        targIdx = in.P[i][0];
        fieldIdx = in.P[i][1];

        // check for a exit flag
        exitFlag = 0;
        if(in.targExitCodes[targIdx] == 1 || in.targExitCodes[fieldIdx] == 1)
            exitFlag = 1;
        
        // if no exit flag
        if (exitFlag == 0)
            numTrajCompJobs++;
    }

    // allocate stack for trajectory comparison array
    int targTrajCompJobs[numTrajCompJobs];

    // add jobs to comparison array 
    idx = 0;
    for (i = 0; i < in.combs; i++)
    {
        // get object Ids
        targIdx = in.P[i][0];
        fieldIdx = in.P[i][1];

        // check for a exit flag
        exitFlag = 0;
        if(in.targExitCodes[targIdx] == 1 || in.targExitCodes[fieldIdx] == 1)
            exitFlag = 1;
        
        // if no exit flag
        if (exitFlag == 0)
        {
            targTrajCompJobs[idx] = i;
            idx++;
        }
    }

    // copy memory
    memcpy(in.trajCompJobs, &targTrajCompJobs, sizeof(int)*numTrajCompJobs);
    memcpy(in.targPropJobs, &targPropArray, sizeof(int)*numTargPropJobs);
    // printf("done copying memory on rank %d \n",rank);


    if (in.rank == 0)
    {
        // update target states
        for (i = 0; i < numTargPropJobs; i++)
        {
            // check for 
            //grab final state and index
            objIdx = in.targPropJobs[i];
            finalState = in.targetTrajLengths[objIdx];
            finalIdx = finalState - 1;

            // convert to orbital elements
            R[0] = in.targetTrajectory[objIdx][finalIdx * 12 + 1];
            R[1] = in.targetTrajectory[objIdx][finalIdx * 12 + 2];
            R[2] = in.targetTrajectory[objIdx][finalIdx * 12 + 3];
            V[0] = in.targetTrajectory[objIdx][finalIdx * 12 + 4];
            V[1] = in.targetTrajectory[objIdx][finalIdx * 12 + 5];
            V[2] = in.targetTrajectory[objIdx][finalIdx * 12 + 6];

            // call fortran conversion routine
            cart2coe4c_(R, V, COE, &mu);

            //update states
            in.targetStates[objIdx][0] = in.targetTrajectory[objIdx][finalIdx * 12 + 0];
            in.targetStates[objIdx][1] = COE[0];
            in.targetStates[objIdx][2] = COE[1];
            in.targetStates[objIdx][3] = COE[2]*r2d;
            in.targetStates[objIdx][4] = COE[3]*r2d;
            in.targetStates[objIdx][5] = COE[4]*r2d;
            in.targetStates[objIdx][6] = COE[5]*r2d;
            in.targetStates[objIdx][7] = in.targetTrajectory[objIdx][finalIdx * 12 + 7];
            in.targetStates[objIdx][8] = in.targetTrajectory[objIdx][finalIdx * 12 + 8];
            in.targetStates[objIdx][9] = in.targetTrajectory[objIdx][finalIdx * 12 + 9];
            in.targetStates[objIdx][10] = in.targetTrajectory[objIdx][finalIdx * 12 + 10];
            in.targetStates[objIdx][11] = in.targetTrajectory[objIdx][finalIdx * 12 + 11];

            // send target state
            //MPI_Bcast(targetStates[objIdx], 1, stateSend, 0, MPI_COMM_WORLD);

        }

    }
    
    // printf("broadcasting states on rank %d\n",rank);
    // Broadcast states from root rank
    for (i = 0; i < numTargPropJobs; i++)
    {
        // send target state
        objIdx = in.targPropJobs[i];
        MPI_Bcast(in.targetStates[objIdx], 12, MPI_DOUBLE, 0, MPI_COMM_WORLD);
    }
    
    // printf("done broadcasting states on rank %d\n",rank);
    
    // update number of jobs
    (*in.numTargJobs) = numTargPropJobs;
    (*in.numCompJobs) = numTrajCompJobs;

    // zero C
    if (in.rank == 0)
    {
        // printf("zeroing C array\n");
        memset(in.C->arr, 0, *(in.C->elements)*sizeof(double));
    }

    //DEBUG
    if ( buggout == 10 )
    {

        for (j = 0; j < numTargPropJobs; j++)
        {
            printf("TRAJECTORY: %d | CORE: %d\n",j,in.rank);
            printf("npts: %d\n", in.targetTrajLengths[j]);
            printf("%.12f %.12f %.12f %.12f %.12f %.12f %.12f %.12f %.12f %.12f %.12f %.12f\n",
                    in.targetStates[j][0], in.targetStates[j][1], in.targetStates[j][2], in.targetStates[j][3],
                    in.targetStates[j][4], in.targetStates[j][5], in.targetStates[j][6], in.targetStates[j][7],
                    in.targetStates[j][8], in.targetStates[j][9], in.targetStates[j][10], in.targetStates[j][11]);
        }
    }

    // deallocate MPI derived type
    MPI_Type_free(&stateSend);

    return 0;

    

}

/* function to update exogenous values for next time chunk*/
int updateExoStates(struct exoUpdateIn in)
{
    // VARS
    int i, j, k, idx, finalState, finalIdx;
    double COE[6], R[3], V[3];
    double mu = 3.986004414498200E+05;
    int numTargPropJobs, numFieldPropJobs, numTrajCompJobs, objIdx;
    int targIdx, fieldIdx, exitFlag;
    int tag;
    double r2d = 180/M_PI;
    int buggout = 0;

    // initialize MPI
    MPI_Status stat;
    MPI_Datatype stateSend;
    MPI_Type_contiguous(12, MPI_DOUBLE, &stateSend);
    MPI_Type_commit(&stateSend);

    // calculate number of target propagation jobs required
    numTargPropJobs = 0;
    for ( i= 0; i < in.numTargetObjs; i++)
    {
        if (in.targExitCodes[i] == 0)
            numTargPropJobs++;

    }

    // calculate number of field propagation jobs required
    numFieldPropJobs = 0;
    for ( i= 0; i < in.numFieldObjs; i++)
    {
        if (in.fieldExitCodes[i] == 0)
            numFieldPropJobs++;

    }

    // printf("num field jobs: %d --- num targ jobs: %d\n",numTargPropJobs,numFieldPropJobs);

    // allocate stack for propagation job array
    int targPropArray[numTargPropJobs];
    int fieldPropArray[numFieldPropJobs];

    // add jobs to target propagation array
    idx = 0;
    for (i = 0; i < in.numTargetObjs; i++)
    {
        if (in.targExitCodes[i] == 0)
        {
            targPropArray[idx] = i;
            idx++;
        }

    }

    // add jobs to field propagation array
    idx = 0;
    for (i = 0; i < in.numFieldObjs; i++)
    {
        if (in.fieldExitCodes[i] == 0)
        {
            fieldPropArray[idx] = i;
            idx++;
        }

    }

    // calculate number of trajectory comparison jobs
    numTrajCompJobs = 0;
    for (i = 0; i < in.combs; i++)
    {
        // get object Ids
        targIdx = in.P[i][0];
        fieldIdx = in.P[i][1];

        // check for a exit flag
        exitFlag = 0;
        if(in.targExitCodes[targIdx] == 1 || in.fieldExitCodes[fieldIdx] == 1)
            exitFlag = 1;
        
        // if no exit flag
        if (exitFlag == 0)
            numTrajCompJobs++;
    }

    // allocate stack for trajectory comparison array
    int targTrajCompJobs[numTrajCompJobs];

    // add jobs to comparison array array
    idx = 0;
    for (i = 0; i < in.combs; i++)
    {
        // get object Ids
        targIdx = in.P[i][0];
        fieldIdx = in.P[i][1];

        // check for a exit flag
        exitFlag = 0;
        if(in.targExitCodes[targIdx] == 1 || in.fieldExitCodes[fieldIdx] == 1)
            exitFlag = 1;
        
        // if no exit flag
        if (exitFlag == 0)
        {
            targTrajCompJobs[idx] = i;
            idx++;
        }
    }

    // // reallocate trajectory comparison and propagation job arrays
    // (*totalMem) -= ((*trajComJobsMem) + (*targPropJobsMem) + (*fieldPropJobsMem))/1e6;
    // targPropJobs = realloc(targPropJobs, sizeof(int)*numTargPropJobs);
    // fieldPropJobs = realloc(fieldPropJobs, sizeof(int)*numFieldPropJobs);
    // trajCompJobs = realloc(trajCompJobs, sizeof(int)*numTrajCompJobs);
    // (*trajComJobsMem) = sizeof(int)*numTargPropJobs;
    // (*targPropJobsMem) = sizeof(int)*numFieldPropJobs;
    // (*fieldPropJobsMem) = sizeof(int)*numTrajCompJobs;
    // (*totalMem) += ((*trajComJobsMem) + (*targPropJobsMem) + (*fieldPropJobsMem))/1e6;

    // copy memory
    memcpy(in.trajCompJobs, &targTrajCompJobs, sizeof(int)*numTrajCompJobs);
    memcpy(in.targPropJobs, &targPropArray, sizeof(int)*numTargPropJobs);
    memcpy(in.fieldPropJobs, &fieldPropArray, sizeof(int)*numFieldPropJobs);
    // printf("done copying memory on rank %d \n",rank);


    if (in.rank == 0)
    {
        // update target states
        for (i = 0; i < numTargPropJobs; i++)
        {
            // check for 
            //grab final state and index
            objIdx = in.targPropJobs[i];
            finalState = in.targetTrajLengths[objIdx];
            finalIdx = finalState - 1;

            // convert to orbital elements
            R[0] = in.targetTrajectory[objIdx][finalIdx * 12 + 1];
            R[1] = in.targetTrajectory[objIdx][finalIdx * 12 + 2];
            R[2] = in.targetTrajectory[objIdx][finalIdx * 12 + 3];
            V[0] = in.targetTrajectory[objIdx][finalIdx * 12 + 4];
            V[1] = in.targetTrajectory[objIdx][finalIdx * 12 + 5];
            V[2] = in.targetTrajectory[objIdx][finalIdx * 12 + 6];

            // call fortran conversion routine
            cart2coe4c_(R, V, COE, &mu);

            //update states
            in.targetStates[objIdx][0] = in.targetTrajectory[objIdx][finalIdx * 12 + 0];
            in.targetStates[objIdx][1] = COE[0];
            in.targetStates[objIdx][2] = COE[1];
            in.targetStates[objIdx][3] = COE[2]*r2d;
            in.targetStates[objIdx][4] = COE[3]*r2d;
            in.targetStates[objIdx][5] = COE[4]*r2d;
            in.targetStates[objIdx][6] = COE[5]*r2d;
            in.targetStates[objIdx][7] = in.targetTrajectory[objIdx][finalIdx * 12 + 7];
            in.targetStates[objIdx][8] = in.targetTrajectory[objIdx][finalIdx * 12 + 8];
            in.targetStates[objIdx][9] = in.targetTrajectory[objIdx][finalIdx * 12 + 9];
            in.targetStates[objIdx][10] = in.targetTrajectory[objIdx][finalIdx * 12 + 10];
            in.targetStates[objIdx][11] = in.targetTrajectory[objIdx][finalIdx * 12 + 11];

            // send target state
            //MPI_Bcast(targetStates[objIdx], 1, stateSend, 0, MPI_COMM_WORLD);

        }

        // update field states
        for (i = 0; i < numFieldPropJobs; i++)
        {
            // check for 
            //grab final state and index
            objIdx = in.fieldPropJobs[i];
            finalState = in.fieldTrajLengths[objIdx];
            finalIdx = finalState - 1;

            // convert to orbital elements
            R[0] = in.fieldTrajectory[objIdx][finalIdx * 12 + 1];
            R[1] = in.fieldTrajectory[objIdx][finalIdx * 12 + 2];
            R[2] = in.fieldTrajectory[objIdx][finalIdx * 12 + 3];
            V[0] = in.fieldTrajectory[objIdx][finalIdx * 12 + 4];
            V[1] = in.fieldTrajectory[objIdx][finalIdx * 12 + 5];
            V[2] = in.fieldTrajectory[objIdx][finalIdx * 12 + 6];

            // call fortran conversion routine
            cart2coe4c_(R, V, COE, &mu);

            //update states
            in.fieldStates[objIdx][0] = in.fieldTrajectory[objIdx][finalIdx * 12 + 0];
            in.fieldStates[objIdx][1] = COE[0];
            in.fieldStates[objIdx][2] = COE[1];
            in.fieldStates[objIdx][3] = COE[2]*r2d;
            in.fieldStates[objIdx][4] = COE[3]*r2d;
            in.fieldStates[objIdx][5] = COE[4]*r2d;
            in.fieldStates[objIdx][6] = COE[5]*r2d;
            in.fieldStates[objIdx][7] = in.fieldTrajectory[objIdx][finalIdx * 12 + 7];
            in.fieldStates[objIdx][8] = in.fieldTrajectory[objIdx][finalIdx * 12 + 8];
            in.fieldStates[objIdx][9] = in.fieldTrajectory[objIdx][finalIdx * 12 + 9];
            in.fieldStates[objIdx][10] = in.fieldTrajectory[objIdx][finalIdx * 12 + 10];
            in.fieldStates[objIdx][11] = in.fieldTrajectory[objIdx][finalIdx * 12 + 11];

            // send field state
            //MPI_Bcast(fieldStates[objIdx], 1, stateSend, 0, MPI_COMM_WORLD);
        }
    }
    
    // printf("broadcasting states on rank %d\n",rank);
    // Broadcast states from root rank
    for (i = 0; i < numTargPropJobs; i++)
    {
        // send target state
        objIdx = in.targPropJobs[i];
        MPI_Bcast(in.targetStates[objIdx], 12, MPI_DOUBLE, 0, MPI_COMM_WORLD);
    }
    for (i = 0; i < numFieldPropJobs; i++)
    {
        // send field state
        objIdx = in.fieldPropJobs[i];
        MPI_Bcast(in.fieldStates[objIdx], 12, MPI_DOUBLE, 0, MPI_COMM_WORLD);
        
    }
    // printf("done broadcasting states on rank %d\n",rank);
    
    // update number of jobs
    (*in.numTargJobs) = numTargPropJobs;
    (*in.numFieldJobs) = numFieldPropJobs;
    (*in.numCompJobs) = numTrajCompJobs;

    // for (i = 0; i < numTargetObjs; i++)
    //     free(targetTrajectory[i]);
    // free(targetTrajectory);
    
    // for (i = 0; i < numFieldObjs; i++)
    //     free(fieldTrajectory[i]);
    // free(fieldTrajectory);

    // zero C
    if (in.rank == 0)
    {
        // printf("zeroing C array\n");
        memset(in.C->arr, 0, *(in.C->elements)*sizeof(double));
    }

    //DEBUG
    if ( buggout == 10 )
    {
        for (j = 0; j < numFieldPropJobs; j++)
        {
            printf("TRAJECTORY: %d | CORE: %d\n",j,in.rank);
            printf("npts: %d\n", in.fieldTrajLengths[j]);
            printf("%.12f %.12f %.12f %.12f %.12f %.12f %.12f %.12f %.12f %.12f %.12f %.12f\n",
                    in.fieldStates[j][0], in.fieldStates[j][1], in.fieldStates[j][2], in.fieldStates[j][3],
                    in.fieldStates[j][4], in.fieldStates[j][5], in.fieldStates[j][6], in.fieldStates[j][7],
                    in.fieldStates[j][8], in.fieldStates[j][9], in.fieldStates[j][10], in.fieldStates[j][11]);
        }

        for (j = 0; j < numTargPropJobs; j++)
        {
            printf("TRAJECTORY: %d | CORE: %d\n",j,in.rank);
            printf("npts: %d\n", in.targetTrajLengths[j]);
            printf("%.12f %.12f %.12f %.12f %.12f %.12f %.12f %.12f %.12f %.12f %.12f %.12f\n",
                    in.targetStates[j][0], in.targetStates[j][1], in.targetStates[j][2], in.targetStates[j][3],
                    in.targetStates[j][4], in.targetStates[j][5], in.targetStates[j][6], in.targetStates[j][7],
                    in.targetStates[j][8], in.targetStates[j][9], in.targetStates[j][10], in.targetStates[j][11]);
        }
    }

    // deallocate MPI derived type
    MPI_Type_free(&stateSend);

    return 0;

    

}