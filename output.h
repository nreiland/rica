// MODULES
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <math.h>
#include <mpi.h>
#include <stdbool.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <dirent.h>
#include <unistd.h>
#include <errno.h>

/* function to initialize output folder */
int initDirOut(char dirOut[], int numIntervals, int rank)
{
    // function definitions
    int cleanDir(char dirPath[]);    

    if (rank == 0)
    {
        int i, j, k;
        char buf[1000];
        char traj[1000], states[1000], cols[1000], interval[1000], origTraj[1000], origTargTraj[1000], origFieldTraj[1000];
        char targTraj[1000], fieldTraj[1000], targStates[1000], fieldStates[1000];
        char fieldTraj1[1000], targTraj1[1000], fieldState1[1000], targState1[1000], col1[1000];
        char fieldTraj2[1000], targTraj2[1000], fieldState2[1000], targState2[1000], col2[1000];
        char fieldTraj3[1000], targTraj3[1000], fieldState3[1000], targState3[1000], col3[1000];

        // make output directory
        cleanDir(dirOut);

        // make sub folders for each time interval
        for (i = 0; i < numIntervals; i++)
        {
            // make strings for sub folders
            snprintf(interval, 10, "%d", i);
            // itoa(i, interval, 10);

            // original trajectories
            // trajectories
            strcpy(origTraj, dirOut);
            strcat(origTraj, "/");
            strcat(origTraj, interval);
            mkdir(origTraj, 0777);
            strcat(origTraj, "/originalTrajectories");
            //mkdir(origTraj, 0777);
            strcpy(origTargTraj, origTraj);
            strcpy(origFieldTraj, origTraj);
            strcat(origTargTraj, "/target");
            strcat(origFieldTraj, "/field");
            //mkdir(origTargTraj, 0777);
            //mkdir(origFieldTraj, 0777);

            // trajectories
            strcpy(traj, dirOut);
            strcat(traj, "/");
            strcat(traj, interval);
            strcat(traj, "/trajectories/");
            //mkdir(traj, 0777);
            strcpy(targTraj, traj);
            strcpy(fieldTraj, traj);
            strcat(targTraj, "target");
            strcat(fieldTraj, "field");
            //mkdir(targTraj, 0777);
            //mkdir(fieldTraj, 0777);

            strcpy(fieldTraj1, traj);
            strcpy(fieldTraj2, traj);
            strcpy(fieldTraj3, traj);
            strcat(fieldTraj1, "field/filter1");
            strcat(fieldTraj2, "field/filter2");
            strcat(fieldTraj3, "field/filter3");
            strcpy(targTraj1, traj);
            strcpy(targTraj2, traj);
            strcpy(targTraj3, traj);
            strcat(targTraj1, "target/filter1");
            strcat(targTraj2, "target/filter2");
            strcat(targTraj3, "target/filter3");
            //mkdir(targTraj1, 0777);
            //mkdir(fieldTraj1, 0777);
            //mkdir(targTraj2, 0777);
            //mkdir(fieldTraj2, 0777);
            //mkdir(targTraj3, 0777);
            //mkdir(fieldTraj3, 0777);

            // states
            strcpy(states, dirOut);
            strcat(states, "/");
            strcat(states, interval);
            //mkdir(states, 0777);
            strcat(states, "/");
            //mkdir(states, 0777);
            strcpy(targStates, states);
            strcpy(fieldStates, states);
            strcat(targStates, "target");
            strcat(fieldStates, "field");
            mkdir(targStates, 0777);
            mkdir(fieldStates, 0777);

            strcpy(fieldState1, states);
            strcpy(fieldState2, states);
            strcpy(fieldState3, states);
            strcat(fieldState1, "field/filter1");
            strcat(fieldState2, "field/filter2");
            strcat(fieldState3, "field/filter3");
            strcpy(targState1, states);
            strcpy(targState2, states);
            strcpy(targState3, states);
            strcat(targState1, "target/filter1");
            strcat(targState2, "target/filter2");
            strcat(targState3, "target/filter3");
            // mkdir(targState1, 0777);
            // mkdir(fieldState1, 0777);
            // mkdir(targState2, 0777);
            // mkdir(fieldState2, 0777);
            // mkdir(targState3, 0777);
            // mkdir(fieldState3, 0777);

            // collisions
            strcpy(cols, dirOut);
            strcat(cols, "/");
            strcat(cols, interval);
            // mkdir(cols, 0777);
            strcat(cols, "/collisions");
            // mkdir(cols, 0777);

            strcpy(col1, cols);
            strcpy(col2, cols);
            strcpy(col3, cols);
            strcat(col1, "/filter1");
            strcat(col2, "/filter2");
            strcat(col3, "/filter3");
            // mkdir(col1, 0777);
            // mkdir(col2, 0777);
            // mkdir(col3, 0777);
        }
    }

    return 1; 
}

/* function to print filter output */
int printOutput(struct printIn in, char dirOut[])
{
    // function definitions
    int outputTrajectory(char dir[], double **traj, int numObjs, int *trajLength, int *mapping, int *status);
    int outputState(char dir[], double **states, int numObjs, int *mapping, int *status, int tag);
    int outputCollisions(char dir[], double *C, int numApproaches, int tag);
    int outputMapping(char fname[], int *mapping, int numObjects);

    if (in.rank == 0)
    {

        int i, j, k;
        char traj[1000], states[1000], cols[1000], interval[1000];
        char printDir[1000];
        char origTraj[1000], mappingTarg[1000], mappingField[1000], origFieldTraj[1000], origTargTraj[1000];
        char fieldTraj1[1000], targTraj1[1000], fieldState1[1000], targState1[1000], col1[1000];
        char fieldTraj2[1000], targTraj2[1000], fieldState2[1000], targState2[1000], col2[1000];
        char fieldTraj3[1000], targTraj3[1000], fieldState3[1000], targState3[1000], col3[1000];
        char mappingTarg1[1000], mappingTarg2[1000], mappingTarg3[1000];
        char mappingField1[1000], mappingField2[1000], mappingField3[1000];

        // define path to files
        snprintf(interval, 10, "%d", in.idx);
        // itoa(i, interval, 10);

        //for (j = 0; j < )

        // output dir
        strcpy(printDir,dirOut);
        strcat(printDir, "/");
        strcat(printDir, interval);
        // printf("printing to directory %s\n",dirOut);

        // original trajectories
        strcpy(origTraj, dirOut);
        strcat(origTraj, "/");
        strcat(origTraj, interval);
        strcat(origTraj, "/originalTrajectories/");
        strcpy(origTargTraj, origTraj);
        strcpy(origFieldTraj, origTraj);
        strcat(origTargTraj, "target");
        strcat(origFieldTraj, "field");

        // mapping
        strcpy(mappingTarg, dirOut);
        strcat(mappingTarg, "/");
        strcat(mappingTarg, interval);
        strcpy(mappingField, dirOut);
        strcat(mappingField, "/");
        strcat(mappingField, interval);
        strcpy(mappingTarg1, mappingTarg);
        strcat(mappingTarg1, "/targetMapping1.txt");
        strcpy(mappingField1, mappingField);
        strcat(mappingField1, "/fieldMapping1.txt");
        strcpy(mappingTarg2, mappingTarg);
        strcat(mappingTarg2, "/targetMapping2.txt");
        strcpy(mappingField2, mappingField);
        strcat(mappingField2, "/fieldMapping2.txt");
        strcpy(mappingTarg3, mappingTarg);
        strcat(mappingTarg3, "/targetMapping3.txt");
        strcpy(mappingField3, mappingField);
        strcat(mappingField3, "/fieldMapping3.txt");


        // trajectories
        strcpy(traj, dirOut);
        strcat(traj, "/");
        strcat(traj, interval);
        strcat(traj, "/trajectories/");
        strcpy(fieldTraj1, traj);
        strcpy(fieldTraj2, traj);
        strcpy(fieldTraj3, traj);
        strcat(fieldTraj1, "field/filter1");
        strcat(fieldTraj2, "field/filter2");
        strcat(fieldTraj3, "field/filter3");
        strcpy(targTraj1, traj);
        strcpy(targTraj2, traj);
        strcpy(targTraj3, traj);
        strcat(targTraj1, "target/filter1");
        strcat(targTraj2, "target/filter2");
        strcat(targTraj3, "target/filter3");

        // states
        strcpy(states, dirOut);
        strcat(states, "/");
        strcat(states, interval);
        // strcat(states, "/states/");
        strcpy(fieldState1, states);
        strcpy(fieldState2, states);
        strcpy(fieldState3, states);
        strcat(fieldState1, "/field");
        strcat(fieldState2, "field/filter2");
        strcat(fieldState3, "field/filter3");
        strcpy(targState1, states);
        strcpy(targState2, states);
        strcpy(targState3, states);
        strcat(targState1, "/target");
        strcat(targState2, "target/filter2");
        strcat(targState3, "target/filter3");

        // collisions
        strcpy(cols, dirOut);
        strcat(cols, "/");
        strcat(cols, interval);
        strcat(cols, "/collisions/");
        strcpy(col1, cols);
        strcpy(col2, cols);
        strcpy(col3, cols);
        strcat(col1, "filter1");
        strcat(col2, "filter2");
        strcat(col3, "filter3");

        // switch to case

        // filter 1
        if (in.tag == 1)
        {
            // endogenous
            if (in.endo == 1)
            {
                outputState(targState1, in.targStates, in.numTargetObjs, in.targMapping, in.targStatus, in.tag);
                outputCollisions(printDir, in.C, in.numApproaches, in.tag);
            }
            else
            {   
                // outputMapping(mappingTarg1, in.targMapping, in.numTargetObjs);
                // outputTrajectory(origTargTraj, in.ogTargetTraj, in.ogNumTargetObjs, in.ogTargTrajLengths, in.targMapping, in.targStatus);
                // outputTrajectory(targTraj1, in.targTraj, in.numTargetObjs, in.targTrajLengths, in.targMapping, in.targStatus);
                // outputState(targState1, in.targStates, in.numTargetObjs, in.targMapping, in.targStatus);
                // outputCollisions(col1, in.C, in.numApproaches);
                // outputMapping(mappingField1, in.in.fieldMapping, in.numFieldObjs);
                // outputTrajectory(origFieldTraj, in.ogFieldTraj, in.ogNumFieldObjs, in.ogFieldTrajLengths, in.in.fieldMapping, in.fieldStatus);
                // outputTrajectory(fieldTraj1, in.fieldTraj, in.numFieldObjs, in.fieldTrajLengths, in.fieldMapping, 1);
                // outputState(fieldState1, in.fieldStates, in.numFieldObjs, in.fieldMapping, in.fieldStatus);
                // outputCollisions(col1, in.C, in.numApproaches);
                outputState(targState1, in.targStates, in.numTargetObjs, in.targMapping, in.targStatus, in.tag);
                outputState(fieldState1, in.fieldStates, in.numFieldObjs, in.fieldMapping, in.fieldStatus, in.tag);
                outputCollisions(printDir, in.C, in.numApproaches, in.tag);
            }
        }

        // filter 2
        else if (in.tag == 2)
        {
            // endogenous
            if (in.endo == 1)
            {
                outputState(targState1, in.targStates, in.numTargetObjs, in.targMapping, in.targStatus, in.tag);
                outputCollisions(printDir, in.C, in.numApproaches, in.tag);
            }
            else
            {
                // outputMapping(mappingTarg2, in.targMapping, in.numTargetObjs);
                //outputTrajectory(origTargTraj, in.ogTargetTraj, in.ogNumTargetObjs, in.ogTargTrajLengths);
                // outputTrajectory(targTraj2, in.targTraj, in.numTargetObjs, in.targTrajLengths, in.targMapping, in.targStatus);
                // outputState(targState2, in.targStates, in.numTargetObjs, in.targMapping, in.targStatus);
                // outputCollisions(col2, in.C, in.numApproaches);

                // outputMapping(mappingField2, in.fieldMapping, in.numFieldObjs);
                //outputTrajectory(origFieldTraj, in.ogFieldTraj, in.ogNumFieldObjs, in.ogFieldTrajLengths);
                // outputTrajectory(fieldTraj2, in.fieldTraj, in.numFieldObjs, in.fieldTrajLengths, in.fieldMapping, in.fieldStatus);
                // outputState(fieldState2, in.fieldStates, in.numFieldObjs, in.fieldMapping, in.fieldStatus);
                //outputCollisions(col2, in.C, in.numApproaches);
                outputState(targState1, in.targStates, in.numTargetObjs, in.targMapping, in.targStatus, in.tag);
                outputState(fieldState1, in.fieldStates, in.numFieldObjs, in.fieldMapping, in.fieldStatus, in.tag);
                outputCollisions(printDir, in.C, in.numApproaches, in.tag);
            }
        }

        // filter 3
        else if (in.tag == 3)
        {
            // endogenous
            if (in.endo == 1)
            {
                outputState(targState1, in.targStates, in.numTargetObjs, in.targMapping, in.targStatus, in.tag);
                outputCollisions(printDir, in.C, in.numApproaches, in.tag);
            }
            else
            {
                // outputMapping(mappingTarg3, in.targMapping, in.numTargetObjs);
                //outputTrajectory(origTargTraj, in.ogTargetTraj, in.ogNumTargetObjs, in.ogTargTrajLengths);
                // outputTrajectory(targTraj3, in.targTraj, in.numTargetObjs, in.targTrajLengths, in.targMapping, in.targStatus);
                // outputState(targState3, in.targStates, in.numTargetObjs, in.targMapping, in.targStatus);
                // outputCollisions(col3, in.C, in.numApproaches);

                // outputMapping(mappingField3, in.fieldMapping, in.numFieldObjs);
                //outputTrajectory(origFieldTraj, in.ogFieldTraj, in.ogNumFieldObjs, in.ogFieldTrajLengths);
                // outputTrajectory(fieldTraj3, in.fieldTraj, in.numFieldObjs, in.fieldTrajLengths, in.fieldMapping, in.fieldStatus);
                // outputState(fieldState3, in.fieldStates, in.numFieldObjs, in.fieldMapping, in.fieldStatus);
                // outputCollisions(col3, in.C, in.numApproaches);
                outputState(targState1, in.targStates, in.numTargetObjs, in.targMapping, in.targStatus, in.tag);
                outputState(fieldState1, in.fieldStates, in.numFieldObjs, in.fieldMapping, in.fieldStatus, in.tag);
                outputCollisions(printDir, in.C, in.numApproaches, in.tag);
            }
        }

        // filter 4
        else if (in.tag == 4)
        {
            // endogenous
            if (in.endo == 1)
            {
                outputState(targState1, in.targStates, in.numTargetObjs, in.targMapping, in.targStatus, in.tag);
                outputCollisions(printDir, in.C, in.numApproaches, in.tag);
            }
            else
            {
                // outputMapping(mappingTarg3, in.targMapping, in.numTargetObjs);
                //outputTrajectory(origTargTraj, in.ogTargetTraj, in.ogNumTargetObjs, in.ogTargTrajLengths);
                // outputTrajectory(targTraj3, in.targTraj, in.numTargetObjs, in.targTrajLengths, in.targMapping, in.targStatus);
                // outputState(targState3, in.targStates, in.numTargetObjs, in.targMapping, in.targStatus);
                // outputCollisions(col3, in.C, in.numApproaches);

                // outputMapping(mappingField3, in.fieldMapping, in.numFieldObjs);
                //outputTrajectory(origFieldTraj, in.ogFieldTraj, in.ogNumFieldObjs, in.ogFieldTrajLengths);
                // outputTrajectory(fieldTraj3, in.fieldTraj, in.numFieldObjs, in.fieldTrajLengths, in.fieldMapping, in.fieldStatus);
                // outputState(fieldState3, in.fieldStates, in.numFieldObjs, in.fieldMapping, in.fieldStatus);
                // outputCollisions(col3, in.C, in.numApproaches);
                outputState(targState1, in.targStates, in.numTargetObjs, in.targMapping, in.targStatus, in.tag);
                outputState(fieldState1, in.fieldStates, in.numFieldObjs, in.fieldMapping, in.fieldStatus, in.tag);
                outputCollisions(printDir, in.C, in.numApproaches, in.tag);
            }
        }

        // filter 5
        else if (in.tag == 5)
        {
            // endogenous
            if (in.endo == 1)
            {
                outputState(targState1, in.targStates, in.numTargetObjs, in.targMapping, in.targStatus, in.tag);
                outputCollisions(printDir, in.C, in.numApproaches, in.tag);
            }
            else
            {
                // outputMapping(mappingTarg3, in.targMapping, in.numTargetObjs);
                //outputTrajectory(origTargTraj, in.ogTargetTraj, in.ogNumTargetObjs, in.ogTargTrajLengths);
                // outputTrajectory(targTraj3, in.targTraj, in.numTargetObjs, in.targTrajLengths, in.targMapping, in.targStatus);
                // outputState(targState3, in.targStates, in.numTargetObjs, in.targMapping, in.targStatus);
                // outputCollisions(col3, in.C, in.numApproaches);

                // outputMapping(mappingField3, in.fieldMapping, in.numFieldObjs);
                //outputTrajectory(origFieldTraj, in.ogFieldTraj, in.ogNumFieldObjs, in.ogFieldTrajLengths);
                // outputTrajectory(fieldTraj3, in.fieldTraj, in.numFieldObjs, in.fieldTrajLengths, in.fieldMapping, in.fieldStatus);
                // outputState(fieldState3, in.fieldStates, in.numFieldObjs, in.fieldMapping, in.fieldStatus);
                // outputCollisions(col3, in.C, in.numApproaches);
                outputState(targState1, in.targStates, in.numTargetObjs, in.targMapping, in.targStatus, in.tag);
                outputState(fieldState1, in.fieldStates, in.numFieldObjs, in.fieldMapping, in.fieldStatus, in.tag);
                outputCollisions(printDir, in.C, in.numApproaches, in.tag);
            }
        }
        // printf("returning\n");
        return 0;
    }
    else
    {
        return 0;
    }
    

    
}

/* function to recursively delete a directory*/
int remove_directory(const char *path)
{
    DIR *d = opendir(path);
    size_t path_len = strlen(path);
    int r = -1;
    
    if (d)
    {
        struct dirent *p;
        
        r = 0;
        
        while (!r && (p=readdir(d)))
        {
            int r2 = -1;
            char *buf;
            size_t len;
            
            /* Skip the names "." and ".." as we don't want to recurse on them. */
            if (!strcmp(p->d_name, ".") || !strcmp(p->d_name, ".."))
            {
                continue;
            }
            
            len = path_len + strlen(p->d_name) + 2;
            buf = malloc(len);
            
            if (buf)
            {
                struct stat statbuf;
                
                snprintf(buf, len, "%s/%s", path, p->d_name);
                
                if (!stat(buf, &statbuf))
                {
                    if (S_ISDIR(statbuf.st_mode))
                    {
                        r2 = remove_directory(buf);
                    }
                    else
                    {
                        r2 = unlink(buf);
                    }
                }
                
                free(buf);
            }
            
            r = r2;
        }
        
        closedir(d);
    }
    
    if (!r)
    {
        r = rmdir(path);
    }
    
    return r;
}

/* function to delete directory if it exists and then create new directory */
int cleanDir(char dirPath[])
{
    // functions
    int remove_directory(const char *path);

    const char *dirPoint = dirPath;
    
    DIR* dir = opendir(dirPath);
    if (dir)
    {
        /* Directory exists. */
        closedir(dir);
        remove_directory(dirPoint);
        mkdir(dirPath, 0777);
    }
    else if (ENOENT == errno)
    {
        /* Directory does not exist. */
        mkdir(dirPath, 0777);
        
    }
    else
    {
        /* opendir() failed for some other reason. */
        printf("opendir() failed for some other reason");
        exit(-1);
    }
    
    return 0;
}

/* function to print trajectories */
int outputTrajectory(char dir[], double **traj, int numObjs, int *trajLength, int *mapping, int *status)
{
    int i, j, k;
    int npts;
    int objIdx;
    int localIdx;
    char fname[1000];
    char fname2[1000];
    char idx[1000];

    FILE *file;

    // print trajectories
    for (j = 0; j < numObjs; j++)
    {
        // get global object index
        objIdx = j;

        // if object is active
        if(status[objIdx] == 1)
        {
            
            // get local object index
            localIdx = mapping[objIdx];

            // get number of points
            npts = trajLength[localIdx];
                
            snprintf(idx, 10, "%d", objIdx);
            strcpy(fname, dir);
            strcat(fname, "/");
            strcat(fname, idx);
            strcat(fname, ".txt");

            file = fopen(fname, "w");

            if (file == NULL)
            {
                printf("Error opening file!\n");
                exit(-1);
            }

            for (i = 0; i < npts; i++)
            {
                fprintf(file,"%.15f %.15f %.15f %.15f %.15f %.15f %.15f %.15f %.15f %.15f %.15f %.15f\n", traj[localIdx][i * 12 + 0],
                    traj[localIdx][i * 12 + 1], traj[localIdx][i * 12 + 2],traj[localIdx][i * 12 + 3],traj[localIdx][i * 12 + 4],
                    traj[localIdx][i * 12 + 5], traj[localIdx][i * 12 + 6],traj[localIdx][i * 12 + 7],traj[localIdx][i * 12 + 8],
                    traj[localIdx][i * 12 + 9], traj[localIdx][i * 12 + 10],traj[localIdx][i * 12 + 11]);
            }

            fclose(file);

        }
    }

    return 0;

}

/* function to print states */
int outputState(char dir[], double **states, int numObjs, int *mapping, int *status, int tag)
{
    int i, j, k;
    int npts;
    int objIdx;
    int localIdx;
    char fname[1000];
    char fname2[1000];
    char idx[1000];
    char tagchar[1000];

    FILE *file;

    // copy tag to char
    snprintf(tagchar, 10, "%d", tag);

    // print states
    for (j = 0; j < numObjs; j++)
    {
        // get global object index
        objIdx = j;

        // if object is active
        if (status[objIdx] == 1)
        {

            // get local object index
            localIdx = mapping[objIdx];

            snprintf(idx, 10, "%d", objIdx);
            strcpy(fname, dir);
            strcat(fname, "/state_");
            strcat(fname, idx);
            strcat(fname, "_");
            strcat(fname, tagchar);
            strcat(fname, ".txt");

            file = fopen(fname, "w");

            if (file == NULL)
            {
                printf("Error opening file!\n");
                exit(-1);
            }

            fprintf(file, "%.12f %.12f %.12f %.12f %.12f %.12f %.12f %.12f %.12f %.12f %.12f %.12f\n",
                    states[localIdx][0], states[localIdx][1], states[localIdx][2], states[localIdx][3],
                    states[localIdx][4], states[localIdx][5], states[localIdx][6], states[localIdx][7],
                    states[localIdx][8], states[localIdx][9], states[localIdx][10], states[localIdx][11]);

            fclose(file);

        }
    }

    return 0;

}

/* function to print collisions */
int outputCollisions(char dir[], double *C, int numApproaches, int tag)
{
    int i, j, k;
    int npts;
    char fname[1000];
    char fname2[1000];
    char idx[1000];
    char tagchar[1000];

    FILE *file;

    // copy tag to char
    snprintf(tagchar, 10, "%d", tag);

    strcpy(fname, dir);
    strcat(fname, "/collisions_");
    strcat(fname, tagchar);
    strcat(fname, ".txt");

    file = fopen(fname, "w");

    if (file == NULL)
    {
        printf("Error opening file!\n");
        exit(-1);
    }

    // print collisions
    for (i = 0; i < numApproaches; i++)
    {
       fprintf(file, "%.0f %.0f %.15f %.15f \n", C[i * 4 + 0], C[i * 4 + 1], C[i * 4 + 2],
               C[i * 4 + 3]);
    }

    fclose(file);

    return 0;

}

int outputMapping(char fname[], int *mapping, int numObjects)
{
    int i;

    FILE *file;

    file = fopen(fname, "w");

    if (file == NULL)
    {
        printf("Error opening file!\n");
        exit(-1);
    }
    
    for (i = 0; i < numObjects; i++)
        fprintf(file, "traj_%d: %d\n",i, mapping[i]);
    
    fclose(file);

    return 0;

}