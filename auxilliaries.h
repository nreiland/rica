/* STRUCTURE DEFINITIONS */
struct branchInput
{
    double mu;
    double d1;
    double d2;
    double d3;
    double d4;
    double d5;
    double **targetStates;
    double **fieldStates;
    int combs;
    int **P;
    double allocatedMem;
    int numtasks;
    int rank;
    int numFilters;
    struct Input inputs;
};

struct exoTargPropInput
{
    int rank;
    struct exoJobParams jobParams;
    struct Input input;
    double **targetTraj;
    double **targetStates;
    int mxpts; 
    double partialTspan; 
    double tstep; 
    int *targTrajLengths; 
    int numJobs; 
    int *targPropJobs;
    int *targExitCodes; 
    int lenSet; 
    int bugtag; 
    int cpus;
};

struct exoFieldPropInput
{
    int rank; 
    struct exoJobParams jobParams; 
    struct Input input;
    double **fieldTraj; 
    double **fieldStates;
    int mxpts; 
    double partialTspan; 
    double tstep; 
    int *fieldTrajLengths; 
    int numJobs; 
    int *fieldPropJobs;
    int *fieldExitCodes; 
    int lenSet;  
    int bugtag; 
    int cpus;
};

struct exoTrajCompIn
{
    int rank; 
    int *targTrajLengths;
    int *fieldTrajLengths;
    double **targetTrajectories; 
    double **fieldTrajectories; 
    struct Input input;
    struct exoJobParams jobParams; 
    int mxpts; 
    int **P; 
    struct contigDouble *C; 
    int *trajCompJobs; 
    int *targetMap; 
    int *fieldMap; 
    double dApproach; 
    int combs; 
    int cpus; 
    int *pairs; 
    struct contigInt *locApproaches; 
    struct contigDouble *sepArr;
    struct contigDouble *mjdArr; 
    double *cpuMem; 
    struct redStates *nextRed; 
    int *targExitCodes; 
    int *fieldExitCodes;
};

struct endoTrajCompIn
{
    int rank; 
    int *targTrajLengths;
    double **targetTrajectories; 
    double **fieldTrajectories; 
    struct Input input;
    struct endoJobParams jobParams; 
    int mxpts; 
    int **P; 
    struct contigDouble *C; 
    int *trajCompJobs; 
    int *targetMap; 
    double dApproach; 
    int combs; 
    int cpus; 
    int *pairs; 
    struct contigInt *locApproaches; 
    struct contigDouble *sepArr;
    struct contigDouble *mjdArr; 
    double *cpuMem; 
    struct redStates *nextRed; 
    int *targExitCodes; 
};

struct exoUpdateIn
{
    double **targetTrajectory; 
    double **targetStates; 
    int *targetTrajLengths;
    double **fieldTrajectory; 
    double **fieldStates;
    int *fieldTrajLengths;
    int *targExitCodes; 
    int *fieldExitCodes; 
    struct contigDouble *C; 
    int **P; 
    int numTargetObjs; 
    int numFieldObjs; 
    int combs; 
    int *targPropJobs; 
    int *fieldPropJobs; 
    int *trajCompJobs; 
    int rank; 
    int *numCompJobs; 
    int *numTargJobs; 
    int *numFieldJobs;
    double *totalMem; 
    double *trajComJobsMem; 
    double *targPropJobsMem; 
    double *fieldPropJobsMem; 
};

struct endoUpdateIn
{
    double **targetTrajectory; 
    double **targetStates; 
    int *targetTrajLengths;
    int *targExitCodes; 
    struct contigDouble *C; 
    int **P; 
    int numTargetObjs; 
    int combs; 
    int *targPropJobs; 
    int *trajCompJobs; 
    int rank; 
    int *numCompJobs; 
    int *numTargJobs; 
    double *totalMem; 
    double *trajComJobsMem; 
    double *targPropJobsMem; 
};

struct exoReduceIn
{
    double **origTargStates; 
    double **origFieldStates;
    struct contigDouble *C; 
    int nApproach; 
    int numTargObjs; 
    int numFieldObjs; 
    int rank; 
    double d_rec; 
    double tspan; 
    double *totalMem; 
    struct redStates *reds;  
    int cpus; 
    int combos;
};

struct endoReduceIn
{
    double **origTargStates; 
    struct contigDouble *C; 
    int nApproach; 
    int numTargObjs; 
    int rank; 
    double d_rec; 
    double tspan; 
    double *totalMem; 
    struct redStates *reds;  
    int cpus; 
    int combos;
};

struct printIn
{
    double **targTraj; 
    double **fieldTraj; 
    double **targStates; 
    double **fieldStates; 
    int tag; 
    int idx; 
    int rank; 
    int endo; 
    int numApproaches; 
    int *targTrajLengths; 
    int *fieldTrajLengths; 
    int numTargetObjs; 
    int numFieldObjs;
    double *C; 
    double **ogTargetTraj; 
    double **ogFieldTraj; 
    int *ogTargTrajLengths; 
    int *ogFieldTrajLengths;
    int ogNumTargetObjs; 
    int ogNumFieldObjs; 
    int *targMapping; 
    int *fieldMapping; 
    int *fieldStatus; 
    int *targStatus;
};