#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <math.h>
#include <mpi.h>
#include "semiContig.h"
#include "rowDouble.h"
#include "rowInt.h"
#include "contigDouble.h"
#include "contigInt.h"
#include "trajSharing.h"
#include "batchPropHead.h"
#include "auxilliaries.h"
#include "initializeCol.h"
#include "prop.h"
#include "reduceStates.h"
#include "compareTraj.h"
#include "update.h"
#include "output.h"
#include "exoBranch.h"
#include "endoBranch.h"

/*main function*/
int main( int argc, char *argv[] )
{

    // DEFINE VARIABLES
    double **targetStates;
    double **fieldStates;
    int combs;
    int **P;
    double allocatedMem;
    int numtasks;
    int rank;
    struct Input input;
    struct branchInput endoIn;
    struct branchInput exoIn;
    double mu = 3.986004414498200E+05;
    double d1;
    double d2;
    double d3;
    double d4;
    double d5;
    double buff;

    // get input propagation parameters
    input = getPropInputs(argv[1]);

    // read in target and field object states
    if(rank == 0)
        printf("getting target states\n");
    targetStates = readObjects(input.targetObjects, input.numTargetObjs);
    if(rank == 0)
        printf("getting field states\n");
    fieldStates = readObjects(input.fieldObjects, input.numFieldObjs);

    // calculate maximum number of combinations of collision events
    if(rank == 0)
        printf("calculating max number of combinations\n");
    combs = calcEventCombs(input);
    
    // allocate space for potential collision events and collisions
    P = allocateInt2DArray(combs, 2, &buff);
    allocatedMem += buff/1e6;
    //int **C = allocateInt2DArray(combs, 3);

    // populate P and C arrays
    popP(P, input);
    
    if(rank == 0)
        printf("ready to start mpi\n");
    // initialize MPI
    MPI_Init(&argc,&argv);
    
	//    printf("started mpi\n");
    // get number of tasks 
    MPI_Comm_size(MPI_COMM_WORLD,&numtasks);
    input.numCores = numtasks;
    
    // get my rank  
    MPI_Comm_rank(MPI_COMM_WORLD,&rank);

    // SELECT CASE
    switch(input.endogenous)
    {
        // ENDOGENOUS
        case 1:

            // populate input struct
            endoIn.mu = mu;
            endoIn.d1 = input.d1;
            endoIn.d2 = input.d2;
            endoIn.d3 = input.d3;
            endoIn.d4 = input.d4;
            endoIn.d5 = input.d5;
            endoIn.targetStates = targetStates;
            endoIn.combs = combs;
            endoIn.P = P;
            endoIn.allocatedMem = allocatedMem;
            endoIn.numtasks = numtasks;
            endoIn.rank = rank;
            endoIn.inputs = input;

            // launch
            int endoCode =  launchEndoCase(endoIn);

            

        // EXOGENOUS
        case 0:

            // populate input struct
            exoIn.mu = mu;
            exoIn.d1 = input.d1;
            exoIn.d2 = input.d2;
            exoIn.d3 = input.d3;
            exoIn.d4 = input.d4;
            exoIn.d5 = input.d5;
            exoIn.targetStates = targetStates;
            exoIn.fieldStates = fieldStates;
            exoIn.combs = combs;
            exoIn.P = P;
            exoIn.allocatedMem = allocatedMem;
            exoIn.numtasks = numtasks;
            exoIn.rank = rank;
            exoIn.inputs = input;

            // launch
            int exoCode = launchExoCase(exoIn);

    } 

    if ( rank == 0 )
            printf("EXITING SUCCESSFULLY\n");

    // done with MPI  
    MPI_Finalize();  

}