clc;clear
close all

% inputs
outputFile = '/Users/nreiland/Documents/starlinknsgo/DATA/debug/collisions.txt';
% outputFile = 'oneWebNom.txt';
figDir = '/Users/nreiland/Documents/starlinknsgo/DATA/figs/debug';

% outputFile = '/Users/nreiland/Documents/testProcess/results.txt';
% setName = 'MiSO';
setName = 'Nominal';

% load collisions
collision = csvread(outputFile);
targId = collision(:,1);
fieldId = collision(:,2);
dcol = collision(:,3);
mjd = collision(:,4);

%%

% histogram of date
h = figure('units','normalized','outerposition',[0 0 1 1]);
totalDays = ceil(max(mjd) - min(mjd));
nbins = totalDays;
tdays = mjd - min(mjd);
histogram(tdays)
histogram(tdays,nbins)
xtickangle(45);
xtick = 1:10:90;
xtitle = 'Time [days]';
ytitle = 'Close approaches $\leq$ 1 km';
ttle = setName;
configFig;
ylim = [0 3000];
set( gca , 'YLim' , ylim);
% xlabel( xtitle , ...
%         'Interpreter', 'LaTex' , ... 
%         'FontSize' , 12 );

name = 'Frequency.pdf';
figName = strcat(figDir,name);

% ax = gca;
% outerpos = ax.OuterPosition;
% ti = ax.TightInset; 
% left = outerpos(1) + ti(1);
% bottom = outerpos(2) + ti(2);
% ax_width = outerpos(3) - ti(1) - ti(3);
% ax_height = outerpos(4) - ti(2) - ti(4);
% ax.Position = [left bottom ax_width ax_height];
% tightfig(h)

h.PaperPosition = [0 0 27 15.1875];
h.PaperSize = [ 27 15.1875];



print(h, '-dpdf', figName);
%%

% histogram of approach distance
h = figure('units','normalized','outerposition',[0 0 1 1]);
nbins = 100;
histogram(dcol)
histogram(dcol,nbins)
xtitle = 'Approach Distance [km]';
ytitle = 'Number of Approaches';
xtick = 0:0.1:1;
ttle = setName;
ylim = [0 1400];
xlim = [0 1];
configFig;
set( gca , 'YLim' , ylim);

name = 'Distance.pdf';
figName = strcat(figDir,name);
h.PaperPosition = [0 0 27 15.1875];
h.PaperSize = [ 27 15.1875];

% ax = gca;
% outerpos = ax.OuterPosition;
% ti = ax.TightInset; 
% left = outerpos(1) + ti(1);
% bottom = outerpos(2) + ti(2);
% ax_width = outerpos(3) - ti(1) - ti(3);
% ax_height = outerpos(4) - ti(2) - ti(4);
% ax.Position = [left bottom ax_width ax_height];

print(h, '-dpdf', figName);
%%

% histogram of field plane
planes = assignPlanes(fieldId,55);
planesTicks = unique(planes);
planesTicks = sort(planesTicks);
h = figure('units','normalized','outerposition',[0 0 1 1]);
nbins = max(planes);
histogram(planes)
histogram(planes,nbins)
set(gca,'xtick',planesTicks)
xtickangle(45);
set(gca,'fontsize',12)
xlabel('Field Object Plane','fontsize',30)
ylabel('Number of Approaches < 1 km','fontsize',30)
set(gca,'fontsize',20)
grid on

% calculate initial nodal difference
nodalDif = calcInitNodalDif(planes,5.1);

name = 'Planes.pdf';
figName = strcat(figDir,name);
h.PaperPosition = [0 0 27 15.1875];
h.PaperSize = [ 27 15.1875];

% ax = gca;
% outerpos = ax.OuterPosition;
% ti = ax.TightInset; 
% left = outerpos(1) + ti(1);
% bottom = outerpos(2) + ti(2);
% ax_width = outerpos(3) - ti(1) - ti(3);
% ax_height = outerpos(4) - ti(2) - ti(4);
% ax.Position = [left bottom ax_width ax_height];

print(h, '-dpdf', figName);

%% scatterplot of close approaches
h = figure('units','normalized','outerposition',[0 0 1 1]);
x = tdays;
y = dcol;
sz = 25;
c = nodalDif;
scatter(x,y,sz,c,'filled')
xtitle = 'Time [days]';
ytitle = 'Approach Distance [km]';
xtick = 1:10:90;
xlim = [0 90];
hcb = colorbar;
caxis([0 180])
colorTitleHandle = get(hcb,'Title');
ttle = setName;
titleString = 'Initial Difference in $\Omega$ [$^{\circ}$]';
set(colorTitleHandle ,'String',titleString,'Interpreter', 'LaTex', 'fontsize',20);
configFig;
ylim = [0 1];
set( gca , 'YLim' , ylim);

name = 'Color.pdf';
figName = strcat(figDir,name);
h.PaperPosition = [0 0 27 15.1875];
h.PaperSize = [ 27 15.1875];

% ax = gca;
% outerpos = ax.OuterPosition;
% ti = ax.TightInset; 
% left = outerpos(1) + ti(1);
% bottom = outerpos(2) + ti(2);
% % ax_width = outerpos(3) - ti(1) - ti(3);
% ax_width = outerpos(3) - ti(1);
% ax_height = outerpos(4) - ti(2) - ti(4);
% ax.Position = [left bottom ax_width ax_height];
% tightfig(h);

print(h, '-dpdf', figName);

%% close figures
close all
%%

% get minimum approach
minCase.Dist = min(dcol);
minCase.idx = find(dcol==minCase.Dist);
minCase.MJD = mjd(minCase.idx);
minCase.TargId = targId(minCase.idx);
minCase.FieldId = fieldId(minCase.idx);

% save total collisions
totalCollision = size(collision,1);
tspan = max(mjd) - min(mjd);
colProb = totalCollision/(tspan*24*60*60*55*1925);

tspan = tspan*60*60*24;

% 1 km
idxArr = find(dcol < 1);
approachMJDArr = floor(mjd(idxArr) - min(mjd));
days = unique(approachMJDArr);
approach1km = dcol(idxArr);
pcol1km = length(approach1km)/tspan;
pcol1kmPerDay = length(days)/90;

% 500 m
idxArr = find(dcol < 0.5);
approachMJDArr = floor(mjd(idxArr) - min(mjd));
days = unique(approachMJDArr);
approach500m = dcol(idxArr);
pcol500m = length(approach500m)/tspan;
pcol500mPerDay = length(days)/90;

% 250 m
idxArr = find(dcol < 0.25);
approachMJDArr = floor(mjd(idxArr) - min(mjd));
days = unique(approachMJDArr);
approach250m = dcol(idxArr);
pcol250m = length(approach250m)/tspan;
pcol250mPerDay = length(days)/90;

% 100 m
idxArr = find(dcol < 0.1);
approachMJDArr = floor(mjd(idxArr) - min(mjd));
days = unique(approachMJDArr);
approach100m = dcol(idxArr);
pco100m = length(approach100m)/tspan;
pcol100mPerDay = length(days)/90;

% 50 meters
idxArr = find(dcol < 0.05);
approachMJDArr = floor(mjd(idxArr) - min(mjd));
days = unique(approachMJDArr);
approach50m = dcol(idxArr);
pcol50m = length(approach50m)/tspan;
pcol50mPerDay = length(days)/90;

% 2 meters
idxArr = find(dcol < 0.002);
approachMJDArr = floor(mjd(idxArr) - min(mjd));
days = unique(approachMJDArr);
approach2m = dcol(idxArr);
pcol2m = length(approach2m)/tspan;
pcol2mPerDay = length(days)/90;


vals = [pcol1kmPerDay;pcol500mPerDay;pcol250mPerDay;pcol100mPerDay;pcol50mPerDay];
close all

