function plotHistogram(mjd,dcol,nbins,nedges)

% X = horzcat(mjd,dcol)';
X = mjd;
Y = dcol;

totalDays = ceil(max(mjd) - min(mjd));
nbins = totalDays;


histogram(X)
histogram(Y)

histogram(X,nbins)
histogram(X,nedges)
% histogram('BinEdges',edges,'BinCounts',counts)
% histogram(C)
% histogram(C,Categories)
% histogram('Categories',Categories,'BinCounts',counts)
% histogram(___,Name,Value)
% histogram(ax,___)
% h = histogram(___)

end

