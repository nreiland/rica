""" Module to build Fica input files bases on states from a specific timestep -> can be used to restart failed sims """

# import modules
import os
import shutil
import sys
import subs

# hard code inputs
targDir = '/Users/nreiland/Documents/cpropTest/0/states/target'
fieldDir = '/Users/nreiland/Documents/cpropTest/0/states/field'
outputDir = '/Users/nreiland/Documents/ficaRestart'

# define main function

def main(targetStatesDir, fieldStatesDir, outputDir):

    # get target states
    targetStates = subs.grabStates(targetStatesDir)

    # get field states
    fieldStates = subs.grabStates(fieldStatesDir)

    # sort target states
    targetStates = subs.sortStates(targetStates)

    # sort field states
    fieldStates = subs.sortStates(fieldStates)

    # create output file strings
    targFile = outputDir + '/targets.txt'
    fieldFile = outputDir + '/fields.txt'

    # write output files
    subs.writeNewInputs(targetStates, targFile)
    subs.writeNewInputs(fieldStates, fieldFile)

    return

# launch
main(targDir,fieldDir,outputDir)


