""" subroutine module for restartSim.py """

# import modules
import os
import sys
import shutil

# function to delete ds store files
def killDsStore(listIn):
    """Function to kill all .DS_Store files in a list"""

    # define indexing
    idx = 0

    # loop through list and delete .DS_Store files
    for dummy in listIn:

        if listIn[idx].endswith(".DS_Store"):

            del listIn[idx]

            # step back indexing
            idx = idx - 1

        # update indexing
        idx = idx + 1

    return listIn

# function to create an array of states from input directory
def grabStates(statesDir):

    # define array
    states = []

    # load state files
    paths = os.listdir(statesDir)
    paths = killDsStore(paths)

    # number of objects
    numObjects = len(paths)

    # loop through objects
    for i in range(numObjects):

        # define file name
        fname = statesDir + '/' + paths[i]

        # define dictionary
        obj = {}

        # open file
        with open(fname, 'r') as stateFile:

            # get object Id
            split = fname.split('.')
            objId = split[0]
            

            # save lines
            lines = stateFile.readlines()

            # get orbels string
            orbelsString = lines[0]

            # split string at white space
            split = orbelsString.split(' ')

            # split object id
            idSplit = objId.split('/')

            # save orbels, physical params and mjd to dictionary
            obj['objId'] = idSplit[-1]
            obj['mjd'] = split[0]
            obj['sma'] = split[1]
            obj['ecc'] = split[2]
            obj['inc'] = split[3]
            obj['raan'] = split[4]
            obj['argp'] = split[5]
            obj['ma'] = split[6]
            obj['scmass'] = split[7]
            obj['adrag'] = split[8]
            obj['asrp'] = split[9]
            obj['cd'] = split[10]
            obj['cr'] = split[11]

        # append to array
        states.append(obj)

    # output
    return states

# function to sort states
def sortStates(states):

    # length of states
    numObjects = len(states)
    
    # define minimum
    min = numObjects + 1000
    
    # define ordered states
    orderedStates = []

    # define list for tracking if object has already been added
    markers = [0] * numObjects
    
    # keep track of total number of objects marked
    numMarked = 0
    
    # enter while loop
    while numMarked < numObjects:

        # loop through states
        for idx in range(numObjects):
            
            # get object designation
            objectDesig = int(states[idx]['objId'])

            # check for new minimuj
            if objectDesig < min and markers[idx] == 0:
                min = objectDesig
                minObjectIndex = idx

        # add to sorted list
        orderedStates.append(states[minObjectIndex])

        # update markers
        markers[minObjectIndex] = 1

        # update number that have been marked
        numMarked = numMarked + 1

        # reset min
        min = numObjects + 100

    return orderedStates

# function to write to output files
def writeNewInputs(states, outputFile):

    # count number of objects
    numObjects = len(states)

    # open file
    with open(outputFile, 'w') as fp:
        
        # loop through objects
        for idx in range(numObjects):

            # orbital elements
            mjd = states[idx]['mjd']
            sma = states[idx]['sma']
            ecc = states[idx]['ecc']
            inc = states[idx]['inc']
            raan = states[idx]['raan']
            argp = states[idx]['argp']
            ma = states[idx]['ma']
            scmass = states[idx]['scmass']
            adrag = states[idx]['adrag']
            asrp = states[idx]['asrp']
            cd = states[idx]['cd']
            cr = states[idx]['cr']
            fp.write("{},{},{},{},{},{},{},{},{},{},{},{}".format(mjd,sma,ecc,
                     inc,raan,argp,ma,scmass,adrag,asrp,cd,cr))
    
    return









            






