import os
import sys
import shutil

def main():
    """main function to be mapped to the command line"""
    # dirIn = sys.argv[1]
    # fout = sys.argv[2]

    dirIn = "/Users/nreiland/Documents/oneweb/FICA/output/controlFinal"
    fout = "/Users/nreiland/Documents/oneweb/FICA/output/processed/newControl.txt"
    filterStage = "3"

    print("fica output directory: ",dirIn)
    print("output file: ",fout)

    # read in time chunks
    chunkPaths = readDeltaTs(dirIn)

    # append input directory
    chunkPaths = appendDir(chunkPaths, dirIn)

    # read in collision file paths
    collisionPaths = getCollisionPaths(chunkPaths, filterStage)

    # collect all collision
    collisions = collectCollision(collisionPaths)

    # write to output file
    write2csv(collisions, fout)
    
    return

def appendDir(paths, dirIn):
    for i in range(len(paths)):
        paths[i] = dirIn + "/" + paths[i]

    return paths

def readDeltaTs(dir):
    """function to read in time chunks"""
    paths = os.listdir(dir)
    paths = killDsStore(paths)

    return paths

def killDsStore(listIn):
    """Function to kill all .DS_Store files in a list"""

    # define indexing
    idx = 0

    # loop through list and delete .DS_Store files
    for dummy in listIn:

        if listIn[idx].endswith(".DS_Store"):

            del listIn[idx]

            # step back indexing
            idx = idx - 1

        # update indexing
        idx = idx + 1

    return listIn

def getCollisionPaths(chunkPaths, filterStage):
    """function to grab paths to collision information files"""

    collisionPaths = {"length" : 0, "collision" : []}

    for i in range(len(chunkPaths)):
        chunk = chunkPaths[i]
#        stage3Path = chunk + "/collisions/filter3"
#        paths = os.listdir(stage3Path)
        paths = os.listdir(chunk)
        paths = killDsStore(paths)
        collisionPath = chunk + "/collisions_" + filterStage + ".txt"
        if os.path.exists(collisionPath) is True:
            collisionPaths["collision"].append(collisionPath)
            collisionPaths["length"] = collisionPaths["length"] + 1
        else:
            continue

    return collisionPaths

def collectCollision(collisionPaths):
    """function to collect all collision information"""

    totalCollisions = []

    for i in range(collisionPaths["length"]):
        collisions = readCollisions(collisionPaths["collision"][i])

        for j in range(len(collisions)):
            totalCollisions.append(collisions[j])

            

    return totalCollisions

def readCollisions(collision):
    """read collision.txt"""

    collisions = []

    with open(collision, 'r') as f_obj:
        lines = f_obj.readlines()
    for i in range(len(lines)):
        str = lines[i].split()
        targId = int(str[0])
        fieldId = int(str[1])
        missDis = float(str[2])
        MJD = float(str[3])
        dir = {"targetId": targId, "fieldId": fieldId, "approachDistance": missDis, "time": MJD}
        collisions.append(dir)

    return collisions


def sumApproaches(collision):

    for i in range(len(collision)):
        test = 1
    
    return test

def write2csv(collisions, outputFile):

     # open file
    with open(outputFile, 'w') as fp:

        # write header
#        fp.write('targetId,fieldId,approachDist,mjd\n')

        # loop through collisions
        for idx in range(len(collisions)):

            # write close approach parameters
            targId = collisions[idx]['targetId']
            fieldId = collisions[idx]['fieldId']
            missDist = collisions[idx]['approachDistance']
            mjd = collisions[idx]['time']
            fp.write('{},{},{},{}\n'.format(targId,fieldId,missDist,mjd))
   
    return

#  launch
main()

