function nodalDif = calcInitNodalDif(planes,deltaW)

nodalDif = zeros(length(planes),1);

for i = 1:length(planes)
    nodalDif(i) = deltaW*planes(i);
end

end

