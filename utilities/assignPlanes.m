function planes = assignPlanes(objIds,objsPerPlane)

planes = zeros(length(objIds),1);

for i = 1:length(objIds)
    idx = objIds(i) + 2;
    planeId = ceil(idx/objsPerPlane);
    planes(i) = planeId;
end
    

end

